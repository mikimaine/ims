<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 11/7/16
 * Time: 4:27 PM
 */

namespace Innovate\DailyPayroll;


use Innovate\MainPayment\TaxRate;

/**
 * Class CalculatePayment
 * @package Innovate\DailyPayroll
 */
class CalculatePayment
{
    const HOUR_PER_DAY = 8;
    /**
     *
     */
    const OT_1_2_5 = 1.25;

    /**
     *
     */
    const OT_1_5 = 1.5;

    /**
     *
     */
    const OT_2_0 = 2.0;

    /**
     *
     */
    const OT_2_5 = 2.5;

    /**
     * @var
     */
    private $total_ot;


    /**
     * @var
     */
    private $taxable_income;

    /**
     * @var
     */
    private $income_tax;

    /**
     * @var
     */
    private $total_deduction;

    /**
     * @var
     */
    private  $net_salary;
    /**
     * @var array
     */
    private $input   = array();
    /**
     * @var array
     */
    private $setting = array();


    /**
     * @var
     */
    private $basic_salary;
    /**
     * @var
     */
    private $wage_per_hour;

    /**
     * CalculatePayment constructor.
     * @param $input
     * @param $setting
     */
    public function __construct($input, $setting)
    {
        $this->input = $input;
        $this->setting = $setting;
    }

    /**
     * @return $this
     */
    public function calculate()
    {
        $this->basic_salary()
             ->wage_per_hour()
             ->total_ot()
             ->taxable_income()
             ->income_tax()
             ->total_deduction()
             ->net_salary()
             ->result();
        return $this;
    }

    /**
     * @return array
     */
    public function result()
    {
             return [
                     'daily_payment_total_ot'           => $this->total_ot,
                     'daily_payment_taxable_income'     => $this->taxable_income,
                     'daily_payment_income_tax'         => $this->income_tax,
                     'daily_payment_total_deduction'    => $this->total_deduction,
                     'daily_payment_net_salary'         => $this->net_salary,
                    ];
    }

    /**
     * @return $this
     */
    private function total_ot()
    {
        $this->total_ot = $this->ot_1_2_5() + $this->ot_1_5() + $this->ot_2_0() + $this->ot_2_5();
        return $this;
    }

    /**
     * @return float
     */
    private function ot_1_2_5()
    {
        return (double) ($this->input['daily_labour_ot_1_25'] * $this->wage_per_hour * (double)self::OT_1_2_5);
    }

    /**
     * @return float
     */
    private function ot_1_5()
    {
        return (double)$this->input['daily_labour_ot_1_5'] * $this->wage_per_hour * (double)self::OT_1_5;
    }

    /**
     * @return float
     */
    private function ot_2_0()
    {
        return (double)$this->input['daily_labour_ot_2_0'] * $this->wage_per_hour * (double)self::OT_2_0;
    }

    /**
     * @return float
     */
    private function ot_2_5()
    {
        return (double)$this->input['daily_labour_ot_2_5'] * $this->wage_per_hour * (double)self::OT_2_5;
    }

    /**
     * @return $this
     */
    private function taxable_income()
    {
        $this->taxable_income = $this->basic_salary + $this->total_ot;
        return $this;
    }

    /**
     * @return $this
     */
    private function income_tax()
    {
        $tax_rate = TaxRate::of($this->taxable_income, $this->setting);
        $this->income_tax = (((double)($this->taxable_income)) * ($tax_rate['percent']/100)) - $tax_rate['deduction_amount'];
        return $this;
    }

    /**
     * @return $this
     */
    private function total_deduction()
    {
        $this->total_deduction = (double) ($this->income_tax + $this->input['daily_labour_loan'] + $this->input['daily_labour_penalty']);
        return $this;
    }

    /**
     * @return $this
     */
    private function net_salary()
    {
        $this->net_salary = (double)($this->taxable_income - $this->total_deduction);
        return $this;
    }

    /**
     * @return $this
     */
    private function wage_per_hour()
    {

        $this->wage_per_hour = (double) ($this->input['daily_labour_wage_per_day'] / self::HOUR_PER_DAY);
        return $this;
    }
    /**
     * @return $this
     */
    private function basic_salary()
    {
        $this->basic_salary = (double) ($this->input['daily_labour_wage_per_day'] * $this->input['daily_labour_working_day']);

        return $this;
    }
}