<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 11/7/16
 * Time: 3:40 PM
 */

namespace Innovate\DailyPayroll;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class DailyPayment
 * @package Innovate\DailyPayroll
 */
class DailyPayment extends Model
{
    use RevisionableTrait,SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'payroll_daily_payment';

    /**
     * @var array
     */
    protected $fillable = [ 'daily_payment_total_ot','daily_payment_taxable_income','daily_payment_income_tax',
                            'daily_payment_total_deduction','daily_payment_net_salary','daily_payment_seen',
                            'daily_payment_approved','daily_payment_id'
                            ];

    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = true;

}