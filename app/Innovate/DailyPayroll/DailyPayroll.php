<?php

namespace Innovate\DailyPayroll;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Innovate\DailyPayroll\Traits\Attribute\DailyPayrollAttribute;
use Innovate\DailyPayroll\Traits\Relationship\DailyPayrollRelationship;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class MainPayroll
 * @package Innovate\Payroll
 */
class DailyPayroll extends Model
{

    use DailyPayrollAttribute,DailyPayrollRelationship,RevisionableTrait,SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'payroll_daily_labour';

    /**
     * Columns That can be mass assigned
     * @var array
     */
    protected $fillable = [ 'daily_labour_project_id','daily_labour_employee_id','daily_labour_bank_account','daily_labour_working_day',
                            'daily_labour_wage_per_day','daily_labour_salary','daily_labour_loan', 'daily_labour_penalty','daily_labour_ot_1_25',
                            'daily_labour_ot_1_5','daily_labour_ot_2_0','daily_labour_ot_2_5'
                          ];

    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = true;


}
