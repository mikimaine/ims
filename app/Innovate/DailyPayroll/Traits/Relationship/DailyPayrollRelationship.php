<?php
/**
 * Created by PhpStorm.
 * User: maine
 * Date: 10/11/16
 * Time: 1:12 PM
 */

namespace  Innovate\DailyPayroll\Traits\Relationship;

use App\Models\Access\User\User;
use Innovate\DailyPayroll\DailyPayment;

trait DailyPayrollRelationship {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


    /**
     * @return mixed
     */
    public function payment()
    {
        return $this->belongsTo(DailyPayment::class,'id','daily_payment_id');
    }
}