<?php

namespace Innovate\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class InnovateServiceProvider.
 */
class InnovateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBinding();
    }

    /**
     *  Bind all innovate module repository to there implementation
     *  which do not have there own service provider.
     */
    private function registerBinding()
    {
        $this->registerMainPayroll();
        $this->registerDailyPayroll();
        $this->registerBackPayPayroll();
        $this->registerPayrollDepartmentSetting();
        $this->registerPayrollTaxRateSetting();
        $this->registerPayrollSetting();
        $this->registerDailyPayrollPaymentSetting();
        $this->registerBackPayPayrollPaymentSetting();


    }

    /**
     *  bind Payroll Repository to its Eloquent implementation and
     *  bind Main payroll Payment
     */
    private function registerMainPayroll()
    {
        $this->app->bind(
            \Innovate\Repositories\Payroll\PayrollContract::class,
            \Innovate\Repositories\Payroll\EloquentPayrollRepository::class
        );
        $this->app->bind(
            \Innovate\Repositories\MainPayment\MainPaymentContract::class,
            \Innovate\Repositories\MainPayment\EloquentMainPaymentRepository::class
        );
    }

    /**
     *  bind DailyPayroll Repository to its Eloquent implementation
     */
    private function registerDailyPayroll()
    {
        $this->app->bind(
            \Innovate\Repositories\DailyPayroll\DailyPayrollContract::class,
            \Innovate\Repositories\DailyPayroll\EloquentDailyPayrollRepository::class
        );
    }

    /**
     *   bind BackPayPayroll Repository to its Eloquent implementation
     */
    private function registerBackPayPayroll()
    {

        $this->app->bind(
            \Innovate\Repositories\BackPayPayroll\BackPayPayrollContract::class,
            \Innovate\Repositories\BackPayPayroll\EloquentBackPayPayrollRepository::class
        );
    }
    /**
     *   bind BackPayPayroll Repository to its Eloquent implementation
     */
    private function registerPayrollDepartmentSetting()
    {

        $this->app->bind(
            \Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract::class,
            \Innovate\Repositories\PayrollSettings\EloquentDepartmentPayrollSettingRepository::class
        );
    }
    /**
     *   bind BackPayPayroll Repository to its Eloquent implementation
     */
    private function registerPayrollTaxRateSetting()
    {

        $this->app->bind(
            \Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract::class,
            \Innovate\Repositories\PayrollSettings\EloquentPayrollTaxRateSettingRepository::class
        );
    }
    /**
     *   bind Payroll Setting Repository to its Eloquent implementation
     */
    private function registerPayrollSetting()
    {

        $this->app->bind(
            \Innovate\Repositories\PayrollSettings\PayrollSettingContract::class,
            \Innovate\Repositories\PayrollSettings\EloquentPayrollSettingRepository::class
        );
    }

    private function registerDailyPayrollPaymentSetting()
    {
        $this->app->bind(
            \Innovate\Repositories\DailyPayroll\DailyPaymentContract::class,
            \Innovate\Repositories\DailyPayroll\EloquentDailyPaymentRepository::class
        );
    }
    private function registerBackPayPayrollPaymentSetting()
    {
        $this->app->bind(
            \Innovate\Repositories\BackPayPayroll\BackPayPaymentPayrollContract::class,
            \Innovate\Repositories\BackPayPayroll\EloquentBackPayPaymentPayrollRepository::class
        );
    }




}
