<?php
/**
 * Created by PhpStorm.
 * User: maine
 * Date: 10/11/16
 * Time: 1:12 PM
 */

namespace  Innovate\BackPayPayroll\Traits\Relationship;

use App\Models\Access\User\User;
use Innovate\BackPayPayroll\BackPayPayment;

trait BackPayPayrollRelationship {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function payment()
    {
        return $this->belongsTo(BackPayPayment::class,'id','back_pay_payment_id');
    }
}