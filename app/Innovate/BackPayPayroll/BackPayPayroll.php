<?php

namespace Innovate\BackPayPayroll;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Innovate\BackPayPayroll\Traits\Attribute\BackPayPayrollAttribute;
use Innovate\BackPayPayroll\Traits\Relationship\BackPayPayrollRelationship;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class MainPayroll
 * @package Innovate\Payroll
 */
class BackPayPayroll extends Model
{

    use BackPayPayrollAttribute,BackPayPayrollRelationship,RevisionableTrait,SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'payroll_back_pay';

    /**
     * List of attributes that can be added
     * @var array
     */
    protected $fillable = [ 'back_pay_project_id', 'back_pay_employee_id', 'back_pay_bank_account','back_pay_permanent_employee',
                            'back_pay_no_of_months', 'back_pay_previous_salary', 'back_pay_new_salary','back_pay_previous_taxable_income',
                            'back_pay_new_taxable_incomes', 'back_pay_previous_non_taxable_income', 'back_pay_new_non_taxable_income' ];


    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = false;


}
