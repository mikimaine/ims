<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 11/14/16
 * Time: 11:39 AM
 */

namespace Innovate\BackPayPayroll;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class BackPayPayment
 * @package Innovate\BackPayPayroll
 */
class BackPayPayment  extends Model
{
    /**
     *
     */
    use RevisionableTrait,SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'payroll_back_pay_payment';

    /**
     * List of attributes that can be added
     * @var array
     */
    protected $fillable = [ 'back_pay_payment_gross_pay','back_pay_payment_taxable_income',
                            'back_pay_payment_pre_income_tax','back_pay_payment_new_income_tax',
                            'back_pay_payment_company_pension','back_pay_payment_employee_pension',
                            'back_pay_payment_total_deduction','back_pay_payment_net_income_tax',
                            'back_pay_payment_net_salary','back_pay_payment_seen','back_pay_payment_approved',
                          ];


    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = false;

}