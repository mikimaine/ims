<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 11/14/16
 * Time: 11:37 AM
 */

namespace Innovate\BackPayPayroll;


use Innovate\MainPayment\TaxRate;

/**
 * Class CalculatePayment
 * @package Innovate\BackPayPayroll
 */
class CalculatePayment
{

    /**
     * @var
     */
    private $input = array();

    /**
     * @var
     */
    private $setting = array();

    /**
     * @var
     */
    private $gross_pay;
    /**
     * @var
     */
    private $taxable_income;
    /**
     * @var
     */
    private $net_salary;
    /**
     * @var
     */
    private $net_income_tax;
    /**
     * @var
     */
    private $total_deduction;
    /**
     * @var
     */
    private $employee_pension;
    /**
     * @var
     */
    private $company_pension;
    /**
     * @var
     */
    private $pre_income_tax;
    /**
     * @var
     */
    private $new_income_tax;

    /**
     * CalculatePayment constructor.
     * @param $input
     * @param $setting
     */
    public function __construct($input, $setting)
    {

        $this->input = $input;
        $this->setting = $setting;
    }

    /**
     * @return $this
     */
    public function calculate()
    {
        $this->gross_pay()
             ->taxable_income()
             ->pre_income_tax()
             ->new_income_tax()
             ->company_pension()
             ->employee_pension()
             ->total_deduction()
             ->net_income_tax()
             ->net_salary();

        return $this;
    }

    /**
     * @return array
     */
    public function result()
    {
        return [
                'back_pay_payment_gross_pay'          => $this->gross_pay,
                'back_pay_payment_taxable_income'     => $this->taxable_income,
                'back_pay_payment_pre_income_tax'     => $this->pre_income_tax,
                'back_pay_payment_new_income_tax'     => $this->new_income_tax,
                'back_pay_payment_company_pension'    => $this->company_pension,
                'back_pay_payment_employee_pension'   => $this->employee_pension,
                'back_pay_payment_total_deduction'    => $this->total_deduction,
                'back_pay_payment_net_income_tax'     => $this->net_income_tax,
                'back_pay_payment_net_salary'         => $this->net_salary,
               ];
    }

    /**
     * @return $this
     */
    private function gross_pay()
    {
        $this->gross_pay = ((double) (($this->input['back_pay_new_salary'] +
                           $this->input['back_pay_new_taxable_incomes'] +
                           $this->input['back_pay_new_non_taxable_income']) -
                           ($this->input['back_pay_previous_salary'] +
                           $this->input['back_pay_previous_taxable_income'] +
                           $this->input['back_pay_previous_non_taxable_income']))) * $this->input['back_pay_no_of_months'];

        return $this;
    }

    /**
     * @return $this
     */
    private function taxable_income()
    {
        $this->taxable_income = (($this->input['back_pay_new_taxable_incomes'] + $this->input['back_pay_new_salary']) -
                                 ($this->input['back_pay_previous_taxable_income'] + $this->input['back_pay_previous_salary'])) *
                                 $this->input['back_pay_no_of_months'];
        return $this;
    }

    /**
     * @return $this
     */
    private function pre_income_tax()
    {
        $tax_rate = TaxRate::of(($this->input['back_pay_previous_taxable_income'] + $this->input['back_pay_previous_salary']), $this->setting);
        $this->pre_income_tax = ((((double)(($this->input['back_pay_previous_taxable_income'] + $this->input['back_pay_previous_salary']))) * ($tax_rate['percent']/100)) - $tax_rate['deduction_amount']) *
                                $this->input['back_pay_no_of_months'];
            return $this;
    }

    /**
     * @return $this
     */
    private function new_income_tax()
    {
        $tax_rate = TaxRate::of(($this->input['back_pay_new_taxable_incomes'] + $this->input['back_pay_new_salary']), $this->setting);
        $this->new_income_tax = ((((double)(($this->input['back_pay_new_taxable_incomes'] + $this->input['back_pay_new_salary']))) * ($tax_rate['percent']/100)) - $tax_rate['deduction_amount']) *
                                 $this->input['back_pay_no_of_months'];
        return $this;
    }

    /**
     * @return $this
     */
    private function company_pension()
    {
        $this->company_pension =  ((double) (($this->input['back_pay_new_salary'] - $this->input['back_pay_previous_salary']) * ($this->setting['company_pension_rate'] / 100) )) * $this->input['back_pay_no_of_months'];

            return $this;
    }

    /**
     * @return $this
     */
    private function employee_pension()
    {
        $this->employee_pension =((double) (($this->input['back_pay_new_salary'] - $this->input['back_pay_previous_salary']) * ($this->setting['employee_pension_rate'] / 100))) * $this->input['back_pay_no_of_months'];

            return $this;
    }

    /**
     * @return $this
     */
    private function total_deduction()
    {
        $this->total_deduction = ($this->new_income_tax - $this->pre_income_tax) + $this->employee_pension ;
            return $this;
    }

    /**
     * @return $this
     */
    private function net_income_tax()
    {
        $this->net_income_tax = (double) ($this->new_income_tax - $this->pre_income_tax);
            return $this;
    }

    /**
     * @return $this
     */
    private function net_salary()
    {
        $this->net_salary = (double) ($this->gross_pay - $this->total_deduction);

        return $this;
    }

}