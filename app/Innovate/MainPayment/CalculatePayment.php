<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE E-COMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 10/19/16
 * Time: 9:50 PM
 */

namespace Innovate\MainPayment;


/**
 * Class CalculatePayment
 * @package Innovate\MainPayment
 */
class CalculatePayment
{


    /**
     *
     */
    const OT_1_2_5 = 1.25;

    /**
     *
     */
    const OT_1_5 = 1.5;

    /**
     *
     */
    const OT_2_0 = 2.0;

    /**
     *
     */
    const OT_2_5 = 2.5;


    /**
     * @var array
     */
    protected $givenData = array();

    /**
     * @var
     */
    private $setting = array();

    /**
     * @var
     */
    private $total_ot;

    /**
     * @var
     */
    private $taxable_allowance;

    /**
     * @var
     */
    private $non_taxable_allowance;

    /**
     * @var
     */
    private $other_deduction;

    /**
     * @var
     */
    private $taxable_income;

    /**
     * @var
     */
    private $income_tax;

    /**
     * @var
     */
    private $company_pension;

    /**
     * @var
     */
    private $employee_pension;

    /**
     * @var
     */
    private $total_deduction;

    /**
     * @var
     */
    private $gross_pay;

    /**
     * @var
     */
    private $net_salary;


    /**
     * CalculatePayment constructor.
     * @param array $data
     * @param $setting
     */
    public function __construct($data, $setting)
    {
        $this->givenData = $data;
        $this->setting = $setting;

        return $this;
    }

    /**
     * @return $this
     */
    public function calculate()
    {
       $this->taxable_allowance()
            ->non_taxable_allowance()
            ->total_OT()
            ->taxable_income()
            ->income_tax()
            ->company_pension()
            ->employee_pension()
            ->other_deduction()
            ->total_deduction()
            ->gross_pay()
            ->net_salary();
        return $this;
    }

    /**
     * @return array
     */
    public function result()
    {
         return [
                    'main_payment_taxable_allowance'        =>$this->taxable_allowance,
                    'main_payment_non_taxable_allowance'    =>$this->non_taxable_allowance,
                    'main_payment_gross_pay'                =>$this->gross_pay,
                    'main_payment_total_ot'                 =>$this->total_ot,
                    'main_payment_taxable_income'           =>$this->taxable_income,
                    'main_payment_income_tax'               =>$this->income_tax,
                    'main_payment_company_pension'          =>$this->company_pension,
                    'main_payment_employee_pension'         =>$this->employee_pension,
                    'main_payment_other_deduction'          =>$this->other_deduction,
                    'main_payment_total_deduction'          =>$this->total_deduction,
                    'main_payment_net_salary'               =>$this->net_salary
                ];

    }


    /**
     * @return $this
     */
    private function taxable_allowance()
    {

      $this->taxable_allowance = $this->givenData['payroll_main_special_allowance'] +
            $this->givenData['payroll_main_house_allowance'] +
            $this->givenData['payroll_main_position_allowance'] +
            $this->givenData['payroll_main_transport_allowance'] +
            $this->givenData['payroll_main_daily_perdiem_allowance'] +
            $this->givenData['payroll_main_telephone_allowance'] +
            $this->givenData['payroll_main_representation_allowance'] +
            $this->givenData['payroll_main_ot_sub_allowance'] +
            $this->givenData['payroll_main_other_taxable_allowance'];
        return $this;
    }

    /**
     * @return $this
     */
    private function non_taxable_allowance()
    {
        $this->non_taxable_allowance = $this->givenData['payroll_main_transport_non_taxable_allowance'] +
            $this->givenData['payroll_main_desert_allowance'] +
            $this->givenData['payroll_main_representation_non_taxable_allowance'] +
            $this->givenData['payroll_main_other_non_taxable_allowance'];
        return $this;
    }

    /**
     * @return $this
     */
    private function total_OT()
    {

        $this->total_ot = ($this->salary_for_ot() * $this->ot_1_2_5()) + ($this->salary_for_ot() * $this->ot_1_5()) +
            ($this->salary_for_ot() * $this->ot_2_0()) + ($this->salary_for_ot() * $this->ot_2_5());
        return $this;
    }

    /**
     * @return $this
     */
    private function taxable_income()
    {
        $this->taxable_income = $this->taxable_allowance + $this->total_ot + $this->salary_per_month();
        return $this;
    }

    /**
     * @return $this
     */
    private function income_tax()
    {
        $tax_rate = TaxRate::of($this->taxable_income, $this->setting);
        $this->income_tax = (((double)($this->taxable_income)) * ($tax_rate['percent']/100)) - $tax_rate['deduction_amount'];
        return $this;
    }

    /**
     * @return $this
     */
    private function company_pension()
    {
        $this->company_pension = $this->salary_per_month() * ($this->setting['company_pension_rate'] / 100);
        return $this;
    }

    /**
     * @return $this
     */
    private function employee_pension()
    {
        $this->employee_pension = $this->salary_per_month() * ($this->setting['employee_pension_rate'] / 100);

        return $this;
    }

    /**
     * @return $this
     */
    private function other_deduction()
    {
        $this->other_deduction = $this->givenData['payroll_main_other_sport_deduction'] +
            $this->givenData['payroll_main_other_cost_sharing_deduction'] +
            $this->givenData['payroll_main_telephone_deduction'] +
            $this->givenData['payroll_main_other_deduction'];
        return $this;
    }

    /**
     * @return $this
     */
    private function total_deduction()
    {
        $this->total_deduction = $this->income_tax + $this->employee_pension + $this->other_deduction +
            $this->givenData['payroll_main_loan'] + $this->givenData['payroll_main_advance'];
        return $this;
    }

    /**
     * @return $this
     */
    private function gross_pay()
    {
        $this->gross_pay =  $this->salary_per_month() + $this->taxable_allowance + $this->non_taxable_allowance +
            $this->total_ot + $this->givenData['payroll_main_perdiem'];
        return $this;
    }

    /**
     * @return $this
     */
    private function net_salary()
    {
        $this->net_salary = $this->gross_pay - $this->total_deduction;
        return $this;
    }

    /**
     * @return float
     */
    private function ot_1_2_5()
    {
        return (double)$this->givenData['payroll_main_ot_1_25'] * (double)self::OT_1_2_5;
    }

    /**
     * @return float
     */
    private function ot_1_5()
    {
        return (double)$this->givenData['payroll_main_ot_1_5'] * (double)self::OT_1_5;
    }

    /**
     * @return float
     */
    private function ot_2_0()
    {
        return (double)$this->givenData['payroll_main_ot_2_0'] * (double)self::OT_2_0;
    }

    /**
     * @return float
     */
    private function ot_2_5()
    {
        return (double)$this->givenData['payroll_main_ot_2_5'] * (double)self::OT_2_5;
    }

    /**
     * @return float|int
     */
    private function salary_for_ot()
    {
        return $this->givenData['payroll_main_basic_salary'] / $this->setting['working_hour_per_month'];
    }

    /**
     * @return float|int
     */
    private function salary_per_day()
    {
        return $this->givenData['payroll_main_basic_salary'] / $this->setting['working_day'];
    }

    /**
     * @return float
     */
    private function salary_per_month()
    {
        return (double)($this->givenData['payroll_main_working_day'] * $this->salary_per_day());
    }

}