<?php

namespace Innovate\MainPayment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class MainPayroll
 * @package Innovate\Payroll
 */
class MainPayment extends Model
{

    use RevisionableTrait,SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'payroll_main_payment';

    /**
     * List of attributes that can be added
     * @var array
     */
    protected $fillable = [ 'main_payment_taxable_allowance','main_payment_non_taxable_allowance','main_payment_gross_pay',
                            'main_payment_total_ot','main_payment_taxable_income','main_payment_income_tax','main_payment_company_pension',
                            'main_payment_employee_pension','main_payment_other_deduction','main_payment_total_deduction',
                            'main_payment_net_salary','main_payment_seen','main_payment_approved','main_payment_seen','main_payment_approved'];


    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = false;


}
