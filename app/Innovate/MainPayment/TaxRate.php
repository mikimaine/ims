<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 10/19/16
 * Time: 10:39 PM
 */

namespace Innovate\MainPayment;


class TaxRate
{

    public static function of($salary,$setting)
    {
        foreach ($setting['tax_rates'] as $key => $value){
            if ((double)($salary) >= (double) ($value['min_amount']) &&
                (double) ($salary) <= (double) ($value['max_amount'])){
                return $value;
            }
        }

    }

}