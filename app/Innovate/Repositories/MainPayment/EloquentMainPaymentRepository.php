<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\MainPayment;


use App\Exceptions\GeneralException;
use Exception;
use Illuminate\Support\Facades\Auth;
use Innovate\MainPayment\CalculatePayment;
use Innovate\MainPayment\MainPayment;
use Innovate\Payroll\MainPayroll;
use Innovate\Repositories\BaseRepository;

class EloquentMainPaymentRepository extends BaseRepository implements MainPaymentContract
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'Innovate\MainPayment\MainPayment';

    /**
     * Model class corresponding to this repository
     */
    const MODEL = MainPayment::class;

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'back_pay_bank_account', 'back_pay_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'back_pay_bank_account', 'back_pay_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
            ->get();
    }

    public function attache($input, $payroll_id, $setting)
    {
        try {

            if ($this->createStub($input, $payroll_id, $setting)->save()) {
                return true;
            }
        } catch (Exception $e) {
            //Do things with the Error
        }
        throw new GeneralException('There was a problem creating this Payment Please try again!' . $e->getTraceAsString());

    }

    /**
     * @TODO
     *
     * this is a lazy solution .....
     * @param $input
     * @param $payroll_id
     * @param $setting
     * @return mixed
     */
    public function attacheUpdate($input, $payroll_id, $setting)
    {
        $payment = MainPayment::where('main_payroll_id',$payroll_id);
        $result = $this->mapPayment($input, $setting);
        try {
            if ($payment->update($result)) {
                return true;
            }
        } catch (Exception $e) {
            //Do things with the Error
        }

    }

    /**
     * Fluently map the user input to the database filed
     * @param $input
     *
     * @param $payroll_id
     * @param $setting
     * @return MainPayroll
     */
    protected function createStub($input, $payroll_id, $setting)
    {
        $result = $this->mapPayment($input, $setting);
        $payment = new $this->modelName();

        $payment->main_payment_taxable_allowance      = $result['main_payment_taxable_allowance'];
        $payment->main_payment_non_taxable_allowance  = $result['main_payment_non_taxable_allowance'];
        $payment->main_payment_gross_pay              = $result['main_payment_gross_pay'];
        $payment->main_payment_total_ot               = $result['main_payment_total_ot'];
        $payment->main_payment_taxable_income         = $result['main_payment_taxable_income'];
        $payment->main_payment_income_tax             = $result['main_payment_income_tax'];
        $payment->main_payment_company_pension        = $result['main_payment_company_pension'];
        $payment->main_payment_employee_pension       = $result['main_payment_employee_pension'];
        $payment->main_payment_other_deduction        = $result['main_payment_other_deduction'];
        $payment->main_payment_total_deduction        = $result['main_payment_total_deduction'];
        $payment->main_payment_net_salary             = $result['main_payment_net_salary'];
        $payment->main_payment_seen                   = 0;
        $payment->main_payment_approved               = 0;
        $payment->main_payroll_id                     = (integer)$payroll_id;
        $payment->user_id                             = Auth::user()->id;

        return $payment;
    }

    /**
     * Map Payment to Array()
     * @param $input
     * @param $setting
     * @return array
     */
    private function mapPayment($input, $setting)
    {
        $calculated = new CalculatePayment($input, $setting);
        $result = $calculated->calculate()
            ->result();
        return $result;
    }

}