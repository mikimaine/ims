<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:37 PM
 */

namespace Innovate\Repositories\MainPayment;


use Innovate\Repositories\BaseContract;

/**
 * Interface MainPaymentContract
 * @package Innovate\Repositories\MainPayment
 */
interface MainPaymentContract extends BaseContract {

    /**
     * @param $input
     * @param $payroll_id
     * @param $setting
     * @return mixed
     */
    public function attache($input, $payroll_id, $setting);

    /**
     * @param $input
     * @param $payroll_id
     * @param $setting
     * @return mixed
     */
    public function attacheUpdate($input, $payroll_id, $setting);

}