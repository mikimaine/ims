<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\PayrollSettings;


use Illuminate\Support\Facades\Auth;
use Innovate\Payroll\MainPayroll;
use Innovate\PayrollSetting\PayrollTaxRateSetting;
use Innovate\Repositories\BaseRepository;

class EloquentPayrollTaxRateSettingRepository extends BaseRepository implements PayrollTaxRateSettingContract
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'Innovate\PayrollSetting\PayrollTaxRateSetting';

    /**
     * Model class corresponding to this repository
     */
    const MODEL = PayrollTaxRateSetting::class;

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'title', 'min_amount', 'max_amount','percent','deduction_amount', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'title', 'min_amount', 'max_amount','percent','deduction_amount','user_id', 'created_at', 'updated_at', 'deleted_at'])
            ->with('user')->get();
    }


    /**
     * Fluently map the user input to the database filed
     * @param $input
     * @return MainPayroll
     */
    protected function createStub($input)
    {
        $payroll = new $this->modelName();

        $payroll->title                     = $input['title'];
        $payroll->min_amount                = $input['min_amount'];
        $payroll->max_amount                = $input['max_amount'];
        $payroll->percent                   = $input['percent'];
        $payroll->deduction_amount          = $input['deduction_amount'];
        $payroll->user_id                   = Auth::user()->id;

        return $payroll;
    }


}