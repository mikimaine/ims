<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\PayrollSettings;


use Illuminate\Support\Facades\Auth;
use Innovate\Payroll\MainPayroll;
use Innovate\PayrollSetting\PayrollDepartmentSetting;
use Innovate\Repositories\BaseRepository;

class EloquentDepartmentPayrollSettingRepository extends BaseRepository implements PayrollDepartmentSettingContract
{

    /**
     * Model class Name Space of the corresponding to this repository
     * @var string
     */
    protected $modelName = 'Innovate\PayrollSetting\PayrollDepartmentSetting';

    /**
     * Model class corresponding to this repository
     */
    const MODEL = PayrollDepartmentSetting::class;

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'project_code', 'project_name', 'description', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'project_code', 'project_name', 'description','user_id', 'created_at', 'updated_at', 'deleted_at'])
            ->with('user')->get();
    }


    /**
     * Fluently map the user input to the database filed
     * @param $input
     * @return MainPayroll
     */
    protected function createStub($input)
    {
        $payroll = new $this->modelName();

        $payroll->project_code      = $input['project_code'];
        $payroll->project_name      = $input['project_name'];
        $payroll->description       = $input['description'];
        $payroll->user_id = Auth::user()->id;

        return $payroll;
    }


}