<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:37 PM
 */

namespace Innovate\Repositories\PayrollSettings;


use Innovate\Repositories\BaseContract;

interface PayrollTaxRateSettingContract extends BaseContract {

}