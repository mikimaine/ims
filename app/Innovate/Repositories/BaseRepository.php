<?php
/**
 * Created by Miki Maine Amdu.
 * For : INNOVATE E-COMMERCE
 * User: MIKI$
 * Date: 4/24/2016
 * Time: 4:40 PM.
 */
namespace Innovate\Repositories;

use App\Exceptions\GeneralException;
use Exception;

/**
 * Class BaseRepository.
 */
class BaseRepository implements BaseContract
{
    /**
     * @var
     */
    protected $modelName;

    /**
     * Returns the instance of the Model.
     *
     * @return mixed
     */
    protected function getNewInstance()
    {
        $model = $this->modelName;

        return new $model();
    }

    /**
     * @return mixed
     */
    protected function getModelNameFromNamespace()
    {

        return  array_last(explode('.',str_replace('\\', '.', get_class($this->getNewInstance()))));
    }

    /**
     * @param $id
     *
     * @throws GeneralException
     *
     * @return mixed
     *
     * @internal param bool $withRoles
     */
    public function findOrThrowException($id)
    {
        $instance = $this->getNewInstance();
        $collection = $instance->find($id);
        if (!is_null($collection)) {
            return $collection;
        }
        throw new GeneralException('That '.$this->getModelNameFromNamespace().' does not exist.');
    }

    /**
     * @param  $per_page
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     *
     * @internal param $status
     */
    public function getPaginated($per_page, $order_by = 'id', $sort = 'asc')
    {
        $instance = $this->getNewInstance();

        return $instance->orderBy($order_by, $sort)->paginate($per_page);
    }

    /**
     * @param  $per_page
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     *
     * @internal param $status
     */
    public function Paginated($per_page, $order_by = 'id', $sort = 'asc')
    {
        // TODO: Implement Paginated() method.
    }

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        $instance = $this->getNewInstance();

        return $instance->orderBy($order_by, $sort)->get();
    }

    /**
     * @param $input
     *
     * @throws GeneralException
     *
     * @return mixed
     *
     * @internal param $roles
     */
    public function create($input)
    {
        try {

            if ( $this->createStub($input)->save()) {
                return true;
            }
        } catch (Exception $e) {
            //Do things with the Error
        }
        throw new GeneralException('There was a problem creating this '.$this->getModelNameFromNamespace().' Please try again!'.$e->getMessage());
    }

    /**
     * @param $id
     * @param $input
     *
     * @throws GeneralException
     *
     * @return mixed
     *
     * @internal param $roles
     */
    public function update($id, $input)
    {
        $collection = $this->findOrThrowException($id);

        if ($collection->update($input)) {
            return true;
        }
        throw new GeneralException('There was a problem updating this'.$this->getModelNameFromNamespace().'. Please try again.');
    }

    /**
     * @param $id
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function destroy($id)
    {

        if ($this->findOrThrowException($id)->delete()) {
            return true;
        }

        throw new GeneralException('There was a problem deleting this '.$this->getModelNameFromNamespace().'. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function delete($id)
    {
        if ($this->getNewInstance()->withTrashed()->find($id)->forceDelete()) {
            return true;
        }

        throw new GeneralException('There was a problem deleting permanently this '.$this->getModelNameFromNamespace().'. Please try again.');
    }


    public function restore($id)
    {
        if ($this->getNewInstance()->withTrashed()->find($id)->restore()) {
            return true;
        }

        throw new GeneralException('There was a problem restoring this '.$this->getModelNameFromNamespace().'. Please try again.');

    }

    /**
     * @return mixed
     */
    protected function query()
    {
        return call_user_func(static::MODEL.'::query');
    }
}
