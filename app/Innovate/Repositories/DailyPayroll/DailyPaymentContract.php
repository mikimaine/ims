<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 11/7/16
 * Time: 4:22 PM
 */

namespace Innovate\Repositories\DailyPayroll;

use Innovate\Repositories\BaseContract;

interface DailyPaymentContract extends BaseContract
{

    public function attache($input, $payroll_id, $setting);
}