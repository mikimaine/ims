<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\DailyPayroll;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Innovate\DailyPayroll\DailyPayroll;
use Innovate\Payroll\MainPayroll;
use Innovate\Repositories\BaseRepository;

class EloquentDailyPayrollRepository extends BaseRepository implements DailyPayrollContract {

    protected $modelName = 'Innovate\DailyPayroll\DailyPayroll';

    const MODEL  = DailyPayroll::class;
    /**
     * @var DailyPaymentContract
     */
    private $dailyPaymentContract;

    /**
     * EloquentDailyPayrollRepository constructor.
     * @param DailyPaymentContract $dailyPaymentContract
     */
    public function __construct(DailyPaymentContract $dailyPaymentContract)
    {

        $this->dailyPaymentContract = $dailyPaymentContract;
    }

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'daily_labour_bank_account', 'daily_labour_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'daily_labour_bank_account', 'daily_labour_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
               ->get();
    }

    /**
     * @return mixed
     */
    public function getAllEager()
    {
        return $this->query()->with('user','payment')->get();
    }
    /**
     * @param $input
     * @param $setting
     * @return mixed
     */
    public function attache($input, $setting)
    {
        DB::beginTransaction();
        $payroll = $this->createStub($input);
        if ($payroll ->save()) {
            if ($this->dailyPaymentContract->attache($input,$payroll->id,$setting))
            {
                DB::commit();
                return true;
            }else{
                DB::rollback();
            }

        }else{
            DB::rollback();
            throw new GeneralException('There was a problem creating this Main Payroll@PayrollRepository Please try again!');
        }
    }

    /**
     * @param $input
     *
     * @return MainPayroll
     */
    protected function createStub($input)
    {
        $payroll = new $this->modelName();
        $payroll->daily_labour_project_id           = $input['daily_labour_project_id'];
        $payroll->daily_labour_employee_id          = $input['daily_labour_employee_id'];
        $payroll->daily_labour_bank_account         = $input['daily_labour_bank_account'];
        $payroll->daily_labour_working_day          = $input['daily_labour_working_day'];
        $payroll->daily_labour_wage_per_day         = $input['daily_labour_wage_per_day'];
        $payroll->daily_labour_salary               = $input['daily_labour_salary'];
        $payroll->daily_labour_ot_1_25              = $input['daily_labour_ot_1_25'];
        $payroll->daily_labour_ot_1_5               = $input['daily_labour_ot_1_5'];
        $payroll->daily_labour_ot_2_0               = $input['daily_labour_ot_2_0'];
        $payroll->daily_labour_ot_2_5               = $input['daily_labour_ot_2_5'];
        $payroll->daily_labour_loan                 = $input['daily_labour_loan'];
        $payroll->daily_labour_penalty              = $input['daily_labour_penalty'];
        $payroll->user_id	                        = Auth::user()->id;

        return $payroll;
    }



}