<?php
/**
 * DOXA IT TECHNOLOGY PLC
 * Created For AFRO Payroll with INNOVATE ECOMMERCE FRAMEWORK.
 * Author: Miki Maine Amdu @MIKI_MAINE_AMDU
 * Date: 11/7/16
 * Time: 4:23 PM
 */

namespace Innovate\Repositories\DailyPayroll;


use App\Exceptions\GeneralException;
use Exception;
use Illuminate\Support\Facades\Auth;
use Innovate\DailyPayroll\CalculatePayment;
use Innovate\DailyPayroll\DailyPayment;
use Innovate\Repositories\BaseRepository;

/**
 * Class EloquentDailyPaymentRepository
 * @package Innovate\Repositories\DailyPayroll
 */
class EloquentDailyPaymentRepository extends BaseRepository implements DailyPaymentContract
{
    /**
     * @var string
     */
    protected $modelName = 'Innovate\DailyPayroll\DailyPayment';

    /**
     *
     */
    const MODEL  = DailyPayment::class;


    /**
     * Map Payment to Array()
     * @param $input
     * @param $setting
     * @return array
     */
    private function mapPayment($input, $setting)
    {
        $calculated = new CalculatePayment($input, $setting);
        $result = $calculated->calculate()
                             ->result();
        return $result;
    }


    public function attache($input, $payroll_id, $setting)
    {
        try {

            if ($this->createStub($input, $payroll_id, $setting)->save()) {
                return true;
            }
        } catch (Exception $e) {
            //Do things with the Error
        }
        throw new GeneralException('There was a problem creating this Daily Payment Please try again!' . $e->getTraceAsString());

    }

    private function createStub($input,$payroll_id,$setting)
    {
          $result = $this->mapPayment($input,$setting);
          $payment = new $this->modelName;
          $payment->daily_payment_total_ot         = $result['daily_payment_total_ot'] ;
          $payment->daily_payment_taxable_income   = $result['daily_payment_taxable_income'] ;
          $payment->daily_payment_income_tax       = $result['daily_payment_income_tax']  ;
          $payment->daily_payment_total_deduction  = $result['daily_payment_total_deduction']  ;
          $payment->daily_payment_net_salary       = $result['daily_payment_net_salary']  ;
          $payment->daily_payment_seen             = 0  ;
          $payment->daily_payment_approved         = 0  ;
          $payment->daily_payment_id               = (integer) $payroll_id ;
          $payment->user_id                        = Auth::user()->id ;

        return $payment;
    }
}