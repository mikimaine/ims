<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:37 PM
 */

namespace Innovate\Repositories\DailyPayroll;


use Innovate\Repositories\BaseContract;

interface DailyPayrollContract extends BaseContract {
    /**
     * @param $input
     * @param $setting
     * @return mixed
     */
    public function attache($input, $setting);


    /**
     * @return mixed
     */
    public function getAllEager();

}