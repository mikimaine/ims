<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\BackPayPayroll;


use App\Exceptions\GeneralException;
use Exception;
use Illuminate\Support\Facades\Auth;
use Innovate\BackPayPayroll\BackPayPayment;
use Innovate\BackPayPayroll\CalculatePayment;
use Innovate\Payroll\MainPayroll;
use Innovate\Repositories\BaseRepository;

class EloquentBackPayPaymentPayrollRepository extends BaseRepository implements BackPayPaymentPayrollContract  {

    protected $modelName = 'Innovate\BackPayPayroll\BackPayPayment';

    const MODEL  = BackPayPayment::class;

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'back_pay_bank_account', 'back_pay_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'back_pay_bank_account', 'back_pay_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
               ->get();
    }

    /**
     * Map Payment to Array()
     * @param $input
     * @param $setting
     * @return array
     */
    private function mapPayment($input, $setting)
    {
        $calculated = new CalculatePayment($input, $setting);
        $result = $calculated->calculate()
                             ->result();
        return $result;
    }


    public function attache($input, $payroll_id, $setting)
    {
        try {

            if ($this->createStub($input, $payroll_id, $setting)->save()) {
                return true;
            }
        } catch (Exception $e) {
            //Do things with the Error
        }
        throw new GeneralException('There was a problem creating this Back Pay Payment Please try again!' . $e->getMessage());

    }

    private function createStub($input,$payroll_id,$setting)
    {
        $result = $this->mapPayment($input,$setting);
        $payment = new $this->modelName;
        $payment->back_pay_payment_gross_pay        = $result['back_pay_payment_gross_pay'] ;
        $payment->back_pay_payment_taxable_income   = $result['back_pay_payment_taxable_income'] ;
        $payment->back_pay_payment_pre_income_tax   = $result['back_pay_payment_pre_income_tax']  ;
        $payment->back_pay_payment_new_income_tax   = $result['back_pay_payment_new_income_tax']  ;
        $payment->back_pay_payment_company_pension  = $result['back_pay_payment_company_pension']  ;
        $payment->back_pay_payment_employee_pension = $result['back_pay_payment_employee_pension']  ;
        $payment->back_pay_payment_total_deduction  = $result['back_pay_payment_total_deduction']  ;
        $payment->back_pay_payment_net_income_tax   = $result['back_pay_payment_net_income_tax']  ;
        $payment->back_pay_payment_net_salary       = $result['back_pay_payment_net_salary']  ;
        $payment->back_pay_payment_seen             = 0  ;
        $payment->back_pay_payment_approved         = 0  ;
        $payment->back_pay_payment_id               = (integer) $payroll_id ;
        $payment->user_id                           = Auth::user()->id ;

        return $payment;
    }
}