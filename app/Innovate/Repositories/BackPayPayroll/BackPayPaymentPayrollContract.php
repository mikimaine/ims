<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:37 PM
 */

namespace Innovate\Repositories\BackPayPayroll;


use Innovate\Repositories\BaseContract;

interface BackPayPaymentPayrollContract extends BaseContract {
    public function attache($input, $payroll_id, $setting);
}