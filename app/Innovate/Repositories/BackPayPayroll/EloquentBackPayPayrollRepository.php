<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\BackPayPayroll;


use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Innovate\BackPayPayroll\BackPayPayroll;
use Innovate\Payroll\MainPayroll;
use Innovate\Repositories\BaseRepository;

class EloquentBackPayPayrollRepository extends BaseRepository implements BackPayPayrollContract  {

    protected $modelName = 'Innovate\BackPayPayroll\BackPayPayroll';

    const MODEL  = BackPayPayroll::class;
    /**
     * @var BackPayPaymentPayrollContract
     */
    private $backPayPaymentPayrollContract;

    /**
     * EloquentBackPayPayrollRepository constructor.
     * @param BackPayPaymentPayrollContract $backPayPaymentPayrollContract
     */
    public function __construct(BackPayPaymentPayrollContract $backPayPaymentPayrollContract)
    {

        $this->backPayPaymentPayrollContract = $backPayPaymentPayrollContract;
    }


    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'back_pay_bank_account', 'back_pay_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'back_pay_bank_account', 'back_pay_employee_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
               ->get();
    }

    /**
     * @param $input
     * @param $setting
     * @return mixed
     * @throws GeneralException
     */
    public function attache($input, $setting)
    {
        DB::beginTransaction();
        $payroll = $this->createStub($input);
        if ($payroll ->save()) {
            if ($this->backPayPaymentPayrollContract->attache($input,$payroll->id,$setting))
            {
                DB::commit();
                return true;
            }else{
                DB::rollback();
            }

        }else{
            DB::rollback();
            throw new GeneralException('There was a problem creating this Back Pay Payroll@BackPayRepository Please try again!');
        }
    }

    /**
     * @return mixed
     */
    public function getAllEager()
    {
        return $this->query()->with('user','payment')->get();
    }

    /**
     * Fluently map the user input to the database filed
     * @param $input
     *
     * @return MainPayroll
     */
    protected function createStub($input)
    {
        $payroll = new $this->modelName();
        $payroll->back_pay_project_id                   = $input['back_pay_project_id'];
        $payroll->back_pay_employee_id                  = $input['back_pay_employee_id'];
        $payroll->back_pay_bank_account                 = $input['back_pay_bank_account'];
        $payroll->back_pay_permanent_employee           = $input['back_pay_permanent_employee'];
        $payroll->back_pay_no_of_months                 = $input['back_pay_no_of_months'];
        $payroll->back_pay_previous_salary              = $input['back_pay_previous_salary'];
        $payroll->back_pay_new_salary                   = $input['back_pay_new_salary'];
        $payroll->back_pay_previous_taxable_income      = $input['back_pay_previous_taxable_income'];
        $payroll->back_pay_new_taxable_incomes          = $input['back_pay_new_taxable_incomes'];
        $payroll->back_pay_previous_non_taxable_income  = $input['back_pay_previous_non_taxable_income'];
        $payroll->back_pay_new_non_taxable_income       = $input['back_pay_new_non_taxable_income'];
        $payroll->user_id	                            = Auth::user()->id;

        return $payroll;
    }


}