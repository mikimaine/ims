<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:37 PM
 */

namespace Innovate\Repositories\Payroll;


use Innovate\Repositories\BaseContract;

/**
 * Interface PayrollContract
 * @package Innovate\Repositories\Payroll
 */
interface PayrollContract extends BaseContract {

    /**
     * @param $input
     * @param $setting
     * @return mixed
     */
    public function attache($input, $setting);

    /**
     * @param $id
     * @param $input
     * @param $setting
     * @return mixed
     */
    public function attacheUpdate($id,$input, $setting);


    /**
     * @return mixed
     */
    public function getAllEager();

}