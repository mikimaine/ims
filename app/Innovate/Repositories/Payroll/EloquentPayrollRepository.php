<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:55 PM
 */

namespace Innovate\Repositories\Payroll;


use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Innovate\Payroll\MainPayroll;
use Innovate\Repositories\BaseRepository;
use Innovate\Repositories\MainPayment\MainPaymentContract;

class EloquentPayrollRepository extends BaseRepository implements PayrollContract{

    protected $modelName = 'Innovate\Payroll\MainPayroll';

    const MODEL  = MainPayroll::class;
    /**
     * @var MainPaymentContract
     */
    private $mainPaymentContract;

    /**
     * EloquentPayrollRepository constructor.
     * @param MainPaymentContract $mainPaymentContract
     */
    public function __construct(MainPaymentContract $mainPaymentContract )
    {

        $this->mainPaymentContract = $mainPaymentContract;
    }

    /**
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        if ($trashed == "true") {
            return $this->query()->onlyTrashed()
                ->select(['id', 'payroll_main_bank_account_no', 'payroll_main_permanent_employee', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
                ->get();
        }

        return $this->query()->select(['id', 'payroll_main_bank_account_no', 'payroll_main_permanent_employee', 'user_id', 'created_at', 'updated_at', 'deleted_at'])
               ->with('user','payment')->get();
    }

    public function getAllEager()
    {
       return $this->query()->with('user','payment')->get();
    }

    /**
     * @param $input
     * @param $setting
     * @return bool
     * @throws GeneralException
     */
    public function attache($input,$setting)
    {

        DB::beginTransaction();
            $payroll = $this->createStub($input);
            if ($payroll ->save()) {
                if ($this->mainPaymentContract->attache($input,$payroll->id,$setting))
                {
                    DB::commit();
                    return true;
                }else{
                    DB::rollback();
                }

            }else{
                DB::rollback();
                throw new GeneralException('There was a problem creating this Main Payroll@PayrollRepository Please try again!');
        }

    }

    /**
     * @param $id
     * @param $input
     * @param $setting
     * @return mixed
     * @throws GeneralException
     */
    public function attacheUpdate($id,$input, $setting)
    {
        DB::beginTransaction();
        $payroll = $this->findOrThrowException($id);
        if ($payroll->update($input)) {
            if ($this->mainPaymentContract->attacheUpdate($input,$payroll->id,$setting))
            {
                DB::commit();
                return true;
            }else{
                DB::rollback();
            }

        }else{
            DB::rollback();
            throw new GeneralException('There was a problem creating this Main Payroll@PayrollRepository Please try again!');
        }
    }
    /**
     * @param $input
     *
     * @return MainPayroll
     */
    protected function createStub($input)
    {
        $payroll = new $this->modelName();
        $payroll->payroll_main_payroll_project_id                   = $input['payroll_main_payroll_project_id'];
        $payroll->payroll_main_payroll_employee_id                  = $input['payroll_main_payroll_employee_id'];
        $payroll->payroll_main_bank_account_no                      = $input['payroll_main_bank_account_no'];
        $payroll->payroll_main_permanent_employee                   = $input['payroll_main_permanent_employee'];
        $payroll->payroll_main_working_day                          = $input['payroll_main_working_day'];
        $payroll->payroll_main_basic_salary                         = $input['payroll_main_basic_salary'];
        $payroll->payroll_main_perdiem                              = $input['payroll_main_perdiem'];
        $payroll->payroll_main_ot_1_25                              = $input['payroll_main_ot_1_25'];
        $payroll->payroll_main_ot_1_5                               = $input['payroll_main_ot_1_5'];
        $payroll->payroll_main_ot_2_0                               = $input['payroll_main_ot_2_0'];
        $payroll->payroll_main_ot_2_5                               = $input['payroll_main_ot_2_5'];
        $payroll->payroll_main_loan                                 = $input['payroll_main_loan'];
        $payroll->payroll_main_advance                              = $input['payroll_main_advance'];
        $payroll->payroll_main_telephone_deduction                  = $input['payroll_main_telephone_deduction'];
        $payroll->payroll_main_other_deduction                      = $input['payroll_main_other_deduction'];
        $payroll->payroll_main_other_cost_sharing_deduction         = $input['payroll_main_other_cost_sharing_deduction'];
        $payroll->payroll_main_other_sport_deduction                = $input['payroll_main_other_sport_deduction'];
        $payroll->payroll_main_special_allowance                    = $input['payroll_main_special_allowance'];
        $payroll->payroll_main_house_allowance                      = $input['payroll_main_house_allowance'];
        $payroll->payroll_main_position_allowance                   = $input['payroll_main_position_allowance'];
        $payroll->payroll_main_transport_allowance                  = $input['payroll_main_transport_allowance'];
        $payroll->payroll_main_daily_perdiem_allowance              = $input['payroll_main_daily_perdiem_allowance'];
        $payroll->payroll_main_telephone_allowance                  = $input['payroll_main_telephone_allowance'];
        $payroll->payroll_main_representation_allowance             = $input['payroll_main_representation_allowance'];
        $payroll->payroll_main_other_taxable_allowance              = $input['payroll_main_other_taxable_allowance'];
        $payroll->payroll_main_transport_non_taxable_allowance      = $input['payroll_main_transport_non_taxable_allowance'];
        $payroll->payroll_main_desert_allowance                     = $input['payroll_main_desert_allowance'];
        $payroll->payroll_main_representation_non_taxable_allowance	= $input['payroll_main_other_non_taxable_allowance'];
        $payroll->user_id	= Auth::user()->id;

        return $payroll;
    }
}