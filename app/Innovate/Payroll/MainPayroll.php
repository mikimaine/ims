<?php

namespace Innovate\Payroll;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Innovate\Payroll\Traits\Attribute\MainPayrollAttribute;
use Innovate\Payroll\Traits\Relationship\MainPayrollRelationship;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class MainPayroll
 * @package Innovate\Payroll
 */
class MainPayroll extends Model
{

    use MainPayrollAttribute,MainPayrollRelationship,RevisionableTrait,SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'payroll_main_payroll';


    /**
     * List of table attributes that can be mass assigned
     * @var array
     */
    protected $fillable = [ 'payroll_main_payroll_project_id'                   , 'payroll_main_payroll_employee_id',
                            'payroll_main_permanent_employee'                   , 'payroll_main_bank_account_no',
                            'payroll_main_working_day'                          ,'payroll_main_basic_salary',
                            'payroll_main_perdiem'                              ,'payroll_main_loan',
                            'payroll_main_advance'                              ,'payroll_main_telephone_deduction',
                            'payroll_main_other_deduction'                      ,'payroll_main_other_cost_sharing_deduction',
                            'payroll_main_other_sport_deduction'                ,'payroll_main_ot_1_25'              ,
                            'payroll_main_ot_1_5'                               ,'payroll_main_ot_2_0'               ,
                            'payroll_main_ot_2_5'                               ,'payroll_main_special_allowance'    ,
                            'payroll_main_house_allowance'                      ,'payroll_main_position_allowance'   ,
                            'payroll_main_transport_allowance'                  ,'payroll_main_daily_perdiem_allowance',
                            'payroll_main_telephone_allowance'                  ,'payroll_main_representation_allowance',
                            'payroll_main_ot_sub_allowance'                     ,'payroll_main_other_taxable_allowance',
                            'payroll_main_transport_non_taxable_allowance'      ,'payroll_main_desert_allowance'     ,
                            'payroll_main_representation_non_taxable_allowance' ,'payroll_main_other_non_taxable_allowance'
                          ];
    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled created revision on this model
     * @var bool
     */
    //protected $revisionCreationsEnabled = true;


}
