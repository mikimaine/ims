<?php
/**
 * Created by PhpStorm.
 * User: maine
 * Date: 10/11/16
 * Time: 1:12 PM
 */

namespace  Innovate\Payroll\Traits\Relationship;

use App\Models\Access\User\User;
use Innovate\MainPayment\MainPayment;

/**
 * Class MainPayrollRelationship
 * @package Innovate\Payroll\Traits\Relationship
 */
trait MainPayrollRelationship {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function payment()
    {
        return $this->belongsTo(MainPayment::class,'id','main_payroll_id');
    }
}