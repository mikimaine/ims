<?php

namespace Innovate\PayrollSetting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Innovate\BackPayPayroll\Traits\Attribute\BackPayPayrollAttribute;
use Innovate\BackPayPayroll\Traits\Relationship\BackPayPayrollRelationship;
use Innovate\PayrollSetting\Traits\Attribute\TaxRateSettingAttribute;
use Innovate\PayrollSetting\Traits\Relationship\SettingsRelationship;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class MainPayroll
 * @package Innovate\Payroll
 */
class PayrollTaxRateSetting extends Model
{

    use RevisionableTrait,SoftDeletes,SettingsRelationship,TaxRateSettingAttribute;

    /**
     * @var string
     */
    protected $table = 'payroll_tax_rate_settings';

    /**
     * List of attributes that can be added
     * @var array
     */
    protected $fillable = ['title','min_amount','max_amount','percent','deduction_amount'];


    /**
     * Cast result set from database
     * @var array
     */
    protected $casts    = [
                            'min_amount'        => 'double',
                            'max_amount'        => 'double',
                            'percent'           => 'double',
                            'deduction_amount'  => 'double',
                            ];


    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = false;


}
