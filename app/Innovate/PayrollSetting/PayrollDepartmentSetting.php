<?php

namespace Innovate\PayrollSetting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Innovate\PayrollSetting\Traits\Attribute\DepartmentSettingAttribute;
use Innovate\PayrollSetting\Traits\Relationship\SettingsRelationship;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class PayrollDepartmentSetting
 * @package Innovate\PayrollSetting
 */
class PayrollDepartmentSetting extends Model
{

    use RevisionableTrait,SoftDeletes,SettingsRelationship,DepartmentSettingAttribute;

    /**
     * @var string
     */
    protected $table = 'department_settings';

    /**
     * List of attributes that can be added
     * @var array
     */
    protected $fillable = ['project_code','project_name','description'];


    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = false;


}
