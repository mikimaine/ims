<?php

namespace Innovate\PayrollSetting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Innovate\PayrollSetting\Traits\Attribute\PayrollSettingAttribute;
use Innovate\PayrollSetting\Traits\Relationship\SettingsRelationship;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class MainPayroll
 * @package Innovate\Payroll
 */
class PayrollSetting extends Model
{

    use RevisionableTrait,SoftDeletes,SettingsRelationship,PayrollSettingAttribute;

    /**
     * @var string
     */
    protected $table = 'payroll_settings';

    /**
     * List of attributes that can be added
     * @var array
     */
    protected $fillable = ['title','value'];


    /**
     * For soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * This will also boot up
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Enabled revision on this model
     * @var bool
     */
    protected $revisionCreationsEnabled = false;


}
