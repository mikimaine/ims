<?php
/**
 * Created by PhpStorm.
 * User: maine
 * Date: 10/11/16
 * Time: 1:12 PM
 */

namespace  Innovate\PayrollSetting\Traits\Relationship;

use App\Models\Access\User\User;

/**
 * Class MainPayrollRelationship
 * @package Innovate\Payroll\Traits\Relationship
 */
trait SettingsRelationship {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


}