<?php

namespace Innovate\Requests\Payroll;

use App\Http\Requests\Request;

/**
 * Class StorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class StorePayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-main-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payroll_main_payroll_project_id'           => 'integer',
            'payroll_main_payroll_employee_id'          => 'integer',
            'payroll_main_permanent_employee'           => 'bool',
            'payroll_main_bank_account_no'              => 'min:1',
            'payroll_main_working_day'                  => 'integer|min:1|max:30',
            'payroll_main_basic_salary'                 => 'numeric|required',
            'payroll_main_perdiem'                      => 'numeric|min:0',
            'payroll_main_loan'                         => 'numeric|min:0',
            'payroll_main_advance'                      => 'numeric|min:0',
            'payroll_main_telephone_deduction'          => 'numeric|min:0',
            'payroll_main_other_deduction'              => 'numeric|min:0',
            'payroll_main_other_cost_sharing_deduction' => 'numeric|min:0',
            'payroll_main_other_sport_deduction'        => 'numeric|min:0',
            'payroll_main_ot_1_25'                      => 'integer|min:0',
            'payroll_main_ot_1_5'                       => 'integer|min:0',
            'payroll_main_ot_2_0'                       => 'integer|min:0',
            'payroll_main_ot_2_5'                       => 'integer|min:0',
            'payroll_main_special_allowance'            => 'numeric|min:0',
            'payroll_main_house_allowance'              => 'numeric|min:0',
            'payroll_main_position_allowance'           => 'numeric|min:0',
            'payroll_main_transport_allowance'          => 'numeric|min:0',
            'payroll_main_daily_perdiem_allowance'      => 'numeric|min:0',
            'payroll_main_telephone_allowance'          => 'numeric|min:0',
            'payroll_main_representation_allowance'     => 'numeric|min:0',
            'payroll_main_ot_sub_allowance'             => 'numeric|min:0',
            'payroll_main_other_taxable_allowance'      => 'numeric|min:0',
            'payroll_main_transport_non_taxable_allowance'      => 'numeric|min:0',
            'payroll_main_desert_allowance'             => 'numeric|min:0',
            'payroll_main_representation_non_taxable_allowance' => 'numeric|min:0',
            'payroll_main_other_non_taxable_allowance'  => 'numeric|min:0',
        ];
    }

}
