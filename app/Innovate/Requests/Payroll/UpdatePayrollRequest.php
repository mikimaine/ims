<?php

namespace Innovate\Requests\Payroll;

use App\Http\Requests\Request;

/**
 * Class StorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class UpdatePayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-main-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payroll_main_payroll_project_id'           => 'integer',
            'payroll_main_payroll_employee_id'          => 'integer',
            'payroll_main_permanent_employee'           => 'bool',
            'payroll_main_bank_account_no'              => 'integer',
            'payroll_main_working_day'                  => 'integer',
            'payroll_main_basic_salary'                 => 'integer',
            'payroll_main_perdiem'                      => 'integer',
            'payroll_main_loan'                         => 'integer',
            'payroll_main_advance'                      => 'integer',
            'payroll_main_telephone_deduction'          => 'integer',
            'payroll_main_other_deduction'              => 'integer',
            'payroll_main_other_cost_sharing_deduction' => 'integer',
            'payroll_main_other_sport_deduction'        => 'integer',
            'payroll_main_ot_1_25'                      => 'integer',
            'payroll_main_ot_1_5'                       => 'integer',
            'payroll_main_ot_2_0'                       => 'integer',
            'payroll_main_ot_2_5'                       => 'integer',
            'payroll_main_special_allowance'            => 'integer',
            'payroll_main_house_allowance'              => 'integer',
            'payroll_main_position_allowance'           => 'integer',
            'payroll_main_transport_allowance'          => 'integer',
            'payroll_main_daily_perdiem_allowance'      => 'integer',
            'payroll_main_telephone_allowance'          => 'integer',
            'payroll_main_representation_allowance'     => 'integer',
            'payroll_main_ot_sub_allowance'             => 'integer',
            'payroll_main_other_taxable_allowance'      => 'integer',
            'payroll_main_transport_non_taxable_allowance'      => 'integer',
            'payroll_main_desert_allowance'             => 'integer',
            'payroll_main_representation_non_taxable_allowance' => 'integer',
            'payroll_main_other_non_taxable_allowance'  => 'integer',
        ];
    }
}
