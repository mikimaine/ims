<?php

namespace Innovate\Requests\Payroll;

use App\Http\Requests\Request;

/**
 * Class StorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class DestroyPayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('destroy-main-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [  ];
    }
}
