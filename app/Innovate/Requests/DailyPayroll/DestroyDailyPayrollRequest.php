<?php

namespace Innovate\Requests\DailyPayroll;

use App\Http\Requests\Request;

/**
 * Class StorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class DestroyDailyPayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('destroy-daily-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [  ];
    }
}
