<?php

namespace Innovate\Requests\DailyPayroll;

use App\Http\Requests\Request;

/**
 * Class StorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class UpdateDailyPayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('update-daily-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'daily_labour_project_id'     => 'integer',
            'daily_labour_employee_id'    => 'integer',
            'daily_labour_bank_account'   => 'integer',
            'daily_labour_working_day'    => 'integer',
            'daily_labour_wage_per_day'   => 'integer',
            'daily_labour_salary'         => 'integer',
            'daily_labour_loan'           => 'integer',
            'daily_labour_penalty'        => 'integer',
            'daily_labour_ot_1_25'        => 'integer',
            'daily_labour_ot_1_5'         => 'integer',
            'daily_labour_ot_2_0'         => 'integer',
            'daily_labour_ot_2_5'         => 'integer'
        ];
    }
}
