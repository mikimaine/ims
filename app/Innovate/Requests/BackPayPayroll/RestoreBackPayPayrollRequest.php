<?php

namespace Innovate\Requests\BackPayPayroll;

use App\Http\Requests\Request;

/**
 * Class RestorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class RestoreBackPayPayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('restore-back-pay-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [  ];
    }
}
