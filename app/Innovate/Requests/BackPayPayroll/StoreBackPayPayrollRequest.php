<?php

namespace Innovate\Requests\BackPayPayroll;

use App\Http\Requests\Request;

/**
 * Class StorePayrollRequest
 * @package Innovate\Requests\Payroll
 */
class StoreBackPayPayrollRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-back-pay-payroll');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'back_pay_project_id' =>'integer',
           'back_pay_employee_id' => 'integer',
           'back_pay_bank_account'=>'required',
           'back_pay_permanent_employee'=> 'bool',
           'back_pay_no_of_months' => 'integer',
           'back_pay_previous_salary' => 'integer',
           'back_pay_new_salary'=> 'integer',
           'back_pay_previous_taxable_income' => 'integer',
           'back_pay_new_taxable_incomes' => 'integer',
           'back_pay_previous_non_taxable_income' => 'integer',
           'back_pay_new_non_taxable_income'=> 'integer'
        ];
    }
}
