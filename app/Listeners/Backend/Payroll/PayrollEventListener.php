<?php

namespace App\Listeners\Backend\Payroll;

/**
 * Class UserEventListener
 * @package App\Listeners\Backend\Access\User
 */
class PayrollEventListener
{
  /**
   * @var string
   */
  private $history_slug = 'Payroll';

  /**
   * @param $event
   */
  public function onCreated($event) {
    history()->log(
      $this->history_slug,
      'Payroll has been created with  <strong>'.$event->payroll->name.'</strong>',
      $event->user->id,
      'plus',
      'bg-green'
    );
  }


  public function subscribe($events)
  {

      $events->listen(
      \App\Events\Backend\Payroll\PayrollCreated::class,
      'App\Listeners\Backend\Payroll\PayrollEventListener@onCreated'
    );

  }

}
