<?php

namespace App\Events\Backend\Payroll;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated
 * @package App\Events\Backend\Access\User
 */
class PayrollCreated extends Event
{
	use SerializesModels;

	/**
	 * @var $user
	 */
	public $payroll;

	/**
	 * @param $user
	 */
	public function __construct($payroll)
	{
		$this->payroll = $payroll;
	}
}
