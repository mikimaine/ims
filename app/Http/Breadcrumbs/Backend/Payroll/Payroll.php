<?php

/**
 * For main Payroll
 */
Breadcrumbs::register('admin.payroll.main_payroll.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('payroll.menus.payroll_management'), route('admin.payroll.main_payroll.index'));
});

Breadcrumbs::register('admin.payroll.main_payroll.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.payroll.main_payroll.index');
    $breadcrumbs->push(trans('menus.backend.access.users.deactivated'), route('admin.payroll.main_payroll.index'));
});

Breadcrumbs::register('admin.payroll.main_payroll.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.payroll.main_payroll.index');
    $breadcrumbs->push(trans('menus.backend.access.users.create'), route('admin.payroll.main_payroll.create'));
});

Breadcrumbs::register('admin.payroll.main_payroll.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.payroll.main_payroll.index');
    $breadcrumbs->push(trans('menus.backend.access.users.edit'), route('admin.payroll.main_payroll.edit', $id));
});

/**
 * For Daily Payroll
 */
Breadcrumbs::register('admin.daily-payroll.daily_payroll.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('payroll.menus.daily_payroll_management'), route('admin.daily-payroll.daily_payroll.index'));
});

Breadcrumbs::register('admin.daily-payroll.daily_payroll.deleted', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.daily-payroll.daily_payroll.index');
    $breadcrumbs->push(trans('payroll.menus.deactivated_daily_payroll_management'), route('admin.daily-payroll.daily_payroll.index'));
});

Breadcrumbs::register('admin.daily-payroll.daily_payroll.create', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.daily-payroll.daily_payroll.index');
    $breadcrumbs->push(trans('daily_payroll.menus.daily_payroll.create'), route('admin.daily-payroll.daily_payroll.create'));
});

Breadcrumbs::register('admin.daily-payroll.daily_payroll.edit', function ($breadcrumbs, $id) {

    $breadcrumbs->parent('admin.daily-payroll.daily_payroll.index');
    $breadcrumbs->push(trans('daily_payroll.menus.daily_payroll.edit'), route('admin.daily-payroll.daily_payroll.edit', $id));
});

/**
 * For Back Pay Payroll
 */
Breadcrumbs::register('admin.back-pay-payroll.back_pay_payroll.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('payroll.menus.daily_payroll_management'), route('admin.back-pay-payroll.back_pay_payroll.index'));
});

Breadcrumbs::register('admin.back-pay-payroll.back_pay_payroll.deleted', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.back-pay-payroll.back_pay_payroll.index');
    $breadcrumbs->push(trans('payroll.menus.deactivated_daily_payroll_management'), route('admin.back-pay-payroll.back_pay_payroll.index'));
});

Breadcrumbs::register('admin.back-pay-payroll.back_pay_payroll.create', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.back-pay-payroll.back_pay_payroll.index');
    $breadcrumbs->push(trans('daily_payroll.menus.daily_payroll.create'), route('admin.back-pay-payroll.back_pay_payroll.create'));
});

Breadcrumbs::register('admin.back-pay-payroll.back_pay_payroll.edit', function ($breadcrumbs, $id) {

    $breadcrumbs->parent('admin.back-pay-payroll.back_pay_payroll.index');
    $breadcrumbs->push(trans('daily_payroll.menus.back_pay_payroll.edit'), route('admin.back-pay-payroll.back_pay_payroll.edit', $id));
});


