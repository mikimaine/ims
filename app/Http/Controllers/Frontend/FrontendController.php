<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller
{
    use AuthenticatesUsers;

	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
//        if (access()->allow('view-backend')) {
//            return redirect()->route('frontend.user.dashboard');
//        }
//
//        return redirect()->route('frontend.auth.login');

        return view('frontend.home.index');
	}

    /**
     * @return \Illuminate\View\View
     */
    public function about()
    {
        return view('frontend.home.about');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function services()
    {
        return view('frontend.home.services');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function contact()
    {
        return view('frontend.home.contact');
    }
	/**
	 * @return \Illuminate\View\View
	 */
	public function macros()
	{
		//return view('frontend.macros');
	}
}
