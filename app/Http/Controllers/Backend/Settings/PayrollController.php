<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Innovate\Requests\Payroll\StorePayrollRequest;

/**
 * This class is responsible for Main Payroll Only
 *
 * Class PayrollController
 * @package App\Http\Controllers\Backend\Payroll
 */
class PayrollController extends Controller
{
    /**
     * @var PayrollContract
     */
    private $payrollSetting;

    /**
     * PayrollController constructor.
     * @param PayrollSettingContract $payrollSetting
     */
    public function __construct(PayrollSettingContract $payrollSetting)
    {

        $this->payrollSetting = $payrollSetting;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json($this->payrollSetting->getAllEager(), 200);
        }

        return view('backend.settings.index_payroll');
    }

    /**
     * Displays create for for main Payroll
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
       return view('backend.settings.create_payroll');
    }

    /**
     * Send's the input data to the appropriate contact(interface) to be persisted to the database
     *
     * @param Request|StorePayrollRequest $request
     * @return mixed
     */
    public function store(Request $request)
    {

        $this->payrollSetting->create($request->all());
        return redirect()->route('admin.settings.payroll_settings.index')
            ->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function edit($id, Request $request)
    {
        return view('backend.settings.edit_payroll')->withPayroll($this->payrollSetting->findOrThrowException((int)$id));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $this->payrollSetting->update($id, $request->all());
        return redirect()->route('admin.settings.payroll_settings.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {
        // dd($id);
        $this->payrollSetting->destroy($id);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    /**
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function getDeleted(ManagePayrollRequest $request)
    {
        return view('backend.payroll.deleted');
    }


}
