<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Yajra\Datatables\Facades\Datatables;

/**
 *
 *
 * Class PayrollTableController
 * @package App\Http\Controllers\Backend\Payroll
 */
class PayrollDepartmentTableController extends Controller
{
    /**
     * @var PayrollContract
     */
    protected $payrolls;

    /**
     * @param PayrollDepartmentSettingContract $payrolls
     */
    public function __construct(PayrollDepartmentSettingContract $payrolls)
    {
        $this->payrolls = $payrolls;
    }

    /**
     * Invokes the the repository to get the DataTable data
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function __invoke(ManagePayrollRequest $request) {
        return Datatables::of($this->payrolls->getForDataTable($request->get('trashed')))
            ->addColumn('name', function($payroll) {
              //  dd($payroll);
                return $payroll->user->name;
            })
            ->addColumn('actions', function($payroll) {
                return $payroll->action_buttons;
            })
            ->make(true);
    }

}
