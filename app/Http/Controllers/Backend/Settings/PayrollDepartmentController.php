<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Innovate\Requests\Payroll\StorePayrollRequest;

/**
 * This class is responsible for Main Payroll Only
 *
 * Class PayrollController
 * @package App\Http\Controllers\Backend\Payroll
 */
class PayrollDepartmentController extends Controller
{
    /**
     * @var PayrollContract
     */
    private $payrollDepartmentContract;

    /**
     * PayrollController constructor.
     * @param PayrollDepartmentSettingContract $payrollDepartmentContract
     */
    public function __construct(PayrollDepartmentSettingContract $payrollDepartmentContract)
    {

        $this->payrollDepartmentContract = $payrollDepartmentContract;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json($this->payrollDepartmentContract->getAllEager(), 200);
        }

        return view('backend.settings.index_department');
    }

    /**
     * Displays create for for main Payroll
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
       return view('backend.settings.create_department');
    }

    /**
     * Send's the input data to the appropriate contact(interface) to be persisted to the database
     *
     * @param Request|StorePayrollRequest $request
     * @return mixed
     */
    public function store(Request $request)
    {

        $this->payrollDepartmentContract->create($request->all());
        return redirect()->route('admin.settings.department_Settings.index')
            ->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function edit($id, Request $request)
    {
        return view('backend.settings.edit_department')->withPayroll($this->payrollDepartmentContract->findOrThrowException((int)$id));

    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $this->payrollDepartmentContract->update($id, $request->all());
        return redirect()->route('admin.settings.department_settings.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {

        // dd($id);
        $this->payrollDepartmentContract->destroy($id);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    /**
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function getDeleted(ManagePayrollRequest $request)
    {
        return view('backend.payroll.deleted');
    }


}
