<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Yajra\Datatables\Facades\Datatables;

/**
 *
 *
 * Class PayrollTableController
 * @package App\Http\Controllers\Backend\Payroll
 */
class TaxRateTableController extends Controller
{
    /**
     * @var PayrollContract
     */
    protected $payrolls;

    /**
     * @param PayrollTaxRateSettingContract $payrolls
     */
    public function __construct(PayrollTaxRateSettingContract $payrolls)
    {
        $this->payrolls = $payrolls;
    }

    /**
     * Invokes the the repository to get the DataTable data
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function __invoke(ManagePayrollRequest $request) {
        return Datatables::of($this->payrolls->getForDataTable($request->get('trashed')))
            ->addColumn('name', function($payroll) {
                //dd($payroll);
                return $payroll->user->name;
            })
            ->addColumn('actions', function($payroll) {
                return $payroll->action_buttons;
            })
            ->make(true);
    }

}
