<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Innovate\Requests\Payroll\StorePayrollRequest;

/**
 * This class is responsible for Main Payroll Only
 *
 * Class PayrollController
 * @package App\Http\Controllers\Backend\Payroll
 */
class TaxRateController extends Controller
{
    /**
     * @var PayrollContract
     */
    private $payrollTaxRateContract;

    /**
     * PayrollController constructor.
     * @param PayrollTaxRateSettingContract $payrollTaxRateContract
     */
    public function __construct(PayrollTaxRateSettingContract $payrollTaxRateContract)
    {

        $this->payrollTaxRateContract = $payrollTaxRateContract;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json($this->payrollTaxRateContract->getAllEager(), 200);
        }

        return view('backend.settings.index_tax_rate');
    }

    /**
     * Displays create for for main Payroll
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        return view('backend.settings.create_tax_rate');
    }

    /**
     * Send's the input data to the appropriate contact(interface) to be persisted to the database
     * @TODO
     * Here is a deliberate mistake just because im ignorant !!!!
     * This have to be refactored out of the controller. Should be moved to -->
     * The Repository or to a Util(Extractor) Class
     *
     * @param Request|StorePayrollRequest $request
     * @return mixed
     */
    public function store(Request $request)
    {

        if ($request->ajax()) {
            if ($request->user()) {
                try {
                    $send_data = array(
                        'title' => $request->title
                    );
                    if (is_array($request->rates)) {
                        foreach ($request->rates as $key => $value) {
                            $send_data['deduction_amount'] = $value['deduction_amount'];
                            $send_data['max_amount'] = $value['max_amount'];
                            $send_data['min_amount'] = $value['min_amount'];
                            $send_data['percent'] = $value['percent'];
                            if ($send_data['deduction_amount'] != '' &&
                                $send_data['max_amount'] != '' &&
                                $send_data['min_amount'] != '' &&
                                $send_data['percent'] != ''
                            ) {
                                $this->payrollTaxRateContract->create($send_data);
                            }
                        }
                    } else {
                        $send_data['deduction_amount'] = $request->rates['deduction_amount'];
                        $send_data['max_amount'] = $request->rates['max_amount'];
                        $send_data['min_amount'] = $request->rates['min_amount'];
                        $send_data['percent'] = $request->rates['percent'];
                        $this->payrollTaxRateContract->create($send_data);
                    }
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                } catch (GeneralException $e) {
                    return response()->json([
                        'error' => [$e->getMessage()],
                    ], 402);
                }
            }
            return response()->json([
                'error' => 'Session Expired!!',
            ], 402);

        }
        // $this->payrollTaxRateContract->create($request->all());
        return redirect()->route('admin.settings.tax_rate_settings.index')
            ->withFlashSuccess('New Tax rate Setting created successfully!');
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function edit($id, Request $request)
    {
        return view('backend.settings.edit_tax_rate')->withPayroll($this->payrollTaxRateContract->findOrThrowException((int)$id));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $this->payrollTaxRateContract->update($id, $request->all());
        return redirect()->route('admin.settings.tax_rate_settings.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {
        // dd($id);
        $this->payrollTaxRateContract->destroy($id);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    /**
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function getDeleted(ManagePayrollRequest $request)
    {
        return view('backend.payroll.deleted');
    }


}
