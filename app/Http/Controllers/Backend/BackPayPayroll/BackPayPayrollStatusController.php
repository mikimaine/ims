<?php

namespace App\Http\Controllers\Backend\BackPayPayroll;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;


/**
 * Class UserStatusController
 */
class BackPayPayrollStatusController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $backPayPayrollContract;

	/**
	 * @param UserRepository $users
	 */
	public function __construct(BackPayPayrollContract $backPayPayrollContract)
	{
		$this->backPayPayrollContract = $backPayPayrollContract;
	}

	/**
	 * @param ManageUserRequest $request
	 * @return mixed
	 */
	public function getDeleted(ManageBackPayPayrollRequest $request)
	{
		return view('backend.back_pay_payroll.deleted');
	}

	/**
	 * @param User $deletedUser
	 * @param ManageUserRequest $request
	 * @return mixed
	 */
	public function delete($id, ManageBackPayPayrollRequest $request)
	{
		$this->backPayPayrollContract->delete($deletedUser);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted_permanently'));
	}

	/**
	 * @param User $deletedUser
	 * @param ManageUserRequest $request
	 * @return mixed
	 */
	public function restore($id, ManageBackPayPayrollRequest $request)
	{
		$this->backPayPayrollContract->restore($id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.restored'));
	}
}
