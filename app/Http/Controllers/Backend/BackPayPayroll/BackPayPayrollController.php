<?php

namespace App\Http\Controllers\Backend\BackPayPayroll;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Innovate\Repositories\BackPayPayroll\BackPayPayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract;
use Innovate\Requests\BackPayPayroll\StoreBackPayPayrollRequest;
use Innovate\Requests\BackPayPayroll\UpdateBackPayPayrollRequest;
use JavaScript;

/**
 * Class BackPayPayrollController
 * @package App\Http\Controllers\Backend\BackPayPayroll
 */
class BackPayPayrollController extends Controller
{

    /**
     * @var BackPayPayrollContract
     */
    private $backPayPayrollContract;
    /**
     * @var PayrollTaxRateSettingContract
     */
    private $taxRateSettingContract;
    /**
     * @var PayrollSettingContract
     */
    private $payrollSettingContract;
    /**
     * @var PayrollDepartmentSettingContract
     */
    private $departmentSettingContract;

    private $settings = array(
        'working_day' => 0,
        'working_hour_per_month' => 0,
        'company_pension_rate' => 0,
        'employee_pension_rate' => 0,
        'tax_rates' => 0
    );

    /**
     * BackPayPayrollController constructor.
     * @param BackPayPayrollContract $backPayPayrollContract
     * @param PayrollTaxRateSettingContract $taxRateSettingContract
     * @param PayrollSettingContract $payrollSettingContract
     * @param PayrollDepartmentSettingContract $departmentSettingContract
     * @internal param BackPayPayrollContract $backPayPayrollContractPayrollTaxRateSettingContract
     * @internal param BackPayPayrollContract $backPayPayrollContract
     */
    public function __construct(BackPayPayrollContract $backPayPayrollContract,PayrollTaxRateSettingContract $taxRateSettingContract,
                                PayrollSettingContract $payrollSettingContract,PayrollDepartmentSettingContract $departmentSettingContract)
    {

        $this->backPayPayrollContract = $backPayPayrollContract;
        $this->payrollSettingContract = $payrollSettingContract->getAll()->toArray();
        $this->buildSetting();
        $this->settings['tax_rates'] = $taxRateSettingContract->getAll()->toArray();
        $this->departmentSettingContract = $departmentSettingContract;
    }

    /**
     * Map the settings2
     * @TODO
     * We have to move it to a separate Object that will extract this settings
     * @return $this
     */
    public function buildSetting()
    {

        foreach ($this->payrollSettingContract as $setting)
        {
            foreach ($this->settings as $key => $value)
            {
                if ($key === $setting['title']){
                    $this->settings[$key] = $setting['value'];
                }
            }
        }
        return $this;
    }

/**
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json($this->backPayPayrollContract->getAllEager(), 200);
        }

        return view('backend.back_pay_payroll.index') ;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public
/**
 * @param Request $request
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
function create(Request $request)
    {
        /**
         * This keys will be available as variable for the frontend javascript framework (VUE.js, React.js, AngularJS)
         * Also for plain JS
         */
        JavaScript::put($this->settings);

        return view('backend.back_pay_payroll.create')
            ->withDepartments($this->departmentSettingContract->getAll());
    }

    /**
     * @param StoreBackPayPayrollRequest $request
     * @return mixed
     */
    public
/**
 * @param StoreBackPayPayrollRequest $request
 * @return mixed
 */
function store(StoreBackPayPayrollRequest $request)
    {
        if ($request->ajax()) {
            try {
                if ($this->backPayPayrollContract->attache($request->all(),$this->settings)){
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                }
            } catch (GeneralException $e) {
                return response()->json([
                    'error' => [$e->getMessage()],
                ], 402);
                //return  Response::json('{error:'.$e.'}', 402);
            }
        }

       // $this->backPayPayrollContract->create($request->all());
        return redirect()->route('admin.back-pay-payroll.back_pay_payroll.index')
            ->withFlashSuccess(trans('alerts.backend.users.created'));
    }

/**
 * @param $id
 * @param UpdateBackPayPayrollRequest $request
 * @return mixed
 */
public function edit($id, UpdateBackPayPayrollRequest $request)
    {
        $this->settings['payroll'] = $this->backPayPayrollContract->findOrThrowException((int)$id);
        /**
         * This keys will be available as variable for the frontend javascript framework (VUE.js, React.js, AngularJS)
         * Also for plain JS
         */
        JavaScript::put($this->settings);

        return view('backend.back_pay_payroll.edit')
               ->withPayroll($this->settings['payroll'])
               ->withDepartments($this->departmentSettingContract->getAll());;

    }

/**
 * @param $id
 * @param Request|UpdateBackPayPayrollRequest $request
 * @return mixed
 */
public function update($id, UpdateBackPayPayrollRequest $request)
    {
        if ($request->ajax()) {
            try {
                if ($this->backPayPayrollContract->update((int)$id,$request->all())){
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                }
            } catch (GeneralException $e) {
                return response()->json([
                    'error' => [$e->getMessage()],
                ], 402);
                //return  Response::json('{error:'.$e.'}', 402);
            }
        }

        ;

        return redirect()->route('admin.back-pay-payroll.back_pay_payroll.index')
                         ->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

/**
 * @param $id
 * @param Request $request
 * @return mixed
 */
public function destroy($id, Request $request)
    {
        $this->backPayPayrollContract->delete($id);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

/**
 * @param ManageUserRequest $request
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
public function getDeleted(ManageUserRequest $request)
    {
        return view('backend.back_pay.deleted');
    }
}
