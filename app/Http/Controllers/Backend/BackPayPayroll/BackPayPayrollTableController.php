<?php

namespace App\Http\Controllers\Backend\BackPayPayroll;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Innovate\Repositories\BackPayPayroll\BackPayPayrollContract;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Requests\BackPayPayroll\ManageBackPayPayrollRequest;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Yajra\Datatables\Facades\Datatables;

class BackPayPayrollTableController extends Controller
{
    /**
     * @var PayrollContract
     */
    protected $payrolls;

    /**
     * @param BackPayPayrollContract $payrolls
     */
    public function __construct(BackPayPayrollContract $payrolls)
    {
        $this->payrolls = $payrolls;
    }

    /**
     * Invokes the the repository to get the table data
     * @param ManageBackPayPayrollRequest $request
     * @return mixed
     */
    public function __invoke(ManageBackPayPayrollRequest $request) {
        return Datatables::of($this->payrolls->getForDataTable($request->get('trashed')))
            ->addColumn('name', function($payroll) {
                //dd($payroll->name);
                return $payroll->user->name;
            })
            ->editColumn('confirmed', function($payroll) {
                return $payroll->confirmed_label;
            })
            ->addColumn('actions', function($payroll) {
                return $payroll->action_buttons;
            })
            ->make(true);
    }

}
