<?php

namespace App\Http\Controllers\Backend\DailyPayroll;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Innovate\Repositories\DailyPayroll\DailyPayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract;
use Innovate\Requests\DailyPayroll\DestroyDailyPayrollRequest;
use Innovate\Requests\DailyPayroll\ManageDailyPayrollRequest;
use Innovate\Requests\DailyPayroll\UpdateDailyPayrollRequest;
use Innovate\Requests\Payroll\StorePayrollRequest;
use JavaScript;

class DailyPayrollController extends Controller
{
    /**
     * @var DailyPayrollContract
     */
    private $dailyPayrollContract;
    /**
     * @var PayrollDepartmentSettingContract
     */
    private $departmentSettingContract;
    /**
     * @var PayrollTaxRateSettingContract
     */
    private $taxRateSettingContract;
    /**
     * @var PayrollSettingContract
     */
    private $payrollSettingContract;

    private $settings = array(
        'working_day' => 0,
        'working_hour_per_month' => 0,
        'company_pension_rate' => 0,
        'employee_pension_rate' => 0,
        'tax_rates' => 0
    );

    /**
     * DailyPayrollController constructor.
     * @param DailyPayrollContract $dailyPayrollContract
     * @param PayrollTaxRateSettingContract $taxRateSettingContract
     * @param PayrollSettingContract $payrollSettingContract
     * @param PayrollDepartmentSettingContract $departmentSettingContract
     */
    public function __construct(DailyPayrollContract $dailyPayrollContract,PayrollTaxRateSettingContract $taxRateSettingContract,
                                PayrollSettingContract $payrollSettingContract,PayrollDepartmentSettingContract $departmentSettingContract )
    {

        $this->dailyPayrollContract = $dailyPayrollContract;
        $this->payrollSettingContract = $payrollSettingContract->getAll()->toArray();
        $this->buildSetting();
        $this->settings['tax_rates'] = $taxRateSettingContract->getAll()->toArray();
        $this->departmentSettingContract = $departmentSettingContract;
    }

    /**
     * Map the settings2
     * @TODO
     * We have to move it to a separate Object that will extract this settings
     * @return $this
     */
    public function buildSetting()
    {

        foreach ($this->payrollSettingContract as $setting)
        {
            foreach ($this->settings as $key => $value)
            {
                if ($key === $setting['title']){
                    $this->settings[$key] = $setting['value'];
                }
            }
        }
        return $this;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return response()->json($this->dailyPayrollContract->getAllEager(), 200);
        }

        return view('backend.daily_payroll.index');
    }

    /**
     * @param Request|StorePayrollRequest $request
     * @return mixed
     */
    public function create(Request $request)
    {
        /**
         * This keys will be available as variable for the frontend javascript framework (VUE.js, React.js, AngularJS)
         * Also for plain JS
         */
        JavaScript::put($this->settings);

        return view('backend.daily_payroll.create')
                   ->withDepartments($this->departmentSettingContract->getAll());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            try {
                if ($this->dailyPayrollContract->attache($request->all(),$this->settings)){
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                }
            } catch (GeneralException $e) {
                return response()->json([
                    'error' => [$e->getMessage()],
                ], 402);
                //return  Response::json('{error:'.$e.'}', 402);
            }
        }

        return redirect()->route('admin.daily-payroll.daily-payroll.index')
                         ->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param $id
     * @param UpdateDailyPayrollRequest $request
     * @return mixed
     */
    public function edit($id, UpdateDailyPayrollRequest $request)
    {
        $this->settings['payroll'] = $this->dailyPayrollContract->findOrThrowException((int)$id);
        /**
         * This keys will be available as variable for the frontend javascript framework (VUE.js, React.js, AngularJS)
         * Also for plain JS
         */
        JavaScript::put($this->settings);
        return view('backend.daily_payroll.edit')
                   ->withPayroll($this->settings['payroll'])
                   ->withDepartments($this->departmentSettingContract->getAll());
    }

    /**
     * @param $id
     * @param UpdateDailyPayrollRequest $request
     * @return mixed
     */
    public function update($id ,UpdateDailyPayrollRequest $request)
    {
        if ($request->ajax()) {
            try {
                if ($this->dailyPayrollContract->update((int)$id, $request->all())){
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                }
            } catch (GeneralException $e) {
                return response()->json([
                    'error' => [$e->getMessage()],
                ], 402);
                //return  Response::json('{error:'.$e.'}', 402);
            }
        }
        return redirect()->route('admin.daily-payroll.daily_payroll.index')
                         ->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param $id
     * @param DestroyDailyPayrollRequest $request
     * @return mixed
     */
    public function destroy($id, DestroyDailyPayrollRequest $request)
    {
        $this->dailyPayrollContract->destroy((int)$id);
        return redirect()->back()
                         ->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    /**
     * @param ManageDailyPayrollRequest $request
     * @return mixed
     */
    public function getDeleted(ManageDailyPayrollRequest $request)
    {
        return view('backend.daily_payroll.deleted');
    }

}
