<?php

namespace App\Http\Controllers\Backend\DailyPayroll;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use Innovate\Repositories\DailyPayroll\DailyPayrollContract;
use Innovate\Requests\DailyPayroll\ManageDailyPayrollRequest;

/**
 * Class UserStatusController
 */
class DailyPayrollStatusController extends Controller
{

    /**
     * @var DailyPayrollContract
     */
    private $dailyPayrollContract;

    /**
     * @param DailyPayrollContract $dailyPayrollContract
     */
	public function __construct(DailyPayrollContract $dailyPayrollContract)
	{

        $this->dailyPayrollContract = $dailyPayrollContract;
    }

    /**
     * @param ManageDailyPayrollRequest $request
     * @return mixed
     */
	public function getDeleted(ManageDailyPayrollRequest $request)
	{
		return view('backend.daily_payroll.deleted');
	}

	/**
	 * @param User $deletedUser
	 * @param ManageDailyPayrollRequest $request
	 * @return mixed
	 */
	public function delete($id, ManageDailyPayrollRequest $request)
	{
		$this->dailyPayrollContract->delete($id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted_permanently'));
	}

    /**
     * @param $id
     * @param ManageUserRequest $request
     * @return mixed
     */
	public function restore($id, ManageDailyPayrollRequest $request)
	{
		$this->dailyPayrollContract->restore((int)$id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.restored'));
	}
}
