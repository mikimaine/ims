<?php

namespace App\Http\Controllers\Backend\DailyPayroll;

use App\Http\Controllers\Controller;
use Innovate\Repositories\DailyPayroll\DailyPayrollContract;
use Innovate\Requests\DailyPayroll\ManageDailyPayrollRequest;
use Yajra\Datatables\Facades\Datatables;

class DailyPayrollTableController extends Controller
{
    /**
     * @var DailyPayrollContract
     */
    protected $payrolls;

    /**
     * @param DailyPayrollContract $payrolls
     */
    public function __construct(DailyPayrollContract $payrolls)
    {
        $this->payrolls = $payrolls;
    }

    /**
     * Invokes the the repository to get the table data
     * @param ManageDailyPayrollRequest $request
     * @return mixed
     */
    public function __invoke(ManageDailyPayrollRequest $request) {
        return Datatables::of($this->payrolls->getForDataTable($request->get('trashed')))
            ->addColumn('name', function($payroll) {
                //dd($payroll->name);
                return $payroll->user->name;
            })
            ->editColumn('confirmed', function($payroll) {
                return $payroll->confirmed_label;
            })
            ->addColumn('actions', function($payroll) {
                return $payroll->action_buttons;
            })
            ->make(true);
    }

}
