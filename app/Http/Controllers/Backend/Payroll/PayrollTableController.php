<?php

namespace App\Http\Controllers\Backend\Payroll;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Yajra\Datatables\Facades\Datatables;

/**
 *
 *
 * Class PayrollTableController
 * @package App\Http\Controllers\Backend\Payroll
 */
class PayrollTableController extends Controller
{
    /**
     * @var PayrollContract
     */
    protected $payrolls;

    /**
     * @param PayrollContract $payrolls
     */
    public function __construct(PayrollContract $payrolls)
    {
        $this->payrolls = $payrolls;
    }

    /**
     * Invokes the the repository to get the DataTable data
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function __invoke(ManagePayrollRequest $request) {
        return Datatables::of($this->payrolls->getForDataTable($request->get('trashed')))
            ->addColumn('name', function($payroll) {
                //dd($payroll->name);
                return $payroll->user->name;
            })
            ->addColumn('net_salary', function($payroll) {
                //dd($payroll->name);
                setlocale(LC_MONETARY, 'en_US');
                return number_format($payroll->payment->main_payment_net_salary,2,'.',',') . ' Birr';
            })
            ->addColumn('actions', function($payroll) {
                return $payroll->action_buttons;
            })
            ->make(true);
    }

}
