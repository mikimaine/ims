<?php

namespace App\Http\Controllers\Backend\Payroll;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Innovate\Requests\Payroll\StorePayrollRequest;
use JavaScript;

/**
 * This class is responsible for Main Payroll Only
 *
 * Class PayrollController
 * @package App\Http\Controllers\Backend\Payroll
 */
class PayrollReportController extends Controller
{
    /**
     * @var PayrollContract
     */
    private $payrollContract;
    /**
     * @var PayrollDepartmentSettingContract
     */
    private $departmentSettingContract;

    /**
     * PayrollController constructor.
     * @param PayrollContract $payrollContract
     * @param PayrollDepartmentSettingContract $departmentSettingContract
     */
    public function __construct(PayrollContract $payrollContract,PayrollDepartmentSettingContract $departmentSettingContract)
    {

        $this->payrollContract = $payrollContract;
        $this->departmentSettingContract = $departmentSettingContract;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index()
    {
        return view('backend.report.index')
            ->withDepartments($this->departmentSettingContract->getAll());
    }

    public function report_print()
    {
        return view('backend.report.print_view');
    }

    public function daily_report()
    {
        return view('backend.report.daily.index')
                    ->withDepartments($this->departmentSettingContract->getAll());
    }

    public function back_pay_report()
    {

        return view('backend.report.back_pay.index')
                    ->withDepartments($this->departmentSettingContract->getAll());
    }
    public function bank_pay_report()
    {

        return view('backend.report.bank_pay.index')
                    ->withDepartments($this->departmentSettingContract->getAll());
    }

}
