<?php

namespace App\Http\Controllers\Backend\Payroll;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Requests\Payroll\DeletePayrollRequest;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Innovate\Requests\Payroll\RestorePayrollRequest;

/**
 * Change / returns Main Payrolls with different status than active
 *
 * Class UserStatusController
 */
class PayrollStatusController extends Controller
{
	/**
	 * @var PayrollContract
	 */
	protected $payrolls;

    /**
     *
     * @param PayrollContract $payrolls
     */
	public function __construct(PayrollContract $payrolls)
	{
		$this->payrolls = $payrolls;
	}

    /**
     * @param ManagePayrollRequest $request
     * @return mixed
     */
	public function getDeleted(ManagePayrollRequest $request)
	{
		return view('backend.payroll.deleted');
	}

    /**
     * Permanently deletes record
     * @param $id
     * @param DeletePayrollRequest $request
     * @return mixed
     */
	public function delete($id, DeletePayrollRequest $request)
	{
		$this->payrolls->delete((int)$id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted_permanently'));
	}

    /**
     * @param User $id
     * @param ManagePayrollRequest|RestorePayrollRequest $request
     * @return mixed
     */
	public function restore($id, RestorePayrollRequest $request)
	{
		$this->payrolls->restore((int)$id);
		return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.restored'));
	}
}
