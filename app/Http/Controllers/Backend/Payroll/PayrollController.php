<?php

namespace App\Http\Controllers\Backend\Payroll;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Innovate\Repositories\Payroll\PayrollContract;
use Innovate\Repositories\PayrollSettings\PayrollDepartmentSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollSettingContract;
use Innovate\Repositories\PayrollSettings\PayrollTaxRateSettingContract;
use Innovate\Requests\Payroll\ManagePayrollRequest;
use Innovate\Requests\Payroll\StorePayrollRequest;
use JavaScript;

/**
 * This class is responsible for Main Payroll Only
 *
 * Class PayrollController
 * @package App\Http\Controllers\Backend\Payroll
 */
class PayrollController extends Controller
{
    /**
     * @var PayrollContract
     */
    private $payrollContract;
    /**
     * @var PayrollTaxRateSettingContract
     */
    private $taxRateSettingContract;


    private $settings = array(
                                'working_day' => 0,
                                'working_hour_per_month' => 0,
                                'company_pension_rate' => 0,
                                'employee_pension_rate' => 0,
                                'tax_rates' => 0
                            );
    /**
     * @var PayrollSettingContract
     */
    private $payrollSettingContract;
    /**
     * @var PayrollDepartmentSettingContract
     */
    private $departmentSettingContract;

    /**
     * PayrollController constructor.
     * @param PayrollContract $payrollContract
     * @param PayrollTaxRateSettingContract $taxRateSettingContract
     * @param PayrollSettingContract $payrollSettingContract
     * @param PayrollDepartmentSettingContract $departmentSettingContract
     */
    public function __construct(PayrollContract $payrollContract,PayrollTaxRateSettingContract $taxRateSettingContract,
                                PayrollSettingContract $payrollSettingContract,PayrollDepartmentSettingContract $departmentSettingContract )
    {
        $this->payrollContract = $payrollContract;
        $this->payrollSettingContract = $payrollSettingContract->getAll()->toArray();
        $this->buildSetting();
        $this->settings['tax_rates'] = $taxRateSettingContract->getAll()->toArray();
        $this->departmentSettingContract = $departmentSettingContract;
    }

    /**
     * Map the settings2
     * @TODO
     * We have to move it to a separate Object that will extract this settings
     * @return $this
     */
    public function buildSetting()
    {

        foreach ($this->payrollSettingContract as $setting)
        {
            foreach ($this->settings as $key => $value)
            {
                if ($key === $setting['title']){
                     $this->settings[$key] = $setting['value'];
                }
            }
        }
        return $this;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json($this->payrollContract->getAllEager(), 200);
        }

        return view('backend.payroll.index');
    }

    /**
     * Displays create form for main Payroll
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        /**
         * This keys will be available as variable for the frontend javascript framework (VUE.js, React.js, AngularJS)
         * Also for plain JS
         */
        JavaScript::put($this->settings);

        return view('backend.payroll.create')
                   ->withDepartments($this->departmentSettingContract->getAll());
    }

    /**
     * Send's the input data to the appropriate contact(interface) to be persisted to the database
     *
     * @param StorePayrollRequest $request
     * @return mixed
     */
    public function store(StorePayrollRequest $request)
    {
        if ($request->ajax()) {
            //  return $request->all();
            try {
                if ($this->payrollContract->attache($request->all(),$this->settings)){
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                }
            } catch (GeneralException $e) {
                return response()->json([
                    'error' => [$e->getMessage()],
                ], 402);
                //return  Response::json('{error:'.$e.'}', 402);
            }

        }
        //dd($request->all());
        return redirect()->route('admin.payroll.main_payroll.index')->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function edit($id, Request $request)
    {
        $this->settings['payroll'] =$this->payrollContract->findOrThrowException($id);

        /**
         * This keys will be available as variable for the frontend javascript framework (VUE.js, React.js, AngularJS)
         * Also for plain JS
         */
       // dd($this->settings['payroll']);
        JavaScript::put($this->settings);

        return view('backend.payroll.edit')
            ->withPayroll($this->settings['payroll'])
            ->withDepartments($this->departmentSettingContract->getAll());;
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {

        if ($request->ajax()) {
            //  return $request->all();
            try {
                if ($this->payrollContract->attacheUpdate($id,$request->all(),$this->settings)){
                    return response()->json([
                        'success' => ['message' => trans('alerts.backend.users.created')],
                    ], 200);
                }
            } catch (GeneralException $e) {
                return response()->json([
                    'error' => [$e->getMessage()],
                ], 402);
                //return  Response::json('{error:'.$e.'}', 402);
            }
        }
       // $this->payrollContract->update($id, $request->all());
        return redirect()->route('admin.payroll.main_payroll.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {
        $this->payrollContract->destroy($id);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    /**
     * @param ManagePayrollRequest $request
     * @return mixed
     */
    public function getDeleted(ManagePayrollRequest $request)
    {
        return view('backend.payroll.deleted');
    }


}
