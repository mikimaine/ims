<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollMainPayrollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create the schema
        Schema::create('payroll_main_payroll', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payroll_main_payroll_project_id')->default(NULL)->nullable();
            $table->integer('payroll_main_payroll_employee_id')->default(NULL)->nullable();
            $table->integer('payroll_main_bank_account_no')->default(NULL)->nullable();
            $table->boolean('payroll_main_permanent_employee');
            $table->integer('payroll_main_working_day')->default(0.00);
            $table->double('payroll_main_basic_salary',15,3)->default(0.00);
            $table->double('payroll_main_perdiem',15,3)->default(0.00);
            $table->integer('payroll_main_ot_1_25')->default(0);
            $table->integer('payroll_main_ot_1_5')->default(0);
            $table->integer('payroll_main_ot_2_0')->default(0);
            $table->integer('payroll_main_ot_2_5')->default(0);
            $table->double('payroll_main_loan', 15, 3)->default(0.00);
            $table->double('payroll_main_advance', 15, 3)->default(0.00);
            $table->double('payroll_main_telephone_deduction', 15, 3)->default(0.00);
            $table->double('payroll_main_other_deduction', 15, 3)->default(0.00);
            $table->double('payroll_main_other_cost_sharing_deduction', 15, 3)->default(0.00);
            $table->double('payroll_main_other_sport_deduction', 15, 3)->default(0.00);
            $table->double('payroll_main_special_allowance',15,3)->default(0.00);
            $table->double('payroll_main_house_allowance',15,3)->default(0.00);
            $table->double('payroll_main_position_allowance',15,3)->default(0.00);
            $table->double('payroll_main_transport_allowance',15,3)->default(0.00);
            $table->double('payroll_main_daily_perdiem_allowance',15,3)->default(0.00);
            $table->double('payroll_main_telephone_allowance',15,3)->default(0.00);
            $table->double('payroll_main_representation_allowance',15,3)->default(0.00);
            $table->double('payroll_main_ot_sub_allowance',15,3)->default(0.00);
            $table->double('payroll_main_other_taxable_allowance',15,3)->default(0.00);
            $table->double('payroll_main_transport_non_taxable_allowance',15,3)->default(0.00);
            $table->double('payroll_main_desert_allowance',15,3)->default(0.00);
            $table->double('payroll_main_representation_non_taxable_allowance',15,3)->default(0.00);
            $table->double('payroll_main_other_non_taxable_allowance',15,3)->default(0.00);
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();


//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_main_payroll');
    }
}
