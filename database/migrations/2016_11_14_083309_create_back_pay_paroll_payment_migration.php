<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackPayParollPaymentMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_back_pay_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->double('back_pay_payment_gross_pay', 15, 3)->default(0.00);
            $table->double('back_pay_payment_taxable_income', 15, 3)->default(0.00);
            $table->double('back_pay_payment_pre_income_tax', 15, 3)->default(0.00);
            $table->double('back_pay_payment_new_income_tax', 15, 3)->default(0.00);
            $table->double('back_pay_payment_company_pension', 15, 3)->default(0.00);
            $table->double('back_pay_payment_employee_pension', 15, 3)->default(0.00);
            $table->double('back_pay_payment_total_deduction', 15, 3)->default(0.00);
            $table->double('back_pay_payment_net_income_tax', 15, 3)->default(0.00);
            $table->double('back_pay_payment_net_salary', 15, 3)->default(0.00);
            $table->boolean('back_pay_payment_seen');
            $table->boolean('back_pay_payment_approved');
            $table->integer('back_pay_payment_id');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
//
//            $table->foreign('back_pay_payment_id')
//                ->references('id')
//                ->on('payroll_back_pay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_back_pay_payment');
    }
}
