<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollTaxRateSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_tax_rate_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->double('title');
            $table->double('min_amount');
            $table->double('max_amount');
            $table->double('percent');
            $table->double('deduction_amount');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_tax_rate_settings');
    }
}
