<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollBackPayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payroll_back_pay', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('back_pay_project_id')->default(NULL)->nullable();
            $table->integer('back_pay_employee_id')->default(NULL)->nullable();
            $table->integer('back_pay_bank_account')->default(NULL)->nullable();
            $table->boolean('back_pay_permanent_employee');
            $table->integer('back_pay_no_of_months')->default(0);
            $table->double('back_pay_previous_salary', 15, 3)->default(0.00);
            $table->double('back_pay_new_salary', 15, 3)->default(0.00);
            $table->double('back_pay_previous_taxable_income', 15, 3)->default(0.00);
            $table->double('back_pay_new_taxable_incomes', 15, 3)->default(0.00);
            $table->double('back_pay_previous_non_taxable_income', 15, 3)->default(0.00);
            $table->double('back_pay_new_non_taxable_income', 15, 3)->default(0.00);
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('payroll_back_pay');
    }
}
