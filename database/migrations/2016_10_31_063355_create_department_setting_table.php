<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('department_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_code');
            $table->string('project_name');
            $table->text('description');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
//            $table->foreign('user_id')
//                  ->references('id')
//                  ->on(config('access.users_table'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('department_settings');
    }
}
