<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollDailyPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_daily_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->double('daily_payment_total_ot', 15, 3)->default(0.00);
            $table->double('daily_payment_taxable_income', 15, 3)->default(0.00);
            $table->double('daily_payment_income_tax', 15, 3)->default(0.00);
            $table->double('daily_payment_total_deduction', 15, 3)->default(0.00);
            $table->double('daily_payment_net_salary', 15, 3)->default(0.00);
            $table->boolean('daily_payment_seen');
            $table->boolean('daily_payment_approved');
            $table->integer('daily_payment_id');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
//
//            $table->foreign('daily_payment_id')
//                ->references('id')
//                ->on('payroll_daily_labour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_daily_payment');
    }
}
