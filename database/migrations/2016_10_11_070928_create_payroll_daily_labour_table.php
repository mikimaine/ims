<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollDailyLabourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_daily_labour', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daily_labour_project_id')->default(NULL)->nullable();
            $table->integer('daily_labour_employee_id')->default(NULL)->nullable();
            $table->integer('daily_labour_bank_account')->default(NULL)->nullable();
            $table->integer('daily_labour_working_day')->default(0);
            $table->integer('daily_labour_wage_per_day')->default(0);
            $table->double('daily_labour_salary', 15, 3)->default(0.00);
            $table->integer('daily_labour_ot_1_25')->default(0);
            $table->integer('daily_labour_ot_1_5')->default(0);
            $table->integer('daily_labour_ot_2_0')->default(0);
            $table->integer('daily_labour_ot_2_5')->default(0);
            $table->double('daily_labour_loan', 15, 3)->default(0.00);
            $table->double('daily_labour_penalty', 15, 3)->default(0.00);
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('payroll_daily_labour');
    }
}
