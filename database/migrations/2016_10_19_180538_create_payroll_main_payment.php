<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollMainPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_main_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->double('main_payment_taxable_allowance', 15, 3)->default(0.00);
            $table->double('main_payment_non_taxable_allowance', 15, 3)->default(0.00);
            $table->double('main_payment_gross_pay', 15, 3)->default(0.00);
            $table->double('main_payment_total_ot', 15, 3)->default(0.00);
            $table->double('main_payment_taxable_income', 15, 3)->default(0.00);
            $table->double('main_payment_income_tax', 15, 3)->default(0.00);
            $table->double('main_payment_company_pension', 15, 3)->default(0.00);
            $table->double('main_payment_employee_pension', 15, 3)->default(0.00);
            $table->double('main_payment_other_deduction', 15, 3)->default(0.00);
            $table->double('main_payment_total_deduction', 15, 3)->default(0.00);
            $table->double('main_payment_net_salary', 15, 3)->default(0.00);
            $table->boolean('main_payment_seen');
            $table->boolean('main_payment_approved');
            $table->integer('main_payroll_id');
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();

//            $table->foreign('user_id')
//                ->references('id')
//                ->on(config('access.users_table'));
//
//            $table->foreign('main_payroll_id')
//                ->references('id')
//                ->on('payroll_main_payroll');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_main_payment');
    }
}
