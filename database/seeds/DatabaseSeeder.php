<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Innovate\PayrollSetting\PayrollSetting;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AccessTableSeeder::class);
        $this->call(HistoryTypeTableSeeder::class);
        $this->call(PayrollPermissionSeeder::class);
        $this->call(PayrollSetting::class);
        $this->call(TaxRateSettings::class);

        Model::reguard();
    }
}
