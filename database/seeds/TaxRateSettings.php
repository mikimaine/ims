<?php
/**
 *
 */
use Carbon\Carbon;
use Illuminate\Database\Seeder;

/**
 * Class TaxRateSettings
 */
class TaxRateSettings extends Seeder
{

    /**
     * The default Title
     */
    const DEFAULT_TAX_RATE_TITLE = 'Standard 2009';

    /**
     * The default creator of this settings
     */
    const DEFAULT_USER_ID = 1;

    /**
     * The Default Tax rate settings
     * @var array
     */
    private $map = array(
                        '0_600' => array(
                                        'min_amount' => 0,
                                        'max_amount' => 600,
                                        'percent'    => 0 ,
                                        'deduction_amount' => 0,
                                        ),
                        '601_1650' => array(
                                        'min_amount' => 601,
                                        'max_amount' => 1650,
                                        'percent'    => 10 ,
                                        'deduction_amount' => 60,
                                         ),
                        '1651_3200' => array(
                                        'min_amount' => 1651,
                                        'max_amount' => 3200,
                                        'percent'    => 15 ,
                                        'deduction_amount' => 143,
                                         ),
                        '3201_5250' => array(
                                        'min_amount' => 3201,
                                        'max_amount' => 5250,
                                        'percent'    => 20 ,
                                        'deduction_amount' => 303,
                                         ),
                        '5251_7800' => array(
                                        'min_amount' => 5251,
                                        'max_amount' => 7800,
                                        'percent'    => 25 ,
                                        'deduction_amount' => 565,
                                         ),
                        '7801_10900' => array(
                                        'min_amount' => 7801,
                                        'max_amount' => 10900,
                                        'percent'    => 30 ,
                                        'deduction_amount' => 955,
                                         ),
                        '10901_999999999' => array(
                                        'min_amount' => 10901,
                                        'max_amount' => 9999999999,
                                        'percent'    => 35,
                                        'deduction_amount' => 1500,
                                         )
                        );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->map as $value){
            $taxRate = new \Innovate\PayrollSetting\PayrollTaxRateSetting();
            $taxRate->title             = static::DEFAULT_TAX_RATE_TITLE;
            $taxRate->min_amount        = $value['min_amount'];
            $taxRate->max_amount        = $value['max_amount'];
            $taxRate->percent           = $value['percent'];
            $taxRate->deduction_amount  = $value['deduction_amount'];
            $taxRate->user_id           = static::DEFAULT_USER_ID;
            $taxRate->created_at        = Carbon::now();
            $taxRate->updated_at        = Carbon::now();
            $taxRate ->save();
        }

    }
}
