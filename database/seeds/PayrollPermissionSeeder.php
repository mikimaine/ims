<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

/**
 * Class PayrollPermissionSeeder
 */
class PayrollPermissionSeeder extends Seeder
{

    /**
     * Because there are four general permissions for user management
     * we will start this after that !!
     */
    const SORTING_START_AFTER_NUMBER = 4;

    /**
     * @var array
     */
    private $map = array(   'manage-payroll',
                            'create-main-payroll',
                            'update-main-payroll',
                            'destroy-main-payroll',
                            'delete-main-payroll',
                            'restore-main-payroll',
                            /**
                             *  Daily Payroll Permissions
                             */
                            'manage-daily-payroll',
                            'create-daily-payroll',
                            'update-daily-payroll',
                            'destroy-daily-payroll',
                            'delete-daily-payroll',
                            'restore-daily-payroll',
                            /**
                             *  Back Pay Payroll Permissions
                             */
                            'manage-back-pay-payroll',
                            'create-back-pay-payroll',
                            'update-back-pay-payroll',
                            'destroy-back-pay-payroll',
                            'delete-back-pay-payroll',
                            'restore-back-pay-payroll',
                            /**
                             * Settings Permissions
                             */
                            'manage-tax-rate-settings',
                            'manage-payroll-settings',


                        );


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Don't need to assign any permissions to administrator because the all flag is set to true
         * in RoleTableSeeder.php
         */

        /**
         * All Payroll Access Permissions
         */
        $permission_model = config('access.permission');

        /**
         * Just do the Seed!
         */
        foreach ($this->map as $key => $value) {
            $permission = new $permission_model;
            $permission->name = $value;
            $permission->display_name = $this->displayName($value);
            $permission->sort = $this->sortingKey($key);
            $permission->created_at = Carbon::now();
            $permission->updated_at = Carbon::now();
            $permission->save();
        }


    }

    /**
     * @param $key
     * @return mixed
     */
    private function sortingKey($key)
    {
        return $key + static::SORTING_START_AFTER_NUMBER;
    }

    /**
     * @param $value
     * @return string
     */
    private function displayName($value)
    {
        return ucwords(str_replace('-', ' ', $value));
    }
}
