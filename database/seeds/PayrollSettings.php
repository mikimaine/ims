<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;


/**
 * Seed Default Value for Payroll Module
 * Class PayrollSettings
 */
class PayrollSettings extends Seeder
{

    const DEFAULT_USER_ID = 1;

    private $map = array(
                         'working_day' =>26,
                         'working_hour_per_month' => 208,
                         'company_pension_rate'   => 11,
                         'employee_pension_rate'  => 7
                        );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        foreach ($this->map as $key => $value){

             $setting = new Innovate\PayrollSetting\PayrollSetting();
             $setting->title = $key;
             $setting->value = $value;
             $setting->user_id =  static::DEFAULT_USER_ID;
             $setting->created_at = Carbon::now();
             $setting->updated_at = Carbon::now();
             $setting->save();
        }

    }
}
