<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'IMC')">
    <meta name="author" content="@yield('meta_author', 'Miki Maine Amdu')">

    @yield('meta')

    <!-- Styles -->
    @yield('before-styles-end')

    {{ Html::style('theme/css/bootstrap.css') }}
    {{ Html::style('theme/css/modern-business.css') }}
    {{ Html::style('theme/font-awesome/css/font-awesome.min.css') }}



    @yield('after-styles-end')

<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>

<body>

        {{--<div id="app">--}}
            @include('includes.partials.logged-in-as')
            @include('frontend.includes.nav')

            {{--<div class="container">--}}
                @include('includes.partials.messages')
                @yield('content')
            {{--</div><!-- container -->--}}
        {{--</div><!--#app-->--}}






        <!-- Footer -->
        <div class="container">

            <hr>

            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; {{ app_name() }} 2017</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- Scripts -->
        @yield('before-scripts-end')
        {!! Html::script(elixir('theme/js/jquery-1.10.2.js')) !!}
        {!! Html::script(elixir('theme/js/bootstrap.js')) !!}
        {!! Html::script(elixir('theme/js/modern-business.js')) !!}
        @yield('after-scripts-end')

        @include('includes.partials.ga')
    </body>
</html>