@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management') . ' | ' . trans('payroll.label.create'))

@section('after-styles-end')
    {{ Html::style("css/backend/plugin/select2/select2.min.css") }}
@stop

@section('page-header')
    <h1>
        {{ trans('payroll.menus.main_payroll.management') }}
        <small>{{ trans('payroll.label.create') }}</small>
    </h1>
@endsection

@section('content')
    <div id="break_down" >
    <div class="col-md-9">
    {{ Form::open(['route' => 'admin.payroll.main_payroll.store', 'class' => 'form-horizontal','@submit.prevent'=>'submitForm', 'role' => 'form', 'method' => 'post']) }}
       @include('backend.payroll.includes.partials._form',[$pageName = 'create',$buttonText = trans('buttons.general.crud.create') ])
    {{ Form::close() }}
    </div>
        <div class="col-md-3 affix-top" data-spy="affix-right" >
                @include('backend.payroll.includes.partials._calculator_aside')
        </div>
    </div>
@stop

@section('after-scripts-end')
    {{ Html::script("js/backend/plugin/accounting/accounting.min.js") }}
    <script>
        /**
         * Setting for Currency Formatting
         * @type @{{ currency: {symbol: string, format: string, decimal: string, thousand: string, precision: number},
         *            number: {precision: number, thousand: string, decimal: string}}}
         */
        accounting.settings = {
            currency: {
                symbol : " ብር ",   // default currency symbol is '$'
                format: "%v%s", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal : ".",  // decimal point separator
                thousand: ",",  // thousands separator
                precision : 2   // decimal places
            },
            number: {
                precision : 0,  // default precision on numbers is 0
                thousand: ",",
                decimal : "."
            }
        }

        /**
         * There is a lot of  computation so please becarfull when you change something
         * @type {Vue}
         */

            var break_down = new Vue({
                el: '#break_down',
                //template: '#project-drop-down',
                data: {
                    payroll_main_payroll_project_id                  : 0,
                    payroll_main_payroll_employee_id                 : 0,
                    payroll_main_bank_account_no                     : '',
                    payroll_main_permanent_employee                  : 1,
                    payroll_main_working_day                         : working_day,
                    payroll_main_basic_salary                        : 0.00,
                    payroll_main_perdiem                             : 0.00,
                    payroll_main_loan                                : 0.00,
                    payroll_main_advance                             : 0.00,
                    payroll_main_telephone_deduction                 : 0.00,
                    payroll_main_other_deduction                     : 0.00,
                    payroll_main_other_cost_sharing_deduction        : 0.00,
                    payroll_main_other_sport_deduction               : 0.00,
                    payroll_main_ot_1_25                             : 0.00,
                    payroll_main_ot_1_5                              : 0.00,
                    payroll_main_ot_2_0                              : 0.00,
                    payroll_main_ot_2_5                              : 0.00,
                    payroll_main_special_allowance                   : 0.00,
                    payroll_main_house_allowance                     : 0.00,
                    payroll_main_position_allowance                  : 0.00,
                    payroll_main_transport_allowance                 : 0.00,
                    payroll_main_daily_perdiem_allowance             : 0.00,
                    payroll_main_telephone_allowance                 : 0.00,
                    payroll_main_representation_allowance            : 0.00,
                    payroll_main_ot_sub_allowance                    : 0.00,
                    payroll_main_other_taxable_allowance             : 0.00,
                    payroll_main_transport_non_taxable_allowance     : 0.00,
                    payroll_main_desert_allowance                    : 0.00,
                    payroll_main_representation_non_taxable_allowance: 0.00,
                    payroll_main_other_non_taxable_allowance         : 0.00,
                    formErrors: {},
                    selected: 0,
                    options: [],
                    optionsE: []

                },
                watch:{
                    payroll_main_payroll_employee_id: function (val) {
                         return this.get_employee_info(val);
                    }
                },
                created: function() {
                    $.ajax({
                        context: this,
                        url: ' {{ env('PAYROLL_URL') }} ',
                        success: function (result) {
                            this.formatDataE(result);
                        },error: function (request, status, error) {
                            console.log(request.responseText);
                        }
                    });
                },
                computed:{
                    netSalary: function () {
                       return accounting.formatMoney(Number((this.calc_net_salary()).toFixed(2)));
                    },
                    grossPay: function(){
                        return  accounting.formatMoney(Number((this.calc_gross_pay()).toFixed(2))) ;
                    },
                    allOT: function () {
                        return accounting.formatMoney(Number((parseFloat(this.calc_all_OT())).toFixed(2)) );
                    },
                    taxableIncome: function(){
                        return accounting.formatMoney(Number((parseFloat(this.calc_taxable_income())).toFixed(2)));
                    },
                    companyPension: function(){
                        return accounting.formatMoney(Number((parseFloat(this.calc_company_pension())).toFixed(2)));
                    },
                    employeePension: function(){
                        return accounting.formatMoney(Number((parseFloat(this.calc_employee_pension())).toFixed(2)));
                    },
                    totalDeduction: function () {
                      return accounting.formatMoney(Number((parseFloat(this.calc_total_deduction())).toFixed(2)))
                    },
                    taxableAllowance: function(){
                      return accounting.formatMoney(Number(this.calc_taxable_allowances()));
                    },
                    nonTaxableAllowance: function(){
                      return accounting.formatMoney(Number(this.calc_non_taxable_incomes()));
                    },
                    incomeTax: function(){
                       return accounting.formatMoney(Number((parseFloat(this.calc_income_tax())).toFixed(2)));
                    },
                    otherDeduction: function(){
                       return accounting.formatMoney(Number((parseFloat(this.calc_other_deduction()).toFixed(2))))
                    },
                    validateWorkingDay: function () {
                        if (this.payroll_main_working_day >= 0 && this.payroll_main_working_day <= 26)
                        {
                            return  false;
                        }
                        return true;
                    },
                    validateWorkingDayClass: function () {
                            return  { 'has-warning': this.validateWorkingDay }
                    },

                },
                methods: {
                    get_employee_info: function (v) {
                        var key = '{{ env('PAYROLL_KEY').'-'.md5(date('Y-m-d')) }}' ;
                        $.ajax({
                            context: this,
                            url: ' {{ env('PAYROLL_URL') }}',
                            data: { employee_id: v, PAYROLL: key },
                            success: function (result) {
                                        this.payroll_main_basic_salary                          = result.employee_basic_salary ;
                                        this.payroll_main_perdiem                               = result.employee_per_diem_allowance;
                                        this.payroll_main_special_allowance                     = result.employee_special_allowance;
                                        this.payroll_main_house_allowance                       = result.employee_house_allowance;
                                        this.payroll_main_telephone_allowance                   = result.employee_telephone_allowance;
                                        this.payroll_main_transport_non_taxable_allowance       = result.employee_transport_allowance;
                                        this.payroll_main_desert_allowance                      = result.employee_desert_allowance;
                                        this.payroll_main_representation_non_taxable_allowance  = result.employee_responsibility_allowance;
                                        this.payroll_main_representation_non_taxable_allowance  = result.employee_responsibility_allowance;
                                /**
                                 * I dont know where to put this
                                 * i will ask Bini about this
                                 */
//                           result.employee_education_allowance;
                            },error: function (request, status, error) {
                                console.log(request.responseText);
                            }
                        });
                    },
                    calc_net_salary: function(){
                        return Number(((this.calc_gross_pay()) -
                                         (parseFloat(this.calc_total_deduction()) +
                                          parseFloat(this.calc_other_deduction())).toFixed(2))
                                     );
                    },
                    calc_gross_pay: function(){
                            return Number(parseFloat(this.current_basic_salary_per_month()) +
                                   parseFloat(this.payroll_main_perdiem) +
                                   parseFloat(this.calc_taxable_allowances()) +
                                   parseFloat(this.calc_non_taxable_incomes()) +
                                   parseFloat(this.calc_all_OT()));
                    },
                    calc_total_deduction: function () {
                        return Number(parseFloat(this.calc_income_tax()) +
                                      parseFloat(this.calc_employee_pension() +
                                      parseFloat(this.payroll_main_loan) +
                                      parseFloat(this.payroll_main_advance)));
                    },
                    /**
                     *
                     *
                     * */
                    basic_salary_per_day: function () {
                        return Number(parseFloat(remove_comma(String(this.payroll_main_basic_salary)))/working_day) ;
                    },
                    /**
                     *
                     *
                     * */
                    current_basic_salary_per_month: function(){
                        return  Number((parseInt(this.payroll_main_working_day) * this.basic_salary_per_day()).toFixed(2))
                    },
                    /**
                     *
                     *
                     * */
                    calc_income_tax: function(){
                      return Number(parseFloat(this.calc_taxable_income())*(parseInt(this.calc_tax_rate().percent) / 100))
                                    - parseFloat(this.calc_tax_rate().deduction_amount )
                    },
                    /**
                     *
                     *
                     * */
                    calc_company_pension: function(){
                             return Number((parseFloat(company_pension_rate)/ 100) * this.current_basic_salary_per_month())
                    },
                    /**
                     *
                     *
                     * */
                    calc_employee_pension: function(){
                        return Number((parseFloat(employee_pension_rate)/ 100) * this.current_basic_salary_per_month())
                    },
                    calc_tax_rate: function(){
                        var rates = Object.keys(tax_rates).map(function (key) { return tax_rates[key]; });
                        var self = this;
                        var result = {};
                        rates.forEach(function (tax_rate) {
                            if(parseFloat(self.calc_taxable_income()) >= parseFloat(tax_rate.min_amount)
                                    && parseFloat(self.calc_taxable_income()) <= parseFloat(tax_rate.max_amount)
                            ){
                                result = tax_rate;
                            }

                        });
                        return result;
                    },
                    calc_taxable_income: function () {
                       return  Number(
                                      parseFloat(this.calc_taxable_allowances())+
                                      parseFloat(this.calc_all_OT()) +
                                      parseFloat(this.current_basic_salary_per_month())
                                     );
                    },
                    calc_other_deduction: function () {
                        return Number(
                                      parseFloat(this.payroll_main_other_sport_deduction) +
                                      parseFloat(this.payroll_main_other_cost_sharing_deduction) +
                                      parseFloat(this.payroll_main_telephone_deduction) +
                                      parseFloat(this.payroll_main_other_deduction)
                                    );
                    },
                    calc_taxable_allowances: function(){
                           return  (
                                     parseFloat(this.payroll_main_special_allowance) +
                                     parseFloat(this.payroll_main_house_allowance ) +
                                     parseFloat(this.payroll_main_position_allowance ) +
                                     parseFloat(this.payroll_main_transport_allowance) +
                                     parseFloat(this.payroll_main_daily_perdiem_allowance)+
                                     parseFloat(this.payroll_main_telephone_allowance) +
                                     parseFloat(this.payroll_main_representation_allowance) +
                                     parseFloat(this.payroll_main_ot_sub_allowance ) +
                                     parseFloat(this.payroll_main_other_taxable_allowance)
                                  ).toFixed(2);

                    },
                    calc_non_taxable_incomes: function(){
                      return (
                                parseFloat(this.payroll_main_transport_non_taxable_allowance) +
                                parseFloat(this.payroll_main_representation_non_taxable_allowance) +
                                parseFloat(this.payroll_main_desert_allowance) +
                                parseFloat(this.payroll_main_other_non_taxable_allowance)

                              ).toFixed(2);
                    },
                    validateOTClass: function (who) {
                        var caller = parseFloat(who);
                        if(this.correct_ot().type !== 0)
                        {
                            if (caller == 1.25 && this.correct_ot().type == 1.25){
                               return false;
                            }
                            if (caller == 1.5 && this.correct_ot().type == 1.5){
                               return false;
                            }
                            if (caller == 2.0 && this.correct_ot().type == 2.0){
                               return false;
                            }
                            if (caller == 2.5 && this.correct_ot().type == 2.5){
                               return false;
                            }
                           return true;
                        }
                        return false;

                    },calc_all_OT: function(){
                      return Number((((this.payroll_main_basic_salary/working_hour_per_month) * parseFloat(this.calc_ot_1_2_5().value)) +
                              ((this.payroll_main_basic_salary/working_hour_per_month) * parseFloat(this.calc_ot_1_5().value)) +
                              ((this.payroll_main_basic_salary/working_hour_per_month) * parseFloat(this.calc_ot_2_0().value)) +
                              ((this.payroll_main_basic_salary/working_hour_per_month) * parseFloat(this.calc_ot_2_5().value))
                      ).toFixed(2));
                    },
                    correct_ot: function(){
                        if(this.calc_ot_1_2_5().value !== 0)
                        {
                            return this.calc_ot_1_2_5();
                        }else if(this.calc_ot_1_5().value !== 0)
                        {
                             return this.calc_ot_1_5();
                        }else if(this.calc_ot_2_0().value !== 0)
                        {
                             return this.calc_ot_2_0();
                        }else if(this.calc_ot_2_5().value !== 0)
                        {
                             return this.calc_ot_2_5();
                        }
                        return {type: 0 , value: 0};
                    },
                    calc_ot_1_2_5: function(){
                        var self = this;
                        return new function () {
                               this.type = 1.25;
                               this.value =Number((parseInt(self.payroll_main_ot_1_25) * this.type).toFixed(2));
                        };
                    },

                    calc_ot_1_5: function(){
                        var self = this;
                        return new function () {
                            this.type = 1.5;
                            this.value =Number((parseInt(self.payroll_main_ot_1_5) * this.type).toFixed(2));
                        };
                    },

                    calc_ot_2_0: function(){
                        var self = this;
                        return new function () {
                            this.type = 2.0;
                            this.value =Number((parseInt(self.payroll_main_ot_2_0) * this.type).toFixed(2));
                        };
                    },

                    calc_ot_2_5: function(){
                        var self = this;
                        return new function () {
                            this.type = 2.5;
                            this.value = Number((parseInt(self.payroll_main_ot_2_5) * this.type).toFixed(2));
                        };
                    },
                    submitForm: function (e) {
                        var form = e.srcElement;
                        var action = form.action;
                        var csrfToken = form.querySelector('input[name="_token"]').value;
                               var datas  = {
                                   payroll_main_payroll_project_id                      : this.payroll_main_payroll_project_id,
                                   payroll_main_payroll_employee_id                     : this.payroll_main_payroll_employee_id,
                                   payroll_main_bank_account_no                         : this.payroll_main_bank_account_no,
                                   payroll_main_permanent_employee                      : this.payroll_main_permanent_employee,
                                   payroll_main_working_day                             : this.payroll_main_working_day,
                                   payroll_main_basic_salary                            : this.payroll_main_basic_salary,
                                   payroll_main_perdiem                                 : this.payroll_main_perdiem,
                                   payroll_main_loan                                    : this.payroll_main_loan,
                                   payroll_main_advance                                 : this.payroll_main_advance,
                                   payroll_main_telephone_deduction                     : this.payroll_main_telephone_deduction,
                                   payroll_main_other_deduction                         : this.payroll_main_other_deduction,
                                   payroll_main_other_cost_sharing_deduction            : this.payroll_main_other_cost_sharing_deduction,
                                   payroll_main_other_sport_deduction                   : this.payroll_main_other_sport_deduction,
                                   payroll_main_ot_1_25                                 : this.payroll_main_ot_1_25,
                                   payroll_main_ot_1_5                                  : this.payroll_main_ot_1_5,
                                   payroll_main_ot_2_0                                  : this.payroll_main_ot_2_0,
                                   payroll_main_ot_2_5                                  : this.payroll_main_ot_2_5,
                                   payroll_main_special_allowance                       : this.payroll_main_special_allowance,
                                   payroll_main_house_allowance                         : this.payroll_main_house_allowance,
                                   payroll_main_position_allowance                      : this.payroll_main_position_allowance,
                                   payroll_main_transport_allowance                     : this.payroll_main_transport_allowance,
                                   payroll_main_daily_perdiem_allowance                 : this.payroll_main_daily_perdiem_allowance,
                                   payroll_main_telephone_allowance                     : this.payroll_main_telephone_allowance,
                                   payroll_main_representation_allowance                : this.payroll_main_representation_allowance,
                                   payroll_main_ot_sub_allowance                        : this.payroll_main_ot_sub_allowance,
                                   payroll_main_other_taxable_allowance                 : this.payroll_main_other_taxable_allowance,
                                   payroll_main_transport_non_taxable_allowance         : this.payroll_main_transport_non_taxable_allowance,
                                   payroll_main_desert_allowance                        : this.payroll_main_desert_allowance,
                                   payroll_main_representation_non_taxable_allowance    : this.payroll_main_representation_non_taxable_allowance,
                                   payroll_main_other_non_taxable_allowance             : this.payroll_main_other_non_taxable_allowance,

                               }
                        this.$http.post(action,datas, {
                            headers: {
                                'X-CSRF-TOKEN': csrfToken
                            }
                        }).then(function (response) {
                            form.submit();
                        })
                        .catch(function (data, status, request) {
                            var errors = data.data;
                            this.formErrors = errors;
                            console.log(errors);
                        });
                    },
                    formatData: function (result) {
                        var self = this;
                        var obj = Object.create(result) ;
                        Object.keys(obj.__proto__).forEach(function(key) {
                            self.options.push({id:key,text:obj[key]});
                        });
                    },formatDataE: function (result) {
                        var self = this;
                        var obj = Object.create(result) ;
                        Object.keys(obj.__proto__).forEach(function(key) {
                            self.optionsE.push({id:key,text:obj[key]});
                        });
                    }
                }
            })
    </script>

@endsection
