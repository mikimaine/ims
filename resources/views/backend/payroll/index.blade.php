@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management'))

@section('after-styles-end')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('page-header')
    <h1>
        {{ trans('payroll.menus.main_payroll.management') }}
        <small>{{ trans('payroll.label.main_payroll.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('payroll.label.main_payroll.active') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.payroll.includes.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                            <th>{{ trans('payroll.label.main_payroll.table.bank_account') }}</th>
                            <th>{{ trans('payroll.label.main_payroll.table.permanent') }}</th>
                            <th>{{ trans('payroll.label.main_payroll.table.created_by') }}</th>
                            <th>{{ trans('payroll.label.net_salary') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('MainPayroll') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@stop

@section('after-scripts-end')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
    {{ Html::script("js/backend/plugin/accounting/accounting.min.js") }}
    <script>
        /**
         * Setting for Currency Formatting
         * @type @{{ currency: {symbol: string, format: string, decimal: string, thousand: string, precision: number},
         *            number: {precision: number, thousand: string, decimal: string}}}
         */
        accounting.settings = {
            currency: {
                symbol : " ብር ",   // default currency symbol is '$'
                format: "%v%s", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal : ".",  // decimal point separator
                thousand: ",",  // thousands separator
                precision : 2   // decimal places
            },
            number: {
                precision : 0,  // default precision on numbers is 0
                thousand: ",",
                decimal : "."
            }
        }
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.payroll.main_payroll.get") }}',
                    type: 'get',
                    data: {trashed: false}
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'payroll_main_bank_account_no', name: 'payroll_main_bank_account_no'},
                    {data: 'payroll_main_permanent_employee', name: 'payroll_main_permanent_employee'},
                    {data: 'name', name: 'name'},
                    {data: 'net_salary', name: 'net_salary'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions'}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop