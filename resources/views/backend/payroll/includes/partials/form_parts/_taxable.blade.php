<div class="form-group">

    {{ Form::label('payroll_main_special_allowance', trans('payroll.label.main_payroll.table.special_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_special_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_special_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.special_allowance')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->

<div class="form-group">
    {{ Form::label('payroll_main_house_allowance', trans('payroll.label.main_payroll.table.house_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_house_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_house_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.house_allowance')]) }}
    </div><!--col-xs-4-->

    {{ Form::label('payroll_main_position_allowance', trans('payroll.label.main_payroll.table.position_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_position_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_position_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.position_allowance')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->


<div class="form-group">
    {{ Form::label('payroll_main_transport_allowance', trans('payroll.label.main_payroll.table.transport_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_transport_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_transport_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.transport_allowance')]) }}
    </div><!--col-xs-4-->

    {{ Form::label('payroll_main_daily_perdiem_allowance', trans('payroll.label.main_payroll.table.perdiem'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_daily_perdiem_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_daily_perdiem_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.perdiem')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->

<div class="form-group">
    {{ Form::label('payroll_main_telephone_allowance', trans('payroll.label.main_payroll.table.telephone_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_telephone_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_telephone_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.telephone_allowance')]) }}
    </div><!--col-xs-4-->

    {{ Form::label('payroll_main_representation_allowance', trans('payroll.label.main_payroll.table.representation_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_representation_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_representation_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.representation_allowance')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->

<div class="form-group">
    {{ Form::label('payroll_main_ot_sub_allowance', trans('payroll.label.main_payroll.table.ot_sub_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_ot_sub_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_ot_sub_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.ot_sub_allowance')]) }}
    </div><!--col-xs-4-->

    {{ Form::label('payroll_main_other_taxable_allowance', trans('payroll.label.main_payroll.table.other_taxable_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_other_taxable_allowance', null, ['class' => 'form-control','v-model'=>'payroll_main_other_taxable_allowance', 'placeholder' => trans('payroll.label.main_payroll.table.other_taxable_allowance')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->