<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('payroll.label.create') }}</h3>

        <div class="box-tools pull-right">
            @include('backend.payroll.includes.partials.header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group ">
            {{ Form::label('payroll_main_payroll_project_id', trans('payroll.label.main_payroll.table.project_code'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-xs-4 ">
                        <select class="form-control"  v-model="payroll_main_payroll_project_id"  required>
                                <option value="">Select Department Name (Code)</option>
                                    @foreach ($departments as $department)
                                        <option value="{!! $department->id !!}">{!! $department->project_name !!} ({!! $department->project_code !!})</option>
                                    @endforeach
                        </select>
                </div>
            {{ Form::label('payroll_main_payroll_employee_id', trans('payroll.label.main_payroll.table.employee_name'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-xs-4">
                        <select  class="form-control"  v-model="payroll_main_payroll_employee_id" required>
                            <option value="">-- Select Employee --</option>
                            <option
                                    v-for="option in optionsE"
                                    track-by="$index"
                                    :value="option.id"
                                    v-text="option.text">

                            </option>
                        </select>
             </div>
            </div>
        <div class="form-group">
            {{ Form::label('payroll_main_permanent_employee', trans('payroll.label.main_payroll.table.permanent'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::checkbox('payroll_main_permanent_employee', '1', null,['v-model'=>'payroll_main_permanent_employee']) }}
            </div><!--col-xs-4-->

            {{ Form::label('payroll_main_bank_account_no', trans('payroll.label.main_payroll.table.bank_account'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_bank_account_no', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.bank_account'), 'v-model'=>'payroll_main_bank_account_no']) }}
            </div><!--col-xs-4-->
        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('payroll_main_working_day', trans('payroll.label.main_payroll.table.working_day'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4" v-bind:class="validateWorkingDayClass" >
                {{ Form::number('payroll_main_working_day', null, ['class' => 'form-control', 'v-model'=>'payroll_main_working_day', 'placeholder' => trans('payroll.label.main_payroll.table.working_day')]) }}
                <label class="control-label " v-if="validateWorkingDay"  for="payroll_main_working_day"><i class="fa fa-bell-o"></i> Working day should be between 0-26</label>
            </div><!--col-xs-4-->

            {{ Form::label('payroll_main_basic_salary', trans('payroll.label.main_payroll.table.basic_salary'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">

                {{ Form::text('payroll_main_basic_salary', null, ['class' => 'form-control','v-model'=>'payroll_main_basic_salary', 'placeholder' => trans('payroll.label.main_payroll.table.basic_salary'),'data-inputmask'=>'\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true, \'digits\': 2, \'digitsOptional\': false, \'prefix\': \'$ \', \'placeholder\': \'0\'']) }}

            </div><!--col-xs-4-->

        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('payroll_main_perdiem', trans('payroll.label.main_payroll.table.perdiem'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_perdiem', null, ['class' => 'form-control','v-model'=>'payroll_main_perdiem', 'placeholder' => trans('payroll.label.main_payroll.table.perdiem')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('payroll_main_loan', trans('payroll.label.main_payroll.table.loan'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_loan', null, ['class' => 'form-control','v-model'=>'payroll_main_loan', 'placeholder' => trans('payroll.label.main_payroll.table.loan')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('payroll_advance', trans('payroll.label.main_payroll.table.advance'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_advance', null, ['class' => 'form-control','v-model'=>'payroll_main_advance', 'placeholder' => trans('payroll.label.main_payroll.table.advance')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('payroll_main_telephone_deduction', trans('payroll.label.main_payroll.table.telephone_deduction'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_telephone_deduction', null, ['class' => 'form-control','v-model'=>'payroll_main_telephone_deduction', 'placeholder' => trans('payroll.label.main_payroll.table.telephone_deduction')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('payroll_main_other_deduction', trans('payroll.label.main_payroll.table.other_deduction'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_other_deduction', null, ['class' => 'form-control','v-model'=>'payroll_main_other_deduction', 'placeholder' => trans('payroll.label.main_payroll.table.other_deduction')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('payroll_main_other_cost_sharing_deduction', trans('payroll.label.main_payroll.table.cost_sharing_deduction'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_other_cost_sharing_deduction', null, ['class' => 'form-control','v-model'=>'payroll_main_other_cost_sharing_deduction', 'placeholder' => trans('payroll.label.main_payroll.table.cost_sharing_deduction')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('payroll_main_other_sport_deduction', trans('payroll.label.main_payroll.table.sport_deduction'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('payroll_main_other_sport_deduction', null, ['class' => 'form-control','v-model'=>'payroll_main_other_sport_deduction', 'placeholder' => trans('payroll.label.main_payroll.table.sport_deduction')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                             {{ trans('payroll.label.ot_inputs')  }}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        @include('backend.payroll.includes.partials.form_parts._ot-form')
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            {{ trans('payroll.label.taxable_inputs')  }}
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        @include('backend.payroll.includes.partials.form_parts._taxable')
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            {{ trans('payroll.label.non_taxable_inputs')  }}
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        @include('backend.payroll.includes.partials.form_parts._non-taxable')
                    </div>
                </div>
            </div>
        </div>


    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.payroll.main_payroll.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit($buttonText, ['class' => 'btn btn-success btn-xs']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
