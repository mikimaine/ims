    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('payroll.label.break_down')  }}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->

        <div id="" class="box-footer no-padding">
            <ul class="nav nav-pills nav-stacked">

                <li><a href="#">
                        {{ trans('payroll.label.taxable_allowance')  }}
                        <span  class="pull-right text-red"  >
                          <p v-text="taxableAllowance" ></p>
                      </span></a></li>

                <li><a href="#">
                        {{ trans('payroll.label.non_taxable_allowance')  }}
                        <span  class="pull-right text-red"  >
                          <p v-text="nonTaxableAllowance" ></p>
                      </span></a></li>
                <li><a href="#">
                        {{ trans('payroll.label.gross_pay')  }}
                        <span  class="pull-right text-red"  >
                          <p v-text="grossPay" ></p>
                      </span></a></li>
                <li><a href="#">
                        {{ trans('payroll.label.total_ot')  }}
                        <span class="pull-right text-green">
                        <p v-text="allOT" ></p>
                        </span></a>
                </li>
                <li><a href="#">
                        {{ trans('payroll.label.taxable_income')  }}
                        <span class="pull-right text-green">
                        <p v-text="taxableIncome" ></p>
                        </span></a>
                </li>
                <li><a href="#">
                        {{ trans('payroll.label.income_tax')  }}
                        <span class="pull-right text-yellow">
                     <p v-text="incomeTax" > </p>
                     </span></a></li>

                <li><a href="#">
                        {{ trans('payroll.label.company_pension')  }}
                        <span class="pull-right text-yellow">
                         <p v-text="companyPension"></p>
                     </span></a></li>

                <li><a href="#">
                        {{ trans('payroll.label.employee_pension')  }}
                        <span class="pull-right text-yellow">
                         <p v-text="employeePension"></p>
                     </span></a></li>

                <li><a href="#">
                        {{ trans('payroll.label.other_deduction')  }}
                        <span class="pull-right text-yellow">
                         <p v-text="otherDeduction"></p>
                     </span></a></li>

                <li><a href="#">
                        {{ trans('payroll.label.total_deduction')  }}
                        <span class="pull-right text-yellow">
                        <p v-text="totalDeduction" ></p>
                     </span></a></li>

                <li><a href="#">
                        <strong> {{ trans('payroll.label.net_salary')  }}
                            <span class="pull-right text-yellow">
                         <p v-text="netSalary"></p>
                     </span></a></li>
                </strong>
            </ul>
        </div>

        <!-- /.footer -->
    </div>