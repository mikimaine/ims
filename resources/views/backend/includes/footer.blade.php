<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Proudly Powered by <a href="http://github.com/mikimaine/ecommerce" target="_blank">{{ trans('strings.backend.general.innovate_link') }}</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="#">Berhanena Selma Printing Enterprise</a>.</strong> {{ trans('strings.backend.general.all_rights_reserved') }} </footer>
