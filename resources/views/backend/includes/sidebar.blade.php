<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ Active::pattern('admin/dashboard') }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            @permission('manage-users')
                <li class="{{ Active::pattern('admin/access/*') }}">
                    <a href="{{ route('admin.access.user.index') }}">
                        <i class="fa fa-user"></i>
                        <span>{{ trans('menus.backend.access.title') }}</span>
                    </a>
                </li>
            @endauth

            <li class="header"> Employee </li>
            <li class="{{ Active::pattern('admin/payroll*') }} {{ Active::pattern('admin/back-pay-payroll*') }} {{ Active::pattern('admin/daily-payroll*') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Employee Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/payroll*', 'menu-open') }} {{ Active::pattern('admin/back-pay-payroll*', 'menu-open') }} {{ Active::pattern('admin/daily-payroll*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/payroll*', 'display: block;') }} {{ Active::pattern('admin/back-pay-payroll*', 'display: block;') }} {{ Active::pattern('admin/daily-payroll*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/payroll/main_payroll') }}">
                        <a href="{{ route('admin.payroll.main_payroll.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Employee</span>
                        </a>
                    </li>
                    

                    <li class="{{ Active::pattern('admin/back-pay-payroll/back_pay_payroll') }}">
                        <a href="{{ route('admin.back-pay-payroll.back_pay_payroll.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Vacancy Management</span>
                        </a>
                    </li>
                </ul>
            </li>


            <li class="header"> Items </li>
            <li class="{{ Active::pattern('admin/payroll*') }} {{ Active::pattern('admin/back-pay-payroll*') }} {{ Active::pattern('admin/daily-payroll*') }} treeview">
                <a href="#">
                    <i class="fa fa-crosshairs"></i>
                    <span>Item Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/payroll*', 'menu-open') }} {{ Active::pattern('admin/back-pay-payroll*', 'menu-open') }} {{ Active::pattern('admin/daily-payroll*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/payroll*', 'display: block;') }} {{ Active::pattern('admin/back-pay-payroll*', 'display: block;') }} {{ Active::pattern('admin/daily-payroll*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/payroll/main_payroll') }}">
                        <a href="{{ route('admin.payroll.main_payroll.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Orders </span>
                        </a>
                    </li>


                </ul>
            </li>


            @permission('manage-payroll')
              <li class="header">{{ trans('payroll.menus.payroll') }}</li>
            <li class="{{ Active::pattern('admin/payroll*') }} {{ Active::pattern('admin/back-pay-payroll*') }} {{ Active::pattern('admin/daily-payroll*') }} treeview">
                <a href="#">
                    <i class="fa fa-money"></i>
                    <span>{{ trans('payroll.menus.payroll_management') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/payroll*', 'menu-open') }} {{ Active::pattern('admin/back-pay-payroll*', 'menu-open') }} {{ Active::pattern('admin/daily-payroll*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/payroll*', 'display: block;') }} {{ Active::pattern('admin/back-pay-payroll*', 'display: block;') }} {{ Active::pattern('admin/daily-payroll*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/payroll/main_payroll') }}">
                        <a href="{{ route('admin.payroll.main_payroll.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.main_payroll_management') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/daily-payroll/daily_payroll') }}">
                        <a href="{{ route('admin.daily-payroll.daily_payroll.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.daily_payroll_management') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/back-pay-payroll/back_pay_payroll') }}">
                        <a href="{{ route('admin.back-pay-payroll.back_pay_payroll.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.back_pay_payroll_management') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth


            @permission('manage-payroll')
            <li class="{{ Active::pattern('payroll/main_payroll*') }} treeview">
                <a href="#">
                    <i class="fa fa-file"></i>
                    <span>{{ trans('payroll.menus.payroll_report') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('payroll/main_payroll*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{{ route('admin.payroll.main_payroll_report.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.main_payroll_management_report') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.payroll.daily_payroll_report.daily') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.daily_payroll_management_report') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.payroll.back_pay_payroll_report.back_pay') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.back_pay_payroll_management_report') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.payroll.bank_pay_report.bank_pay') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.bank_report') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth

            @permission('manage-payroll')
            <li class="{{ Active::pattern('setting/payroll_setting*') }} treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>{{ trans('payroll.menus.payroll_setting') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('payroll/main_payroll*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{{ route('admin.settings.payroll_settings.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.payroll_setting') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.settings.tax_rate_settings.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.tax_rate_setting') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.settings.department_settings.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('payroll.menus.department_settings') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth



            <li class="header">{{ trans('menus.backend.log-viewer.main') }}</li>
            <li class="{{ Active::pattern('admin/log-viewer*') }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/log-viewer*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{{ route('admin.log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
