<div class="pull-right mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('daily_payroll.menus.daily_payroll.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.daily-payroll.daily_payroll.index', trans('daily_payroll.menus.daily_payroll.all')) }}</li>

            @permission('manage-users')
                <li>{{ link_to_route('admin.daily-payroll.daily_payroll.create', trans('daily_payroll.menus.daily_payroll.create')) }}</li>
            @endauth

            <li class="divider"></li>
             <li>{{ link_to_route('admin.daily-payroll.daily_payroll.deleted', trans('daily_payroll.menus.daily_payroll.deleted')) }}</li>
        </ul>
    </div><!--btn group-->


</div><!--pull right-->

<div class="clearfix"></div>
