<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('daily_payroll.label.create') }}</h3>

        <div class="box-tools pull-right">
            @include('backend.daily_payroll.includes.partials.header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <p class="text-red" v-for="error in formErrors">
             @{{ error }}
        </p>
        <div class="form-group ">
            {{ Form::label('daily_labour_project_id', trans('payroll.label.main_payroll.table.project_code'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4 ">
                <select class="form-control"  v-model="daily_labour_project_id"  required>
                    <option value="">Select Department Name (Code)</option>
                    @foreach ($departments as $department)
                        <option value="{!! $department->id !!}">{!! $department->project_name !!} ({!! $department->project_code !!})</option>
                    @endforeach
                </select>
            </div><!--col-xs-4-->

            {{ Form::label('daily_labour_employee_id', trans('payroll.label.main_payroll.table.employee_name'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                <select  class="form-control"  v-model="daily_labour_employee_id" required>
                    <option value="">-- Select Employee --</option>
                    <option
                            v-for="option in optionsE"
                            track-by="$index"
                            :value="option.id"
                            v-text="option.text">

                    </option>
                </select>
            </div><!--col-xs-4-->
        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('daily_labour_bank_account', trans('payroll.label.main_payroll.table.bank_account'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('daily_labour_bank_account', null, ['v-model'=>'daily_labour_bank_account','class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.bank_account')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('daily_labour_working_day', trans('payroll.label.main_payroll.table.working_day'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('daily_labour_working_day', null, ['v-model'=>'daily_labour_working_day','class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.working_day')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('daily_labour_wage_per_day', trans('daily_payroll.label.daily_payroll.table.wage_per_day'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('daily_labour_wage_per_day', null, ['v-model'=>'daily_labour_wage_per_day','class' => 'form-control', 'placeholder' => trans('daily_payroll.label.daily_payroll.table.wage_per_day')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('daily_labour_salary', trans('payroll.label.main_payroll.table.basic_salary'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('daily_labour_salary', null, ['v-model'=>'daily_labour_salary','class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.basic_salary')]) }}
            </div><!--col-xs-4-->

        </div><!--form control-->

        <div class="form-group">

            {{ Form::label('daily_labour_loan', trans('payroll.label.main_payroll.table.loan'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('daily_labour_loan', null, ['v-model'=>'daily_labour_loan','class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.loan')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('daily_labour_penalty', trans('daily_payroll.label.daily_payroll.table.penalty'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('daily_labour_penalty', null, ['v-model'=>'daily_labour_penalty','class' => 'form-control', 'placeholder' => trans('daily_payroll.label.daily_payroll.table.penalty')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                             {{ trans('payroll.label.ot_inputs')  }}
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        @include('backend.daily_payroll.includes.partials.form_parts._ot-form')
                    </div>
                </div>
            </div>

        </div>


    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.daily-payroll.daily_payroll.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit($buttonText, ['class' => 'btn btn-success btn-xs']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
