<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('payroll.label.break_down')  }}</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-footer no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="#">
                    Basic Salary
                    <span class="pull-right text-red">
                    <p v-text="basic_salary" ></p>
                    </span></a></li>
            <li><a href="#">
                    Total Ot
                    <span class="pull-right text-green">
                        <p v-text="total_ot"></p>
                    </span></a>
            </li>
            <li><a href="#">
                    Taxable Income
                    <span class="pull-right text-yellow">
                        <p v-text="taxable_income"></p>
                    </span></a></li>

            <li><a href="#">
                    {{ trans('payroll.label.income_tax')  }}
                    <span class="pull-right text-yellow">
                     <p v-text="income_tax"></p>
                    </span></a></li>


            <li><a href="#">
                    {{ trans('payroll.label.total_deduction')  }}
                    <span class="pull-right text-yellow">
                    <p v-text="total_deduction"> </p>
                    </span></a></li>

            <li><a href="#">
                    <strong> {{ trans('payroll.label.net_salary')  }}
                        <span class="pull-right text-yellow">
                        <p v-text="net_salary"></p>
                        </span></strong></a></li>
            </strong>
        </ul>
    </div>
    <!-- /.footer -->
</div>