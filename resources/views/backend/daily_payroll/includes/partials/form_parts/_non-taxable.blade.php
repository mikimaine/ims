<div class="form-group">
    {{ Form::label('payroll_main_transport_non_taxable_allowance', trans('payroll.label.main_payroll.table.transport_non_taxable_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_transport_non_taxable_allowance', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.transport_non_taxable_allowance')]) }}
    </div><!--col-xs-4-->

    {{ Form::label('payroll_main_desert_allowance', trans('payroll.label.main_payroll.table.desert_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_desert_allowance', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.desert_allowance')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->

<div class="form-group">
    {{ Form::label('payroll_main_representation_non_taxable_allowance', trans('payroll.label.main_payroll.table.representation_non_taxable_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_representation_non_taxable_allowance', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.representation_non_taxable_allowance')]) }}
    </div><!--col-xs-4-->

    {{ Form::label('payroll_main_other_non_taxable_allowance', trans('payroll.label.main_payroll.table.other_non_taxable_allowance'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('payroll_main_other_non_taxable_allowance', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.other_non_taxable_allowance')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->