@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management') . ' | ' . trans('payroll.label.create'))

@section('page-header')
    <h1>
        {{ trans('payroll.menus.main_payroll.management') }}
        <small>{{ trans('payroll.label.create') }}</small>
    </h1>
@endsection

@section('content')
    <div id="break_down" >
    <div class="col-md-9">
    {{ Form::open(['route' => ['admin.daily-payroll.daily_payroll.update',$payroll->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
       @include('backend.daily_payroll.includes.partials._form',[$pageName = 'edit',$buttonText = trans('buttons.general.crud.edit') ])
    {{ Form::close() }}
    </div>

        <div class="col-md-3">
            @include('backend.daily_payroll.includes.partials._calculator_aside')
        </div>
    </div>
@stop

@section('after-scripts-end')
    <script>
        var vm = new Vue({
            el: '#break_down',
            data: {
                daily_labour_project_id                  : payroll.daily_labour_project_id,
                daily_labour_employee_id                 : payroll.daily_labour_employee_id,
                daily_labour_bank_account                : payroll.daily_labour_bank_account,
                daily_labour_working_day                 : payroll.daily_labour_working_day,
                daily_labour_wage_per_day                : payroll.daily_labour_wage_per_day,
                daily_labour_salary                      : payroll.daily_labour_salary,
                daily_labour_loan                        : payroll.daily_labour_loan,
                daily_labour_penalty                     : payroll.daily_labour_penalty,
                daily_labour_ot_1_25                     : payroll.daily_labour_ot_1_25,
                daily_labour_ot_1_5                      : payroll.daily_labour_ot_1_5,
                daily_labour_ot_2_0                      : payroll.daily_labour_ot_2_0,
                daily_labour_ot_2_5                      : payroll.daily_labour_ot_2_5,
                formErrors: {},
                optionsE: []

            },
            created: function () {
                $.ajax({
                    context: this,
                    url: ' {{ env('PAYROLL_URL') }} ',
                    success: function (result) {
                        //console.log(result);
                        this.formatDataE(result);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });
            },
            computed: {
                basic_salary: function () {
                    //console.log(this.calc_income_tax());
                    return this.daily_labour_salary = this.calc_basic_salary();
                },
                total_ot: function () {
                    return this.calc_total_ot();
                },
                income_tax: function () {
                    return Number((this.calc_income_tax()).toFixed(2));
                },
                taxable_income: function () {
                    return Number((this.calc_taxable_income()).toFixed(2));
                },
                total_deduction: function () {
                    return Number((this.calc_total_deduction()).toFixed(2));
                },
                net_salary: function () {
                    return Number((this.calc_net_salary()).toFixed(2));
                }
            },
            methods: {
                calc_net_salary: function () {
                    return parseFloat(this.calc_taxable_income()) - this.calc_total_deduction();
                },
                calc_total_deduction: function(){
                    return Number(this.calc_income_tax() + Number(this.daily_labour_loan) + Number(this.daily_labour_penalty));
                },
                calc_taxable_income: function () {
                    return parseFloat(this.calc_basic_salary()) + parseFloat(this.calc_total_ot());
                },
                calc_income_tax: function(){
                    return Number(parseFloat(this.calc_taxable_income())*(parseInt(this.calc_tax_rate().percent) / 100))
                            - parseFloat(this.calc_tax_rate().deduction_amount )
                },
                calc_tax_rate: function(){
                    //console.log(tax_rates);
                    var rates = Object.keys(tax_rates).map(function (key) { return tax_rates[key]; });
                    var self = this;
                    var result = {};
                    rates.forEach(function (tax_rate) {
                        if(parseFloat(self.calc_taxable_income()) >= parseFloat(tax_rate.min_amount)
                                && parseFloat(self.calc_taxable_income()) <= parseFloat(tax_rate.max_amount)
                        ){
                            result = tax_rate;
                        }

                    });
                    return result;
                },
                calc_total_ot: function () {
                    return Number((this.calc_ot_1_25() + this.calc_ot_1_5() + this.calc_ot_2_0() + this.calc_ot_2_5()).toFixed(2));
                },
                calc_wage_per_hour: function () {
                    return Number((parseFloat(this.daily_labour_wage_per_day)/ 8).toFixed(2) );
                },
                calc_ot_1_25: function () {
                    return this.calc_wage_per_hour() * parseInt(this.daily_labour_ot_1_25) * 1.25;
                },
                calc_ot_1_5: function () {
                    return this.calc_wage_per_hour() * parseInt(this.daily_labour_ot_1_5) * 1.5;
                },
                calc_ot_2_0: function () {
                    return this.calc_wage_per_hour() * parseInt(this.daily_labour_ot_2_0 )* 2.0;
                },
                calc_ot_2_5: function () {
                    return this.calc_wage_per_hour() * parseInt(this.daily_labour_ot_2_5 )* 2.5;
                },
                calc_basic_salary: function () {
                    return (Number(this.daily_labour_wage_per_day) * Number(this.daily_labour_working_day)).toFixed(2);
                },
                submitForm: function (e) {
                    var form = e.srcElement;
                    var action = form.action;
                    var csrfToken = form.querySelector('input[name="_token"]').value;
                    var datas  = {
                        daily_labour_project_id                  : this.daily_labour_project_id,
                        daily_labour_employee_id                 : this.daily_labour_employee_id,
                        daily_labour_bank_account                : this.daily_labour_bank_account,
                        daily_labour_working_day                 : this.daily_labour_working_day,
                        daily_labour_wage_per_day                : this.daily_labour_wage_per_day,
                        daily_labour_salary                      : this.daily_labour_salary,
                        daily_labour_loan                        : this.daily_labour_loan,
                        daily_labour_penalty                     : this.daily_labour_penalty,
                        daily_labour_ot_1_25                     : this.daily_labour_ot_1_25,
                        daily_labour_ot_1_5                      : this.daily_labour_ot_1_5,
                        daily_labour_ot_2_0                      : this.daily_labour_ot_2_0,
                        daily_labour_ot_2_5                      : this.daily_labour_ot_2_5,
                    }
                    this.$http.put(action,datas, {
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        }
                    }).then(function (response) {
                        console.log(response);
                        //form.submit();
                    })
                            .catch(function (data, status, request) {
                                var errors = data.data;
                                this.formErrors = errors;
                                console.log(errors);
                            });
                },
                formatDataE: function (result) {
                    var self = this;
                    var obj = Object.create(result) ;
                    Object.keys(obj.__proto__).forEach(function(key) {
                        self.optionsE.push({id:key,text:obj[key]});
                    });
                },

            }

        });
    </script>
@stop
