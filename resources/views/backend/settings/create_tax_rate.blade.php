@extends ('backend.layouts.app')

@section ('title', trans('back_pay_payroll.menus.back_pay_payroll.management') . ' | ' . trans('back_pay_payroll.label.create'))

@section('page-header')
    <h1>
       Tax Rate Setting
        <small>Create New tax Rate</small>
    </h1>
@endsection

@section('content')
    <div class="col-md-10">
        <div id="tax_Rate_form" >
        {{ Form::open(['route' => 'admin.settings.tax_rate_settings.store','@submit.prevent'=>'submitForm','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
       @include('backend.settings.includes.partials._form_tax_rate',[$pageName = 'create',$buttonText = trans('buttons.general.crud.create') ])
        {{ Form::close() }}
        </div>
    </div>

    <div class="col-md-3">

    </div>
@stop

@section('after-scripts-end')
      <script>
          new Vue({
              el: '#tax_Rate_form',
              data: {
                  increment: 0,
                  title:    '',
                  taxRate :[{
                              min_amount: '',
                              max_amount: '',
                              percent: '',
                              deduction_amount: ''
                            }],
                  formErrors: {},
              },
              methods: {
              addTaxRate: function (e) {
                  e.preventDefault();

                  var dat = {   min_amount:       '',
                                max_amount:       '',
                                percent:          '',
                                deduction_amount: ''
                            };
                  this.taxRate.push(Vue.util.extend({},dat));
              }, 
              removeTaxRate: function (e) {
                    e.preventDefault();
                  if (this.taxRate.length > 1){
                     this.taxRate.pop();
                  }
              },
              submitForm: function (e) {
                  var form = e.srcElement;
                  var action = form.action;
                  var csrfToken = form.querySelector('input[name="_token"]').value;
                  var send_data = {
                                    title: this.title,
                                    rates: this.taxRate
                                    };
                  this.$http.post(action,send_data, {
                              headers: {
                                  'X-CSRF-TOKEN': csrfToken
                              }
                          })
                          .then(function(response){
                              //handle success
                              form.submit();
                          }).catch(function (data, status, request) {
                              var errors = data.data;
                              this.formErrors = errors;
                              console.log(errors);
                          });


              }
              }
          })

      </script>
@stop
