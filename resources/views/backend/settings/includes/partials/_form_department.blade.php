<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Create Department</h3>

        <div class="box-tools pull-right">
            @include('backend.settings.includes.partials.header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group ">
            {{ Form::label('project_code', trans('payroll.label.main_payroll.table.project_code'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4 ">
                {{ Form::text('project_code', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.project_code')]) }}
            </div><!--col-xs-4-->


        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('project_name','Department Name', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('project_name', null, ['class' => 'form-control', 'placeholder' => 'Department Name']) }}
            </div><!--col-xs-4-->

        </div><!--form control-->

    <div class="form-group">
            {{ Form::label('description', 'Description', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
            </div><!--col-xs-4-->

        </div><!--form control-->




    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.settings.department_settings.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit($buttonText, ['class' => 'btn btn-success btn-xs']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
