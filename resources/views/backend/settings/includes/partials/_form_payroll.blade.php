<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Payroll Setting</h3>

        <div class="box-tools pull-right">
            @include('backend.settings.includes.partials.header-buttons_payroll')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group ">
            {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4 ">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
            </div><!--col-xs-4-->


        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('value','Value', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Value']) }}
            </div><!--col-xs-4-->

        </div><!--form control-->





    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.settings.payroll_settings.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit($buttonText, ['class' => 'btn btn-success btn-xs']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
