<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Tax Rate</h3>

        <div class="box-tools pull-right">
            @include('backend.settings.includes.partials.header-buttons_tax_rate')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group ">
            {{ Form::label('title', 'Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-xs-4 ">
                {{ Form::text('title', null, ['v-model'=>'title','class' => 'form-control', 'placeholder' => 'Title']) }}
            </div><!--col-xs-4-->
        </div><!--form control-->

        <div class="form-group" v-for="tax in taxRate">

        <div class="" >
            {{ Form::label('min_amount','Minimum Amount', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::number('', null, ['v-model'=>'tax.	','class' => 'form-control', 'placeholder' => 'Minimum Amount']) }}
            </div><!--col-xs-4-->

            {{ Form::label('max_amount','Maximum Amount', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::number('', null, ['v-model'=>'tax.max_amount','class' => 'form-control', 'placeholder' => 'Maximum Amount']) }}
            </div><!--col-xs-4-->

        </div><!--form control-->

        <div class="">
            {{ Form::label('percent', 'Percent %', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::number('', null, ['v-model'=>'tax.percent','class' => 'form-control', 'placeholder' => 'Percent']) }}
            </div><!--col-xs-4-->

            {{ Form::label('', 'Deduction Amount', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::number('', null, ['v-model'=>'tax.deduction_amount','class' => 'form-control', 'placeholder' => 'Deduction Amount']) }}
            </div><!--col-xs-4-->

        </div><!--form control-->
        </div>

    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.settings.tax_rate_settings.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            <button class="Form__button btn btn-danger btn-xs " @click="removeTaxRate">Remove Tax Rate</button>
        </div><!--pull-left-->

        <div class="pull-right">
            <button class="Form__button btn btn-success btn-xs " @click="addTaxRate">Add Tax Rate</button>
            {{ Form::submit($buttonText, ['class' => 'btn btn-success btn-xs']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
