<div class="pull-right mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Tax Rate Settings <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.settings.tax_rate_settings.index', 'All Tax Rates') }}</li>
            @permission('manage-users')
                <li>{{ link_to_route('admin.settings.tax_rate_settings.create', 'Create Tax Rate') }}</li>
            @endauth
         </ul>
    </div><!--btn group-->


</div><!--pull right-->

<div class="clearfix"></div>
