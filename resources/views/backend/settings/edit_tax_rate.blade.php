@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management') . ' | ' . trans('payroll.label.create'))

@section('page-header')
    <h1>
        Tax Rate Setting
        <small>Edit New tax Rate</small>
    </h1>
@endsection

@section('content')
    <div class="col-md-9">
    {{ Form::model($payroll,['route' => ['admin.settings.tax_rate_settings.update',$payroll->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
       @include('backend.settings.includes.partials._form_tax_rate',[$pageName = 'edit',$buttonText = trans('buttons.general.crud.edit') ])
    {{ Form::close() }}
    </div>

    <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('payroll.label.break_down')  }}</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-footer no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">
                            {{ trans('payroll.label.net_salary')  }}
                      <span class="pull-right text-red"> 12</span></a></li>
                    <li><a href="#">
                            {{ trans('payroll.label.taxable_income')  }}
                        <span class="pull-right text-green"> 4</span></a>
                    </li>
                    <li><a href="#">
                            {{ trans('payroll.label.income_tax')  }}
                     <span class="pull-right text-yellow"> 0</span></a></li>

                    <li><a href="#">
                            {{ trans('payroll.label.income_tax')  }}
                     <span class="pull-right text-yellow"> 0</span></a></li>

                    <li><a href="#">
                            {{ trans('payroll.label.company_pension')  }}
                     <span class="pull-right text-yellow"> 0</span></a></li>

                    <li><a href="#">
                            {{ trans('payroll.label.employee_pension')  }}
                     <span class="pull-right text-yellow"> 0</span></a></li>

                    <li><a href="#">
                            {{ trans('payroll.label.total_deduction')  }}
                     <span class="pull-right text-yellow"> 0</span></a></li>

                    <li><a href="#">
                           <strong> {{ trans('payroll.label.net_salary')  }}
                     <span class="pull-right text-yellow"> 0 </span></a></li>
                            </strong>
                </ul>
            </div>
            <!-- /.footer -->
        </div>
    </div>
@stop

@section('after-scripts-end')

@stop
