@extends ('backend.layouts.app')

@section ('title', trans('back_pay_payroll.menus.back_pay_payroll.management') . ' | ' . trans('back_pay_payroll.label.create'))

@section('page-header')
    <h1>
       Payroll Setting
        <small>Create New payroll setting</small>
    </h1>
@endsection

@section('content')
    <div class="col-md-9">
    {{ Form::open(['route' => 'admin.settings.payroll_settings.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
       @include('backend.settings.includes.partials._form_payroll',[$pageName = 'create',$buttonText = trans('buttons.general.crud.create') ])
    {{ Form::close() }}
    </div>

    <div class="col-md-3">

    </div>
@stop

@section('after-scripts-end')

@stop
