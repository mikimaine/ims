@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management') . ' | ' . trans('payroll.label.create'))

@section('page-header')
    <h1>
        Payroll Setting
        <small>Edit Payroll Setting</small>
    </h1>
@endsection

@section('content')
    <div class="col-md-9">
    {{ Form::model($payroll,['route' => ['admin.settings.payroll_settings.update',$payroll->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
       @include('backend.settings.includes.partials._form_payroll',[$pageName = 'edit',$buttonText = trans('buttons.general.crud.edit') ])
    {{ Form::close() }}
    </div>

    <div class="col-md-3">

    </div>
@stop

@section('after-scripts-end')

@stop
