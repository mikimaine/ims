@extends ('backend.layouts.app')

@section ('title', trans('back_pay_payroll.menus.back_pay_payroll.management') . ' | ' . trans('back_pay_payroll.label.create'))

@section('page-header')
    <h1>
       Department Management
        <small>Create New Department</small>
    </h1>
@endsection

@section('content')
    <div class="col-md-9">
    {{ Form::open(['route' => 'admin.settings.department_settings.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
       @include('backend.settings.includes.partials._form_department',[$pageName = 'create',$buttonText = trans('buttons.general.crud.create') ])
    {{ Form::close() }}
    </div>

    <div class="col-md-3">

    </div>
@stop

@section('after-scripts-end')

@stop
