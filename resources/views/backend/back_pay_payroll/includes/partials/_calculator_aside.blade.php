<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('payroll.label.break_down')  }}</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-footer no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="#">
                    Gross Pay
                    <span class="pull-right text-red">
                    <p v-text="gross_pay" ></p>
                    </span></a></li>
            <li><a href="#">
                    Taxable Income
                    <span class="pull-right text-green">
                        <p v-text="taxable_income"></p>
                    </span></a>
            </li>
            <li><a href="#">
                    Prev. Income Tax
                    <span class="pull-right text-yellow">
                        <p v-text="prev_income_tax"></p>
                    </span></a></li>

            <li><a href="#">
                   New Income Tax
                    <span class="pull-right text-yellow">
                     <p v-text="new_income_tax"></p>
                    </span></a></li>


            <li><a href="#">
                    company Pension
                    <span class="pull-right text-yellow">
                    <p v-text="company_pension"> </p>
                    </span></a></li>
            <li><a href="#">
                    Employee Pension
                    <span class="pull-right text-yellow">
                    <p v-text="employee_pension"> </p>
                    </span></a></li>
            <li><a href="#">
                    Total Deduction
                    <span class="pull-right text-yellow">
                    <p v-text="total_deduction"> </p>
                    </span></a></li>
            <li><a href="#">
                    Net Income Tax
                    <span class="pull-right text-yellow">
                    <p v-text="net_income_tax"> </p>
                    </span></a></li>

            <li><a href="#">
                    <strong> {{ trans('payroll.label.net_salary')  }}
                        <span class="pull-right text-yellow">
                        <p v-text="net_salary"></p>
                        </span></strong></a></li>
            </strong>
        </ul>
    </div>
    <!-- /.footer -->
</div>