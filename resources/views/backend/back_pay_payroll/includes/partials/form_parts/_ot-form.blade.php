<div class="form-group">
    {{ Form::label('daily_labour_ot_1_25', trans('payroll.label.main_payroll.table.ot_1_25'), ['class' => 'col-lg-2 control-label']) }}
    <div class="col-xs-4">
        {{ Form::text('daily_labour_ot_1_25', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.ot_1_25')]) }}
    </div><!--col-xs-4-->
    {{ Form::label('daily_labour_ot_1_5', trans('payroll.label.main_payroll.table.ot_1_5'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('daily_labour_ot_1_5', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.ot_1_5')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->

<div class="form-group">

    {{ Form::label('daily_labour_ot_2_0', trans('payroll.label.main_payroll.table.ot_2_0'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('daily_labour_ot_2_0', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.ot_2_0')]) }}
    </div><!--col-xs-4-->
    {{ Form::label('daily_labour_ot_2_5', trans('payroll.label.main_payroll.table.ot_2_5'), ['class' => 'col-lg-2 control-label']) }}

    <div class="col-xs-4">
        {{ Form::text('daily_labour_ot_2_5', null, ['class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.ot_2_5')]) }}
    </div><!--col-xs-4-->
</div><!--form control-->

