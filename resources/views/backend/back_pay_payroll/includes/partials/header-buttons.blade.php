<div class="pull-right mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('back_pay_payroll.menus.back_pay_payroll.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.back-pay-payroll.back_pay_payroll.index', trans('back_pay_payroll.menus.back_pay_payroll.all')) }}</li>

            @permission('manage-users')
                <li>{{ link_to_route('admin.back-pay-payroll.back_pay_payroll.create', trans('back_pay_payroll.menus.back_pay_payroll.create')) }}</li>
            @endauth

            <li class="divider"></li>
             <li>{{ link_to_route('admin.back-pay-payroll.back_pay_payroll.deleted', trans('back_pay_payroll.menus.back_pay_payroll.deleted')) }}</li>
        </ul>
    </div><!--btn group-->


</div><!--pull right-->

<div class="clearfix"></div>
