<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('back_pay_payroll.label.create') }}</h3>

        <div class="box-tools pull-right">
            @include('backend.back_pay_payroll.includes.partials.header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div  v-for="error in formErrors">
            <div class="alert alert-danger alert-dismissible" id="notify" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <p   v-text="error[0]"  ></p>
            </div>
        </div>

        <div class="form-group ">
            {{ Form::label('back_pay_project_id', trans('payroll.label.main_payroll.table.project_code'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-xs-4 ">
                <select class="form-control"  v-model="back_pay_project_id"  required>
                    <option value="">Select Department Name (Code)</option>
                    @foreach ($departments as $department)
                        <option value="{!! $department->id !!}">{!! $department->project_name !!} ({!! $department->project_code !!})</option>
                    @endforeach
                </select>
            </div><!--col-xs-4-->
            {{ Form::label('back_pay_employee_id', trans('payroll.label.main_payroll.table.employee_name'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-xs-4">
                <select  class="form-control"  v-model="back_pay_employee_id" required>
                    <option value="">-- Select Employee --</option>
                    <option
                            v-for="option in optionsE"
                            track-by="$index"
                            :value="option.id"
                            v-text="option.text">
                    </option>
                </select>
            </div><!--col-xs-4-->
        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('back_pay_bank_account', trans('payroll.label.main_payroll.table.bank_account'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_bank_account', null, ['v-model'=>'back_pay_bank_account','class' => 'form-control', 'placeholder' => trans('payroll.label.main_payroll.table.bank_account')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('back_pay_permanent_employee', trans('payroll.label.main_payroll.table.permanent'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::checkbox('back_pay_permanent_employee', '1', true) }}
            </div><!--col-xs-4-->
        </div><!--form control-->


        <div class="form-group">
            {{ Form::label('back_pay_no_of_months', trans('back_pay_payroll.label.back_pay_payroll.table.no_of_months'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_no_of_months', null, ['v-model'=>'back_pay_no_of_months','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.no_of_months')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('back_pay_previous_salary', trans('back_pay_payroll.label.back_pay_payroll.table.previous_salary'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_previous_salary', null, ['v-model'=>'back_pay_previous_salary','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.previous_salary')]) }}
            </div><!--col-xs-4-->

        </div><!--form control-->
        <div class="form-group">
            {{ Form::label('back_pay_new_salary', trans('back_pay_payroll.label.back_pay_payroll.table.new_salary'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_new_salary', null, ['v-model'=>'back_pay_new_salary','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.new_salary')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('back_pay_previous_taxable_income', trans('back_pay_payroll.label.back_pay_payroll.table.previous_taxable_income'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_previous_taxable_income', null, ['v-model'=>'back_pay_previous_taxable_income','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.previous_taxable_income')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->
        <div class="form-group">
            {{ Form::label('back_pay_new_taxable_incomes', trans('back_pay_payroll.label.back_pay_payroll.table.new_taxable_incomes'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_new_taxable_incomes', null, ['v-model'=>'back_pay_new_taxable_incomes','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.new_taxable_incomes')]) }}
            </div><!--col-xs-4-->

            {{ Form::label('back_pay_previous_non_taxable_income', trans('back_pay_payroll.label.back_pay_payroll.table.previous_non_taxable_income'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-xs-4">
                {{ Form::text('back_pay_previous_non_taxable_income', null, ['v-model'=>'back_pay_previous_non_taxable_income','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.previous_non_taxable_income')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->
        <div class="form-group">
            {{ Form::label('back_pay_new_non_taxable_income', trans('back_pay_payroll.label.back_pay_payroll.table.new_non_taxable_income'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-xs-4">
                {{ Form::text('back_pay_new_non_taxable_income', null, ['v-model'=>'back_pay_new_non_taxable_income','class' => 'form-control', 'placeholder' => trans('back_pay_payroll.label.back_pay_payroll.table.new_non_taxable_income')]) }}
            </div><!--col-xs-4-->
        </div><!--form control-->
    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.daily-payroll.daily_payroll.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit($buttonText, ['class' => 'btn btn-success btn-xs']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
