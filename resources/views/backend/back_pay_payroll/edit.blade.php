@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management') . ' | ' . trans('payroll.label.create'))

@section('page-header')
    <h1>
        {{ trans('payroll.menus.main_payroll.management') }}
        <small>{{ trans('payroll.label.create') }}</small>
    </h1>
@endsection

@section('content')
    <div id="break_down"  v-cloak >
    <div class="col-md-9">
    {{ Form::open(['route' => ['admin.back-pay-payroll.back_pay_payroll.update',$payroll->id], 'class' => 'form-horizontal','@submit.prevent'=>'submitForm','role' => 'form', 'method' => 'PATCH']) }}
       @include('backend.back_pay_payroll.includes.partials._form',[$pageName = 'edit',$buttonText = trans('buttons.general.crud.edit') ])
    {{ Form::close() }}
    </div>
    <div class="col-md-3">
        @include('backend.back_pay_payroll.includes.partials._calculator_aside')
    </div>
    </div>
@stop

@section('after-scripts-end')
    <script>
        var vm = new Vue({
            el:'#break_down',
            data:{
                back_pay_project_id                   : payroll.back_pay_project_id,
                back_pay_employee_id                  : payroll.back_pay_employee_id,
                back_pay_bank_account                 : payroll.back_pay_bank_account,
                back_pay_permanent_employee           : payroll.back_pay_permanent_employee,
                back_pay_no_of_months                 : payroll.back_pay_no_of_months,
                back_pay_previous_salary              : payroll.back_pay_previous_salary,
                back_pay_new_salary                   : payroll.back_pay_new_salary,
                back_pay_previous_taxable_income      : payroll.back_pay_previous_taxable_income,
                back_pay_new_taxable_incomes          : payroll.back_pay_new_taxable_incomes,
                back_pay_previous_non_taxable_income  : payroll.back_pay_previous_non_taxable_income,
                back_pay_new_non_taxable_income       : payroll.back_pay_new_non_taxable_income,
                formErrors: {},
                optionsE: [],
                errorIs: false,
            },
            created: function () {
                $.ajax({
                    context: this,
                    url: ' {{ env('PAYROLL_URL') }} ',
                    success: function (result) {
                        //console.log(result);
                        this.formatDataE(result);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });
            },
            computed:{
                gross_pay: function () {
                    return (this.calc_gross_pay() * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                taxable_income: function () {
                    return (this.calc_taxable_income() * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                prev_income_tax: function () {
                    return (this.calc_prev_income_tax() * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                new_income_tax: function(){
                    return (this.calc_new_income_tax() * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                company_pension: function () {
                    return (this.calc_company_pension() * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                employee_pension: function () {
                    return (this.calc_employee_pension() * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                net_income_tax: function () {
                    return ((this.calc_new_income_tax() -
                    this.calc_prev_income_tax()) * parseInt(this.back_pay_no_of_months)).toFixed(2);
                },
                total_deduction: function () {
                    return this.calc_total_deduction();
                },
                net_salary: function () {
                    return ((this.calc_gross_pay() * parseInt(this.back_pay_no_of_months)) -
                    this.calc_total_deduction()).toFixed(2)
                }
            },
            watch: {
                formErrors: function (errorsQ) {
                    if (this.errorIs == true){
                        this.notify_Fadeout();
                    }
                }
            },
            methods: {
                notify_Fadeout: _.debounce(
                        function () {
                            var self = this;
                            $('#notify').delay(3000).fadeOut(400,function () {
                                self.errorIs = true;
                                self.formErrors = {};
                            });
                        },
                        500),
                /**
                 * @TODO
                * We really have to think about this ......
                 * @returns {number}
                 */
                calc_total_deduction : function () {
                  return (((this.calc_new_income_tax() -
                                this.calc_prev_income_tax()) *
                                parseInt(this.back_pay_no_of_months)) +
                                (this.calc_employee_pension() * parseInt(this.back_pay_no_of_months))).toFixed(2);
                },
                calc_gross_pay: function () {
//                    console.log(tax_rates);
                    return Number(parseFloat(this.back_pay_new_salary) +
                                    parseFloat(this.back_pay_new_taxable_incomes) +
                                    parseFloat(this.back_pay_new_non_taxable_income)) -
                            Number(parseFloat(this.back_pay_previous_salary) +
                                    parseFloat(this.back_pay_previous_taxable_income) +
                                    parseFloat(this.back_pay_previous_non_taxable_income));
                },
                calc_taxable_income: function () {
                    return (Number(parseFloat(this.back_pay_new_salary) +
                            parseFloat(this.back_pay_new_taxable_incomes)) -
                    Number(parseFloat(this.back_pay_previous_salary) +
                            parseFloat(this.back_pay_previous_taxable_income))).toFixed(2);
                },
                calc_prev_taxable_income: function () {
                    return Number((parseFloat(this.back_pay_previous_taxable_income) +
                    parseFloat(this.back_pay_previous_salary)).toFixed(2));
                },
                calc_new_taxable_income: function () {
                    return Number((parseFloat(this.back_pay_new_taxable_incomes) +
                    parseFloat(this.back_pay_new_salary)).toFixed(2));
                },
                calc_prev_income_tax: function () {
                    return ((parseFloat(this.calc_prev_taxable_income()) *
                    (parseInt(this.calc__prev_tax_rate().percent) / 100))
                    - parseFloat(this.calc__prev_tax_rate().deduction_amount)).toFixed(2);
                },
                calc_new_income_tax: function () {
                    return Number(parseFloat(this.calc_new_taxable_income()) * (parseInt(this.calc__new_tax_rate().percent) / 100))
                            - parseFloat(this.calc__new_tax_rate().deduction_amount)
                },
                calc_income_tax: function () {
                    return Number(parseFloat(this.calc_taxable_income()) * (parseInt(this.calc_tax_rate().percent) / 100))
                            - parseFloat(this.calc_tax_rate().deduction_amount)
                },
                calc__prev_tax_rate: function () {
                    var rates = Object.keys(tax_rates).map(function (key) {
                        return tax_rates[key];
                    });
                    var self = this;
                    var result = {};
                    rates.forEach(function (tax_rate) {
                        if (parseFloat(self.calc_prev_taxable_income()) >= parseFloat(tax_rate.min_amount)
                                && parseFloat(self.calc_prev_taxable_income()) <= parseFloat(tax_rate.max_amount)
                        ) {
                            result = tax_rate;
                        }
                    });
                    return result;
                },
                calc__new_tax_rate: function () {
                    var rates = Object.keys(tax_rates).map(function (key) {
                        return tax_rates[key];
                    });
                    var self = this;
                    var result = {};
                    rates.forEach(function (tax_rate) {
                        if (parseFloat(self.calc_new_taxable_income()) >= parseFloat(tax_rate.min_amount)
                                && parseFloat(self.calc_new_taxable_income()) <= parseFloat(tax_rate.max_amount)
                        ) {
                            result = tax_rate;
                        }

                    });
                    return result;
                }
                ,
                calc_tax_rate: function () {
                    var rates = Object.keys(tax_rates).map(function (key) {
                        return tax_rates[key];
                    });
                    var self = this;
                    var result = {};
                    rates.forEach(function (tax_rate) {
                        if (parseFloat(self.calc_taxable_income()) >= parseFloat(tax_rate.min_amount)
                                && parseFloat(self.calc_taxable_income()) <= parseFloat(tax_rate.max_amount)
                        ) {
                            result = tax_rate;
                        }

                    });
                    return result;
                },
                calc_company_pension: function () {
                    return Number((parseFloat(company_pension_rate) / 100) * (this.back_pay_new_salary - this.back_pay_previous_salary ));
                },
                calc_employee_pension: function () {
                    return Number((parseFloat(employee_pension_rate) / 100) * (this.back_pay_new_salary - this.back_pay_previous_salary ));
                },
                submitForm: function (e) {
                    var form = e.srcElement;
                    var action = form.action;
                    var csrfToken = form.querySelector('input[name="_token"]').value;
                    var datas = {
                        back_pay_project_id:                this.back_pay_project_id,
                        back_pay_employee_id:               this.back_pay_employee_id,
                        back_pay_bank_account:              this.back_pay_bank_account,
                        back_pay_permanent_employee:        this.back_pay_permanent_employee,
                        back_pay_no_of_months:              this.back_pay_no_of_months,
                        back_pay_previous_salary:           this.back_pay_previous_salary,
                        back_pay_new_salary:                this.back_pay_new_salary,
                        back_pay_previous_taxable_income:   this.back_pay_previous_taxable_income,
                        back_pay_new_taxable_incomes:       this.back_pay_new_taxable_incomes,
                        back_pay_previous_non_taxable_income: this.back_pay_previous_non_taxable_income,
                        back_pay_new_non_taxable_income:    this.back_pay_new_non_taxable_income,
                    };
                    this.$http.put(action, datas, {
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        }
                    }).then(function (response) {
                        console.log(response);
                        form.submit();
                    }).catch(function (data, status, request) {
                        var errors = data.data;
                        this.errorIs = true;
                        this.formErrors = errors;
                    });
                },
                formatDataE: function (result) {
                    var self = this;
                    var obj = Object.create(result);
                    Object.keys(obj.__proto__).forEach(function (key) {
                        self.optionsE.push({id: key, text: obj[key]});
                    });
                },
                format_error: function (error_message) {

                },
                showAlertMessage: function (notification) {

                },
            }

        })
    </script>
@stop
