@extends ('backend.layouts.app')

@section ('title', trans('payroll.menus.main_payroll.management'))

@section('before-styles-end')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
    <style type="text/css">
        #invoice {
            font-size: 10px;
            line-height: 2;
        }
        @media print {
            * {
                font-size: 10px;
                line-height: 2;
                visibility: hidden;
            }
            #invoice, #invoice * {
                visibility: visible;

            }
        }
    </style>
@stop

@section('page-header')
    <h1>
        {{ trans('report.page_name') }}
        <small> #BB233</small>
    </h1>
@endsection

@section('content')


    <div id="report">
        <div class="pad margin no-print">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
                <h4><i class="fa fa-info"></i> Note:</h4>
                This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
            </div>
        </div>

        <div class="pad margin no-print" >
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-gear"></i>

                    <h3 class="box-title">Setting</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="col-md-9">
                        <div class="box box-solid">
                            <div class="box-header with-border">

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p class="lead">Filter Setting</p>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"   v-on:click="thisMonth = !thisMonth" v-model="thisMonth" >  {{ \Carbon\Carbon::now()->firstOfMonth()->toFormattedDateString() }} -
                                            {{ \Carbon\Carbon::now()->lastOfMonth()->toFormattedDateString() }}
                                            <small class="label label-danger"><i class="fa fa-calendar-check-o"></i> (i.e This Month)</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row" v-if="!thisMonth" >
                                    <div class="col-md-3">
                                        <label>Months & year :</label>
                                    </div>
                                    <div class="col-md-4">
                                        {{  Form::selectMonth('month', null,['v-model'=>'selectedMonth','class'=>'form-control'])  }}
                                    </div>
                                    <div class="col-md-4">
                                        {{ Form::selectYear('year', \Carbon\Carbon::now()->year-3 ,\Carbon\Carbon::now()->year+8 ,null,['v-model'=>'selectedYear','class'=>'form-control']) }}                            <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                    {{ Form::label('back_pay_project_id', trans('payroll.label.main_payroll.table.project_code'), ['class' => 'col-lg-12 control-label']) }}
                                    </div>
                                        <div class="col-md-4">
                                        <select class="form-control"  v-model="back_pay_project_id"  required>
                                            <option value="">Select Department Name (Code)</option>
                                            @foreach ($departments as $department)
                                                <option value="{!! $department->id !!}">{!! $department->project_name !!} ({!! $department->project_code !!})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>

            </div>

        </div>
        <div class="">
        <!-- Main content -->
        <section class="invoice"  id="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        {{ HTML::image('img/backend/logo.jpg') }} {{ trans('report.company_name') }}
                        <small class="pull-right">{{ \Carbon\Carbon::now()->toFormattedDateString() }}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    Kind of Document
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    Issue No:
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>Document No</b><br>
                    <br>
                    <b>Page No:</b> <br>
                </div>
                <!-- /.col -->
            </div><br>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table id="" class="table table-striped ">
                        <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                            <th>Employee Name</th>
                            <th>No of Month</th>
                            <th>New Salary</th>
                            <th>Prev Salary</th>
                            <th>New Taxable Income</th>
                            <th>Prev Taxable Income</th>
                            <th>New Non Taxable Income</th>
                            <th>Prev Non Taxable Income</th>
                            <th>Taxable Income</th>
                            <th>Gross Pay</th>
                            <th>New Income Tax</th>
                            <th>Prev Income Tax</th>
                            <th>Net Income Tax</th>
                            <th>Company Pension</th>
                            <th>Empl Pension</th>
                            <th>Total Deduction</th>
                            <th>Net Income</th>
                            <th>Sign</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr  v-for="item in filterdData">
                             <td v-text="item.id" ></td>
                             <td v-text="getEmployeeName(item.back_pay_employee_id)" ></td>
                             <td v-text="item.back_pay_no_of_months" ></td>
                             <td v-text="formatNumber(item.back_pay_new_salary)" ></td>
                             <td v-text="formatNumber(item.back_pay_previous_salary)" ></td>
                             <td v-text="formatNumber(item.back_pay_new_taxable_incomes)" ></td>
                             <td v-text="formatNumber(item.back_pay_previous_taxable_income)" ></td>
                             <td v-text="formatNumber(item.back_pay_new_non_taxable_income)" ></td>
                             <td v-text="formatNumber(item.back_pay_previous_non_taxable_income)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_taxable_income)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_gross_pay)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_new_income_tax)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_pre_income_tax)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_net_income_tax)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_company_pension)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_employee_pension)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_total_deduction)" ></td>
                             <td v-text="formatNumber(item.payment.back_pay_payment_net_salary)" ></td>
                               <td></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row" id="payment">
                <div class="col-xs-6 pull-right">
                    <p class="lead">Amount Due {{ \Carbon\Carbon::now()->toFormattedDateString() }}</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                                <td ><span v-text="formatNumber(getSubTotal)"></span></td>
                            </tr>
                            <tr>
                                <th>Tax</th>
                                <td><span v-text="formatNumber(getIncomeTax)"></span></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td><span v-text="formatNumber(totalPayroll)"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->.
            </div>
            <!-- /.row -->
            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="" target="_blank" class="btn btn-default" onclick="window.print()" ><i class="fa fa-print"></i> Print </a>
                    <!--
                    <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                    </button>
                    <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </button>
                    -->
                </div>
            </div>
        </section>
      </div>
        <!-- /.content -->
        <div class="clearfix"></div>
    </div>
@stop

@section('after-scripts-end')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
    {{ Html::script("js/backend/plugin/date/moment.min.js") }}
    <script>
        var report = new Vue({
            el: '#report',
            data:{
                back_pay_project_id: 0,
                tableData: {},
                finalData: {},
                Projects: [],
                thisMonth: true,
                selectedMonth: 0,
                selectedYear: 0,
                Employees: [],
                total_net_salary: 0,
                total_income_tax: 0,
            },
            created: function() {
                $.ajax({
                    context: this,
                    url: ' {{ env('PAYROLL_URL') }} ',
                    success: function (result) {
                        //console.log(result);
                        this.formatData(result,this.Employees);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });
                $.ajax({
                    context: this,
                    url: '{{ route("admin.back-pay-payroll.back_pay_payroll.index") }}',
                    success: function (result) {
                        this.formatTable(result);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });

            },
            computed: {

              getSubTotal: function(){
                return parseFloat(this.getTotalNetSalary());
              },
              getIncomeTax: function(){
                return parseFloat(this.getTotalIncomeTax());
              },
              totalPayroll: function(){
                  return this.getTotal();
              },
              filterdData: function () {
                  var lastData = {};
                  if(this.tableData.length > 0){
                      var self = this;
                      var obj =  this.tableData.filter(function (el) {

                          var created_month = moment(el.updated_at).month() + 1;
                          var created_year = moment(el.updated_at).year();
                           if( self.thisMonth === true){
                               var today_month = moment().month() + 1;
                               var today_year  = moment().year();
                               if(created_month === today_month && created_year === today_year){
                                   return el;
                               }
                           }else{
                                if (self.selectedMonth != 0 && self.selectedYear != 0){
                                    //console.log(created_month);
                                    if(created_month == self.selectedMonth && created_year == self.selectedYear){
                                        return el
                                    }
                                }
                               //return el;
                           }
                       });
                  //  console.log(obj);
                      self.total_net_salary = 0;
                      self.total_income_tax = 0;
                      self.finalData = obj;
                      return obj;
                  }
                  return this.finalData;
                }

            },
            methods:  {
                formatNumber(number){
                 return new Intl.NumberFormat().format(number);
                },
                getTotal: function (){
                    this.setTotalNetSalary();
                    this.setIncomeTax();
                  return Number((parseFloat(this.getTotalNetSalary() + this.getTotalIncomeTax())).toFixed(2))
                },
                setTotalNetSalary: function (){
                  var data = this.finalData;
                  var self = this;
                    Object.keys(data).forEach(function(key) {
                        self.total_net_salary += Number(parseFloat(data[key].payment.back_pay_payment_net_salary));
                    });
                },
                getTotalNetSalary: function (){
                  //this.setTotalNetSalary();
                    return Number((parseFloat(this.total_net_salary)).toFixed(2));
                },
                setIncomeTax: function (){
                  var data = this.finalData;
                  var self = this;
                    Object.keys(data).forEach(function(key) {
                        self.total_income_tax += parseFloat(data[key].payment.back_pay_payment_net_income_tax);
                    });
                },
                getTotalIncomeTax: function(){
                    //  this.setIncomeTax();
                      return Number((parseFloat(this.total_income_tax)).toFixed(2));
                },
                formatData: function (result,objectName) {
                  var obj = Object.create(result) ;
                    Object.keys(obj.__proto__).forEach(function(key) {
                        objectName.push({id:key,text:obj[key]});
                    });
                },
                formatTable: function(result){
                    this.tableData = result;
                },
                getEmployeeName: function(empid){
                        var self = this;
                        var result = 'Employee has no appropriate data / Connection Error';
                    this.Employees.forEach(function (key,val) {
                        if (parseInt(val) === parseInt(empid)){
                            result = key.text;
                        }
                    })
                    return result;
                }

            }


        })



    </script>
@stop
