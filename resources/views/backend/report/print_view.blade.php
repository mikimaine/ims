<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Afro-Tsion  | Payroll</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @langRTL
    {{ Html::style(elixir('css/backend-rtl.css')) }}
    {{ Html::style(elixir('css/rtl.css')) }}
    @else
        {{ Html::style(elixir('css/backend.css')) }}
    @endif
    <style type="text/css">
        * {
            font-size: 10px;
            line-height: 2;
        }
        @media print {

            .printme, .printme * {
                visibility: visible;
            }
            .printme {
                position: absolute;
                left: 0;
                top: 0;
            }
            .printme, .printme:last-child {
                page-break-after: avoid;
            }

            .display-none-on, .display-none-on * {
                display: none !important;
            }
            html, body {
                height: auto;
                font-size: 12pt; /* changing to 10pt has no impact */
            }
        }
    </style>
</head>
<body onload="window.print();">
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <div id="report">
            <!-- Main content -->
            <section class="invoice">
                <!-- title row -->
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            {{ HTML::image('img/backend/logo.jpg') }} {{ trans('report.company_name') }}
                            <small class="pull-right">{{ \Carbon\Carbon::now()->toFormattedDateString() }}</small>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Kind of Document
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Issue No:
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>Document No</b><br>
                        <br>
                        <b>Page No:</b> <br>
                    </div>
                    <!-- /.col -->
                </div><br>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table id="" class="table table-striped ">
                            <thead>
                            <tr>
                                <th>Employee Name</th>
                                <th>Working Day</th>
                                <th>Basic Salary</th>
                                <th>Tax Allo</th>
                                <th>OT</th>
                                <th>Perdiem</th>
                                <th>Taxable Income</th>
                                <th>N Taxable Income</th>
                                <th>Company Pension</th>
                                <th>Gross Pay</th>
                                <th>Income Tax</th>
                                <th>Emplo Pension</th>
                                <th>Loan</th>
                                <th>Advance</th>
                                <th>Other Deduction</th>
                                <th>total Deduction</th>
                                <th>Net Income</th>
                                <th>Sign</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr  v-for="item in tableData">
                              <td v-text="getEmployeeName(item.payroll_main_payroll_employee_id)" ></td>
                              <td v-text="item.payroll_main_working_day" ></td>
                              <td v-text="formatNumber(item.payroll_main_basic_salary)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_taxable_allowance)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_total_ot)" ></td>
                              <td v-text="formatNumber(item.payroll_main_perdiem)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_taxable_income)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_non_taxable_allowance)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_company_pension)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_gross_pay)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_income_tax)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_employee_pension)" ></td>
                              <td v-text="formatNumber(item.payroll_main_loan)" ></td>
                              <td v-text="formatNumber(item.payroll_main_advance)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_other_deduction)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_total_deduction)" ></td>
                              <td v-text="formatNumber(item.payment.main_payment_net_salary)" ></td>
                             <td></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                                      <!-- /.col -->
                  <div class="col-xs-6 pull-right">
                      <p class="lead">Amount Due {{ \Carbon\Carbon::now()->toFormattedDateString() }}</p>

                      <div class="table-responsive">
                          <table class="table">
                              <tr>
                                  <th style="width:50%">Subtotal:</th>
                                  <td ><span v-text="formatNumber(getSubTotal)"></span></td>
                              </tr>
                              <tr>
                                  <th>Tax</th>
                                  <td><span v-text="formatNumber(getIncomeTax)"></span></td>
                              </tr>
                              <tr>
                                  <th>Total:</th>
                                  <td><span v-text="formatNumber(totalPayroll)"></span></td>
                              </tr>
                          </table>
                      </div>
                  </div>
                    <!-- /.col -->.
        </div>
    </section>
    <!-- /.content -->
</div>
        </section>
    </div>
<!-- ./wrapper -->
    {{ Html::script(elixir('js/backend.js')) }}
    <script>
        var report = new Vue({
            el: '#report',
            data:{
                tableData: {},
                Projects: [],
                Employees: [],
                total_net_salary: 0,
                total_income_tax: 0,
            },
            created: function() {
                $.ajax({
                    context: this,
                    url: 'http://localhost/afro/api/project_payroll',
                    success: function (result) {
                        this.formatData(result,this.Projects);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });

                $.ajax({
                    context: this,
                    url: 'http://localhost/afro/api/employee_payroll',
                    success: function (result) {
                        //console.log(result);
                        this.formatData(result,this.Employees);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });
                $.ajax({
                    context: this,
                    url: '{{ route("admin.payroll.main_payroll.index") }}',
                    success: function (result) {
                        this.formatTable(result);
                    },error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                });

            },
            computed: {

              getSubTotal: function(){
                return parseFloat(this.getTotalNetSalary());
              },
              getIncomeTax: function(){
                return parseFloat(this.getTotalIncomeTax());
              },
              totalPayroll: function(){
                  return this.getTotal();
              }

            },
            methods:  {
                formatNumber(number){
                 return new Intl.NumberFormat().format(number);
                },
                getTotal: function (){
                    this.setTotalNetSalary();
                    this.setIncomeTax();
                  return Number((parseFloat(this.getTotalNetSalary() + this.getTotalIncomeTax())).toFixed(2))
                },
                setTotalNetSalary: function (){
                  var data = this.tableData;
                  var self = this;
                    Object.keys(data).forEach(function(key) {
                        self.total_net_salary += Number(parseFloat(data[key].payment.main_payment_net_salary));
                    });
                },
                getTotalNetSalary: function (){
                  //this.setTotalNetSalary();
                    return Number((parseFloat(this.total_net_salary)).toFixed(2));
                },
                setIncomeTax: function (){
                  var data = this.tableData;
                  var self = this;
                    Object.keys(data).forEach(function(key) {
                        self.total_income_tax += parseFloat(data[key].payment.main_payment_income_tax);
                    });
                },
                getTotalIncomeTax: function(){
                    //  this.setIncomeTax();
                      return Number((parseFloat(this.total_income_tax)).toFixed(2));
                },
                formatData: function (result,objectName) {
                  var obj = Object.create(result) ;
                    Object.keys(obj.__proto__).forEach(function(key) {
                        objectName.push({id:key,text:obj[key]});
                    });
                },
                formatTable: function(result){
                    this.tableData = result;
                },
                getEmployeeName: function(empid){
                        var self = this;
                        var result = 'Employee has no appropriate data / Connection Error';
                    this.Employees.forEach(function (key,val) {
                        if (parseInt(val) === parseInt(empid)){
                            result = key.text;
                        }
                    })
                    return result;
                }

            }


        })



    </script>
</body>
</html>
