/**
 * Created by mikimiane on 10/17/16.
 */
//Initialize Select2 Elements
$(".select2").select2();
/**
 * URL for
 * @type {string}
 */
const PROJECT_URL = 'http://localhost/afro/api/project_payroll';
const EMPLOYEE_URL = 'http://localhost/afro/api/employee_payroll';
/**
 * if there is any select 2 element that requires API call to the server
 * it will be bind here
 */
Vue.component('select2', {
    props: ['options', 'value'],
    template: '#select2-project-template',
    mounted: function () {
        var vm = this
        $(this.$el)
            .val(this.value)
            // init select2
            .select2({ data: this.options })
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', this.value)
            })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el).select2('val', value)
        },
        options: function (options) {
            // update options
            $(this.$el).select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});
Vue.component('select23', {
    props: ['options', 'value'],
    template: '#select2-employee-template',
    mounted: function () {
        var vm = this
        $(this.$el)
            .val(this.value)
            // init select2
            .select2({ data: this.options })
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', this.value)
            })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el).select2('val', value)
        },
        options: function (options) {
            // update options
            $(this.$el).select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }

});
var vm = new Vue({
    el: '#projectList',
    template: '#project-drop-down',
    data: {
        selected: 0,
        options: []
    },
    created: function() {
        $.ajax({
            context: this,
            url: PROJECT_URL,
            success: function (result) {
                this.formatData(result);
            },error: function (request, status, error) {
                console.log(request.responseText);
            }
        })
    },
    methods: {
        formatData: function (result) {
            var self = this;
            var obj = Object.create(result) ;
            Object.keys(obj.__proto__).forEach(function(key) {
                self.options.push({id:key,text:obj[key]});
            });
        }
    }
});


var vm = new Vue({
    el: '#employeeList',
    template: '#employee-drop-down',
    data: {
        selected: 0,
        options: []
    },
    created: function() {
        $.ajax({
            context: this,
            url: EMPLOYEE_URL,
            success: function (result) {
                this.formatData(result);
            },error: function (request, status, error) {
                console.log(request.responseText);
            }
        })
    },
    methods: {
        formatData: function (result) {
            var self = this;
            var obj = Object.create(result) ;
            var i = 1;
            Object.keys(obj.__proto__).forEach(function(key) {
                self.options.push({id:key,text:obj[key]});
                i++;
            });
        }
    }
});