<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:01 PM
 */

return [

    'menus' => [
        'payroll'=>'Payroll',
        
        'payroll_setting' => 'Payroll Settings',
        'tax_rate_setting' => 'Tax Rate Setting',
        'department_settings' => 'Department Setting',
        'payroll_report' => 'Report',
        'main_payroll_management_report'=>' Main Payroll Report ',
        'bank_report'=>' Bank Report ',
        'daily_payroll_management_report'=>' Daily Payroll Report',
        'back_pay_payroll_management_report'=>'BackPay Payroll Report',
        'payroll_management'=>' Payroll Management',
        'main_payroll_management'=>' Main Payroll ',
        'daily_payroll_management'=>' Daily Payroll ',
        'back_pay_payroll_management'=>'BackPay Payroll ',
        'deactivated_main_payroll_management'=>' Deactivated Main Payroll ',
        'deactivated_daily_payroll_management'=>' Deactivated Daily Payroll ',
        'deactivated_back_pay_payroll_management'=>'Deactivated BackPay Payroll ',
        'main_payroll' => [
            'all' => 'All Main Payroll',
            'create' => 'Create Main Payroll',
            'edit' => 'Edit Main Payroll',
            'deleted' => 'Deleted Main Payrolls',
            'management' => 'Payroll Management',
            'main' => 'Main Payrolls',
        ]
    ],
    'label' => [
        'create'=>'Create Payroll',
        'ot_inputs' => 'OT Inputs',
        'total_ot' => 'Total OT',
        'taxable_inputs' => 'Taxable Allowances Inputs',
        'non_taxable_inputs' => 'Non Taxable Inputs',
        'break_down'=>'Break Down',
        'gross_pay'=>'Gross Pay',
        'taxable_income'=>'Taxable Income',
        'taxable_allowance' => 'Taxable Allowance',
        'non_taxable_allowance' => 'Non Taxable Allowance',
        'income_tax'=>'Income Tax',
        'company_pension'=>'Company Pension',
        'employee_pension'=>'Employee Pension',
        'total_deduction'=>'Total Deduction',
        'net_salary'=>'Net Salary',
        'other_deduction' => 'Other Deduction',
        'main_payroll' => [
            'active' => 'Active Main Payrolls',
            'deleted' => 'Deleted Main Payrolls',
            'table' => [
                'id' => 'ID',
                'bank_account' => 'Bank Account No',
                'permanent' => 'is Permanent?',
                'created_by' => 'Created By',
                'project_code' => 'Project Code',
                'employee_name' => 'Employee Name',
                'working_day' => 'Working Day',
                'basic_salary' => 'Basic Salary',
                'perdiem' => 'Perdiem',
                'ot_1_25' => 'OT 1.25',
                'ot_1_5' => 'OT 1.5',
                'ot_2_0' => 'OT 2.0',
                'ot_2_5' => 'OT 2.5',
                'loan' => 'Loan',
                'advance' => 'Advance',
                'telephone_deduction' => 'Telephone Deduction',
                'other_deduction' => 'Other Deduction',
                'cost_sharing_deduction' => 'Cost Sharing Deduction',
                'sport_deduction' => 'Sport Deduction',
                'special_allowance' => 'Special Allowance',
                'house_allowance' => 'House Allowance',
                'position_allowance' => 'Position Allowance',
                'transport_allowance' => 'Transport Allowance',
                'daily_perdiem_allowance' => 'Daily Perdiem Allowance',
                'telephone_allowance' => 'Telephone Allowance',
                'representation_allowance' => 'Representation Allowance',
                'ot_sub_allowance' => 'OT Sub Allowance',
                'other_taxable_allowance' => 'Other Taxable Allowance',

                'other_non_taxable_allowance' => 'Other NON Taxable Allowance',
                'transport_non_taxable_allowance' => 'Transport Non Taxable Allowance',
                'representation_non_taxable_allowance' => 'Representation Non Taxable Allowance',
                'desert_allowance' => 'Desert Allowance',
            ]

        ]
    ]
];
