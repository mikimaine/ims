<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:01 PM
 */

return [

    'menus' => [
        'back_pay_payroll' => [
            'all' => 'All Back Pay Payroll',
            'create' => 'Create Back Pay Payroll',
            'edit' => 'Edit Back Pay Payroll',
            'deleted' => 'Deleted Back Pay Payrolls',
            'management' => 'Back Pay Management',
            'main' => 'Back Pay Payrolls',
        ]
    ],
    'label' => [
        'create'=>'Create Back Pay',
        'ot_inputs' => 'OT Inputs',
        'break_down'=>'Break Down',
        'gross_pay'=>'Gross Pay',
        'taxable_income'=>'Taxable Income',
        'income_tax'=>'Income Tax',
        'company_pension'=>'Company Pension',
        'employee_pension'=>'Employee Pension',
        'total_deduction'=>'Total Deduction',
        'net_salary'=>'Net Salary',
        'back_pay_payroll' => [
            'active' => 'Active Back Pay Payrolls',
            'deleted' => 'Deleted Back Pay Payrolls',
            'table' => [
                'id' => 'ID',
                'project_code' => 'Project Code',
                'employee_name' => 'Employee Name',
                'bank_account' => 'Bank Account No',
                'no_of_months'=>'No of months',
                'previous_salary' => 'Previous Salary',
                'new_salary' => 'New Salary',
                'new_non_taxable_income' => 'New Non Taxable Income',
                'previous_non_taxable_income' => 'Previous Non Taxable Income',
                'new_taxable_incomes' => 'New Taxable Income',
                'previous_taxable_income' => 'Previous Taxable Income',
                'loan' => 'Loan',
                'penalty' => 'Penalty',
                'created_by' => 'Created By',
            ]

        ]
    ]
];
