<?php
/**
 * Doxa IT TECHNOLOGY PLC
 * Created by PhpStorm.
 * Author: Miki Maine Amdu
 * Date: 10/11/16
 * Time: 2:01 PM
 */

return [

    'menus' => [
        'daily_payroll' => [
            'all' => 'All Daily Payroll',
            'create' => 'Create Daily Payroll',
            'edit' => 'Edit Daily Payroll',
            'deleted' => 'Deleted Daily Payrolls',
            'management' => 'Daily Payroll Management',
            'main' => 'Daily Payrolls',
        ]
    ],
    'label' => [
        'create'=>'Create Payroll',
        'ot_inputs' => 'OT Inputs',
        'break_down'=>'Break Down',
        'gross_pay'=>'Gross Pay',
        'taxable_income'=>'Taxable Income',
        'income_tax'=>'Income Tax',
        'company_pension'=>'Company Pension',
        'employee_pension'=>'Employee Pension',
        'total_deduction'=>'Total Deduction',
        'net_salary'=>'Net Salary',
        'daily_payroll' => [
            'active' => 'Active Daily Payrolls',
            'deleted' => 'Deleted Daily Payrolls',
            'table' => [
                'id' => 'ID',
                'project_code' => 'Project Code',
                'employee_name' => 'Employee Name',
                'bank_account' => 'Bank Account No',
                'working_day' => 'Working Day',
                'wage_per_day' => 'Wage / Day',
                'basic_salary' => 'Salary',
                'ot_1_25' => 'OT 1.25',
                'ot_1_5' => 'OT 1.5',
                'ot_2_0' => 'OT 2.0',
                'ot_2_5' => 'OT 2.5',
                'loan' => 'Loan',
                'penalty' => 'Penalty',
                'created_by' => 'Created By',
            ]

        ]
    ]
];
