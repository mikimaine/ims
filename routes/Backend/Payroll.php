<?php

/**
 * All route names are prefixed with 'admin.payroll'
 */
Route::group([
	'prefix'     => 'payroll',
	'as'		 => 'payroll.',
	'namespace'  => 'Payroll',
], function() {
    /**
     * All resource are accessible if the Auth::user() (Authenticated User)
     * has Manage Payroll (manage-payroll) Role Attached to It by the admin
     *
     * This check is done by the Middleware
     */
    Route::group([
        'middleware' => 'payroll.routeNeedsRole:manage-payroll',
    ], function() {
        /**
         *  Rout resource definition for Main Payroll Module
         *  Includes - Main Payroll
         *           - Main Payroll DataTables
         *           - Deleted Main Payroll Status
         */
        Route::get('main_payroll/deleted', 'PayrollStatusController@getDeleted')->name('main_payroll.deleted');

        Route::resource('main_payroll', 'PayrollController', ['except' => ['show']]);

        //For DataTables
        Route::get('main_payroll/get', 'PayrollTableController')->name('main_payroll.get');

        /**
         * Deleted Main Payroll
         */
        Route::group(['prefix' => 'main-payroll/{deletedMainPayroll}'], function() {
            Route::get('main_payroll/delete', 'PayrollStatusController@delete')->name('main_payroll.delete-permanently');
            Route::get('main_payroll/restore', 'PayrollStatusController@restore')->name('main_payroll.restore');
        });

    });




    Route::group([
        'middleware' => 'payroll.routeNeedsRole:manage-payroll',
    ], function() {
        /**
         *  Rout resource definition for Main Payroll Report Module
         *  Includes - Main Payroll Report
         */

        Route::get('daily_payroll_report/report', 'PayrollReportController@daily_report')->name('daily_payroll_report.daily');
        Route::get('back_pay_payroll_report/report', 'PayrollReportController@back_pay_report')->name('back_pay_payroll_report.back_pay');

        Route::get('bank_pay_report/report', 'PayrollReportController@bank_pay_report')->name('bank_pay_report.bank_pay');

        Route::get('main_payroll_report/preview', 'PayrollReportController@report_print')->name('main_payroll_report.preview');
        Route::resource('main_payroll_report', 'PayrollReportController', ['except' => ['show']]);


    });



});


/**
 * All route names are prefixed with 'admin.daily-payroll'
 */
Route::group([
    'prefix'     => 'daily-payroll',
    'as'		 => 'daily-payroll.',
    'namespace'  => 'DailyPayroll',
], function() {
    /**
     * All resource are accessible if the Auth::user() (Authenticated User)
     * has Manage Payroll (manage-payroll) Role Attached to It by the admin
     *
     * This check is done by the Middleware
     */
    Route::group([
        'middleware' => 'payroll.routeNeedsRole:manage-payroll',
    ], function () {
        /**
         *  Rout resource definition for Daily Payroll Module
         *  Includes - Daily Labour Payroll
         *           - Daily Labour Payroll DataTables
         *           - Deleted Daily Labour Payroll Status
         */
        Route::get('daily_payroll/deleted', 'DailyPayrollStatusController@getDeleted')->name('daily_payroll.deleted');

        Route::resource('daily_payroll', 'DailyPayrollController', ['except' => ['show']]);

        //For DataTables
        Route::get('daily_payroll/get', 'DailyPayrollTableController')->name('daily_payroll.get');

        /**
         * Deleted Main Payroll
         */
        Route::group(['prefix' => 'daily-payroll/{deletedDailyPayroll}'], function() {
            Route::get('daily_payroll/delete', 'DailyPayrollStatusController@delete')->name('daily_payroll.delete-permanently');
            Route::get('daily_payroll/restore', 'DailyPayrollStatusController@restore')->name('daily_payroll.restore');
        });


    });

});


/**
 * All route names are prefixed with 'admin.back-pay-payroll'
 */
Route::group([
    'prefix'     => 'back-pay-payroll',
    'as'		 => 'back-pay-payroll.',
    'namespace'  => 'BackPayPayroll',
], function() {
    /**
     * All resource are accessible if the Auth::user() (Authenticated User)
     * has Manage Back Pay Payroll (manage-back-pay-payroll) Role Attached to It by the admin
     *
     * This check is done by the Middleware
     */
    Route::group([
        'middleware' => 'payroll.routeNeedsRole:manage-payroll',
    ], function () {


        /**
         *  Rout resource definition for Back Pay Payroll Module
         *  Includes - Back Pay Payroll
         *           - Back Pay Payroll DataTables
         *           - Deleted Back Pay Payroll Status
         */
        Route::get('back_pay_payroll/deleted', 'BackPayPayrollStatusController@getDeleted')->name('back_pay_payroll.deleted');

        Route::resource('back_pay_payroll', 'BackPayPayrollController', ['except' => ['show']]);

        //For DataTables
        Route::get('back_pay_payroll/get', 'BackPayPayrollTableController')->name('back_pay_payroll.get');

        /**
         * Deleted Back Pay Payroll
         */
        Route::group(['prefix' => 'main-payroll/{deletedBackPayPayroll}'], function() {
            Route::get('back_pay_payroll/delete', 'BackPayPayrollStatusController@delete')->name('back_pay_payroll.delete-permanently');
            Route::get('back_pay_payroll/restore', 'BackPayPayrollStatusController@restore')->name('back_pay_payroll.restore');
        });


    });
});

/**
 * All route names are prefixed with 'admin.settings'
 */
Route::group([
    'prefix'     => 'settings',
    'as'		 => 'settings.',
    'namespace'  => 'Settings',
], function() {
    /**
     * All resource are accessible if the Auth::user() (Authenticated User)
     * has Manage Settings (manage-back-pay-payroll) Role Attached to It by the admin
     *
     * This check is done by the Middleware
     */
    Route::group([
        'middleware' => 'payroll.routeNeedsRole:manage-payroll',
    ], function () {


        /**
         *  Rout resource definition for Settings Module
         *  Includes - Department Settings
         *           - Tax Rate Settings
         *           - Payroll Settings
         */

        Route::resource('department_settings', 'PayrollDepartmentController', ['except' => ['show']]);
        //For DataTables
        Route::get('department_settings/get', 'PayrollDepartmentTableController')->name('department_settings.get');

        Route::resource('tax_rate_settings', 'TaxRateController', ['except' => ['show']]);
        //For DataTables
        Route::get('tax_rate_settings/get', 'TaxRateTableController')->name('tax_rate_settings.get');


        Route::resource('payroll_settings', 'PayrollController', ['except' => ['show']]);
        //For DataTables
        Route::get('payroll_settings/get', 'PayrollTableController')->name('payroll_settings.get');




    });
});
